import apiConfig from "../config/api_config";
import apiUrl from "../config/api_url.json";
import axiosClient from "./axios";

class Boardig {
  baseUrl = apiConfig.base_url;
  
  getPositionLevel() {
    const url = apiUrl.jobPositions;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  getCustomer() {
    const url = apiUrl.getAllCustomersFilter;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  getBranches() {
    const url = apiUrl.getBranchesMetaData;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  getDevision(res) {
    const url = apiUrl.getDevisionsMetaData;
    const reqUrl = this.baseUrl + url + res.value;
    return axiosClient.get(reqUrl);
  }

  getEmployees() {
    const url = apiUrl.getEmployee;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  getEmployeeType() {
    const url = apiUrl.getAllHrEmploymentType;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  addEmployee(req) {
    const url = apiUrl.addEmployee;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, req);
  }

  updateEmployee(req) {
    const url = apiUrl.updateHrEmployee;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, req);
  }

}

const boardingSvc = new Boardig();

export default boardingSvc;
