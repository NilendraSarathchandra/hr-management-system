import apiConfig from "../config/api_config";
import apiUrl from "../config/api_url.json";
import axiosClient from "./axios";

class Employees {
  baseUrl = apiConfig.base_url;
  
  getEmployees(month) {
    const url = apiUrl.getEmployeesByMonth + month;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  addEmployees(res) {
    const url = apiUrl.addEmployeeAmount;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, res);
  }

  getPayrolItem(res) {
    const url = apiUrl.getPayrollMasterByPositionMaster;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, res);
  }

  updateEmployees(res) {
    const url = apiUrl.updateEmployeeAmount;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, res);
  }

  getAllmonths() {
    const url = apiUrl.getMonths;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  getEmployeeList() {
    const url = apiUrl.getEmployee;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  getAllEmployeePayrolls() {
    const url = apiUrl.getAllEmployeePayroll;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

}

const employeeSvc = new Employees();

export default employeeSvc;
