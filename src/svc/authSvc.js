import apiConfig from "../config/api_config";
import apiUrl from "../config/api_url.json";
import axiosClient from "./axios";

class Auth {
  baseUrl = apiConfig.base_url;
  signing(body) {
    const url = apiUrl.signIn;
    const reqUrl = this.baseUrl + url;
    return axiosClient.postAuth(reqUrl, body);
  }
}

const authSvc = new Auth();

export default authSvc;
