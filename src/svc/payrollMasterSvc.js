import apiConfig from "../config/api_config";
import apiUrl from "../config/api_url.json";
import axiosClient from "./axios";

class PayrollMaster {
  baseUrl = apiConfig.base_url;

  //get call, data related to payroll master screen table
  getPayrollMasterItemList() {
    const url = apiUrl.getPayrollMasterItemList;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

   //get call, data related to payroll master drop down 
   // meta-data position master
   getPayrollMasterPositionMaster() {
    const url = apiUrl.getPayrollMasterPositionMaster;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

   //post request, get payroll data by position master id
   getPayrollMasterByPositionMaster(body) {
    const url = apiUrl.getPayrollMasterByPositionMaster;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }

   //post request, get payroll item by position master id which not mapped with position
   getPayrollMasterItemNotMapWithPosition(body) {
    const url = apiUrl.getPayrollMasterItemNotMapWithPosition;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }

  //post request, add payroll master list
  addPayrollMasterList(body) {
    const url = apiUrl.addPayrollMasterList;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }

  //post request, update payroll master list item
  updatePayrollMasterListItem(body) {
    const url = apiUrl.updatePayrollMasterListItem;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }
}

const PayrollMasterService = new PayrollMaster();
export default PayrollMasterService;
