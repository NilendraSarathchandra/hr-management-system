import apiConfig from "../config/api_config";
import apiUrl from "../config/api_url.json";
import axiosClient from "./axios";

class Salary {
  baseUrl = apiConfig.base_url;
  
  getAllEmployeePayrolls() {
    const url = apiUrl.getAllEmployeePayroll;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  getEmployees() {
    const url = apiUrl.getEmployee;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  getPayrolItem(res) {
    const url = apiUrl.getPayrollMasterByPositionMaster;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, res);
  }

  addEmployeePayrolMaster(res) {
    const url = apiUrl.addEmployeePayrolMasterAdd;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, res);
  }

  updateEmployeePayrolMaster(res) {
    const url = apiUrl.addEmployeePayrolMasterUpdate;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, res);
  }

}

const salarySvc = new Salary();

export default salarySvc;
