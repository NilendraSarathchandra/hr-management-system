import axios from "axios";

class Axios {
  getInit() {
    const init = {
      headers: {
        Accept: "application/json;",
        "Content-Type": "application/json",
        "X-Authorization": "Bearer " + JSON.parse(localStorage.getItem("auth_data")).token,
        "X-Requested-With": "XMLHttpRequest",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
        "Access-Control-Allow-Headers": "Content-Type"
      }
    };
    return init;
  }

  getFormInit() {
    const formInit = {
      headers: {
        // Accept: "application/json;",
        "Content-Type": "multipart/form-data"
        //Authorization: "Bearer " + accessToken,
        // "Access-Control-Allow-Origin": "*",
        // "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
        // "Access-Control-Allow-Headers": "Content-Type"
      }
    };
    return formInit;
  }

  getInitAuth() {
    const init = {
      headers: {
        Accept: "application/json;",
        "Content-Type": "application/json",
        // "X-Authorization": "Bearer " + JSON.parse(localStorage.getItem("auth_data")).token,
        "X-Requested-With": "XMLHttpRequest",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
        "Access-Control-Allow-Headers": "Content-Type"
      }
    };
    return init;
  }

  formPost(url, body) {
    let init = this.getFormInit();
    return axios
      .post(url, body, init)
      .then(
        result => {
          return result;
        },
        error => {
          // TODO : How to handle errors
          console.error("error response", error);
          return error;
        }
      )
      .catch(err => {
        // TODO : How to handle exceptions
        console.error("exception", err);
      });
    //});
  }

  post(url, body) {
    const init = this.getInit();
    return axios
      .post(url, body, init)
      .then(
        result => {
          return result;
        },
        error => {
          // TODO : How to handle errors
          console.error("error response", error);
          return error;
        }
      )
      .catch(err => {
        // TODO : How to handle exceptions
        console.error("exception", err);
      });
    //});
  }

  postAuth(url, body) {
    const init = this.getInitAuth();
    return axios
      .post(url, body, init)
      .then(
        result => {
          console.log(result);
          return result;
        },
        error => {
          // TODO : How to handle errors
          console.error("error response", JSON.stringify(error.response.data));
          return error.response.data ? error.response.data : error;
        }
      )
      .catch(err => {
        // TODO : How to handle exceptions
        console.error("exception", err);
      });
  }

  get(url) {
    const init = this.getInit();
    return axios
      .get(url, init)
      .then(
        result => {
          return result;
        },
        error => {
          // TODO : How to handle errors
          console.log(error);
          return error;
        }
      )
      .catch(err => {
        // TODO : How to handle exceptions
        console.log(err);
      });
  }

  put(url, body) {
    //var accessToken = auth.getToken();
    // body = JSON.stringify({ body });
    //return accessToken.then(response => {
    const init = this.getInit();
    return axios
      .put(url, body, init)
      .then(
        result => {
          return result;
        },
        error => {
          // TODO : How to handle errors
          console.error("error response", error);
          return error;
        }
      )
      .catch(err => {
        // TODO : How to handle exceptions
        console.error("exception", err);
      });
    //});
  }
}

const axiosClient = new Axios();

export default axiosClient;
