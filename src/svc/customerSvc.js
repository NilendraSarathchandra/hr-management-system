import apiConfig from "../config/api_config";
import apiUrl from "../config/api_url.json";
import axiosClient from "./axios";

class Customer {
  baseUrl = apiConfig.base_url;

  /// following for customer related screens
  //Form Post call, from this profile images will be added 1.signature 2.photo
  uploadCustomerProfileImages(body) {
    const url = apiUrl.uploadCustomerProfileImages;
    const reqUrl = this.baseUrl + url;
    return axiosClient.formPost(reqUrl, body);
  }

  // Get call, get branches metadata
  getBranchesMetaData() {
    const url = apiUrl.getBranchesListMetaData;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  // Get call, get division list metadata
  getDivisionListMetaData(branchId) {
    let querytString = "?branchId=" + branchId;
    const url = apiUrl.getDivisionListMetaData;
    const reqUrl = this.baseUrl + url + querytString;
    return axiosClient.get(reqUrl);
  }

  // Get call, get property list metadata
  getPropertyListMetaData(customerTypeId) {
    let querytString = "?customerTypeId=" + customerTypeId;
    const url = apiUrl.getPropertyListMetaData;
    const reqUrl = this.baseUrl + url + querytString;
    return axiosClient.get(reqUrl);
  }

  // Get call, get attachment list metadata
  getAttachmentListMetaData(customerTypeId) {
    let querytString = "?customerTypeId=" + customerTypeId;
    const url = apiUrl.getAttachmentListMetaData;
    const reqUrl = this.baseUrl + url + querytString;
    return axiosClient.get(reqUrl);
  }

  // post call, add new customer
  addCustomer(body) {
    const url = apiUrl.addCustomer;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }

  // post call, update property list
  updateCustomerPropertyList(body) {
    const url = apiUrl.updatePropertyList;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }

  //Form Post call, from this customer attachments are added, one file at a time
  uploadCustomerAttachment(body) {
    const url = apiUrl.uploadCustomerAttachment;
    const reqUrl = this.baseUrl + url;
    return axiosClient.formPost(reqUrl, body);
  }

  // Get call, get all customers
  getAllCustomers() {
    const url = apiUrl.getAllCustomers;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  // post call, update existing customer
  updateCustomer(body) {
    const url = apiUrl.updateCustomer;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }
}

const customerSvc = new Customer();
export default customerSvc;
