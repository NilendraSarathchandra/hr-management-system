import apiConfig from "../config/api_config";
import apiUrl from "../config/api_url.json";
import axiosClient from "./axios";

class Business {
  baseUrl = apiConfig.base_url;


  //get all business list
  getAllCustomers() {
    const url = apiUrl.getAllBusinessList;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  //Add new business
  addNewBusiness(data) {
    const url = apiUrl.addNewBusiness;
    const reqUrl = this.baseUrl + url;
    data = JSON.stringify(data);
    return axiosClient.post(reqUrl, data);
  }

   //Update business
   updateBusiness(data) {
    const url = apiUrl.updateBusiness;
    const reqUrl = this.baseUrl + url;
    data = JSON.stringify(data);
    return axiosClient.post(reqUrl, data);
  }

  //update user by super admin
  updateUsers(info) {
    const url = apiUrl.updateBusinessUser;
    const reqUrl = this.baseUrl + url;
    info = JSON.stringify(info);
    return axiosClient.post(reqUrl, info);
  }

  //add new user by super admin
  addUsers(info) {
    const url = apiUrl.addBusinessUser;
    const reqUrl = this.baseUrl + url;
    info = JSON.stringify(info);
    return axiosClient.post(reqUrl, info);
  }

  //get all users by company id
  getAllUsers(companyId) {
    const url = apiUrl.getAllBusinessUsers;
    const reqUrl = this.baseUrl + url;
    companyId = JSON.stringify(companyId);
    return axiosClient.post(reqUrl, companyId);
  }

  //reset password
  resetPassword(info) {
    const url = apiUrl.resetPassword;
    const reqUrl = this.baseUrl + url;
    info = JSON.stringify(info);
    return axiosClient.post(reqUrl, info);
  }

}

const businessSvc = new Business();
export default businessSvc;
