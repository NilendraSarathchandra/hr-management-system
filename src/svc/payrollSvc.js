import apiConfig from "../config/api_config";
import apiUrl from "../config/api_url.json";
import axiosClient from "./axios";

class Payroll {
  baseUrl = apiConfig.base_url;

  /// following for add payroll items screen
  //get call, this will return all the payroll items as a list
  getPayrollItemList() {
    const url = apiUrl.getPayrollItemList;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  //post call, from this payroll item can be added
  addPayrollItem(body) {
    const url = apiUrl.addPayrollItem;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }

  //post call, from this payroll item can be updated
  updatePayrollItem(body) {
    const url = apiUrl.updatePayrollItem;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }

  //get call, from this payroll item category dropdown populated
  getPayrollItemCategory() {
    const url = apiUrl.getPayrollItemCategoryMetaData;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  /// following for job position screen
  // post request this will return all the items of job position 
  getJobPositionItemList(body) {
    const url = apiUrl.getJobPositionItemList;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }
  
  // get request, position level dropdown values are populated from this
  getJobPositionLevel() {
    const url = apiUrl.getJobPositionLevelMetaData;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  // get request, job description template dropdown values are populated from this
  getJDTemplateValues() {
    const url = apiUrl.getJDTemplateMetaData;
    const reqUrl = this.baseUrl + url;
    return axiosClient.get(reqUrl);
  }

  //post request, job position can be created from here
  addJobPosition(body) {
    const url = apiUrl.addJobPosition;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }

  //post request, job position can be updated from here
  updateJobPosition(body) {
    const url = apiUrl.updateJobPosition;
    const reqUrl = this.baseUrl + url;
    return axiosClient.post(reqUrl, body);
  }
}

const payrollSvc = new Payroll();

export default payrollSvc;
