import React, { Component } from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";

import Auxiliary from "util/Auxiliary";
import UserProfile from "./UserProfile";
import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import { connect } from "react-redux";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class SidebarContent extends Component {
  getNoHeaderClass = navStyle => {
    if (
      navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR ||
      navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR
    ) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  getNavStyleSubMenuClass = navStyle => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };

  render() {
    const { themeType, navStyle, pathname } = this.props;
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split("/")[1];

    let userRole = JSON.parse(localStorage.getItem("auth_data")).userRole;

    const BusinessList = () => {
      if (userRole != "SUPER_ADMIN") return "";
      return (

          <Link to="/business/list">
            <i className="icon icon-auth-screen" />
            Business List
          </Link>

      );
    };
    return (
      <Auxiliary>
        <SidebarLogo />
        <div className="gx-sidebar-content">
          <div
            className={`gx-sidebar-notifications ${this.getNoHeaderClass(
              navStyle
            )}`}
          >
            <UserProfile />
            <AppsNavigation />
          </div>
          <CustomScrollbars className="gx-layout-sider-scrollbar">
            <Menu
              defaultOpenKeys={[defaultOpenKeys]}
              selectedKeys={[selectedKeys]}
              theme={themeType === THEME_TYPE_LITE ? "lite" : "dark"}
              mode="inline"
            >
              <MenuItemGroup
                key="main"
                className="gx-menu-group"
                title={<IntlMessages id="sidebar.main" />}
              >
                <Menu.Item key="main/widgets">
                  <Link to="/payroll">
                    <i className="icon icon-widgets" />
                    <IntlMessages id="Payroll" />
                  </Link>
                </Menu.Item>

                <Menu.Item key="main/metrics">
                  <Link to="/jobposition">
                    <i className="icon icon-apps" />
                    <IntlMessages id="Job position" />
                  </Link>
                </Menu.Item>

                <Menu.Item key="main/layouts">
                  <Link to="/payrollmaster/add">
                    <i className="icon icon-card" />
                    <IntlMessages id="Payroll Master" />
                  </Link>
                </Menu.Item>

                <Menu.Item key="/client/list">
                  <Link to="/client/list">
                    <i className="icon icon-auth-screen" />
                    <IntlMessages id="Client List" />
                  </Link>
                </Menu.Item>

                <Menu.Item key="/boarding/list">
                  <Link to="/boarding/list">
                    <i className="icon icon-all-contacts" />
                    <IntlMessages id="Position boarding" />
                  </Link>
                </Menu.Item>

                <Menu.Item key="/salary/list">
                  <Link to="/salary/list">
                    <i className="icon icon-tickets" />
                    <IntlMessages id="Salary" />
                  </Link>
                </Menu.Item>
                <Menu.Item key="/employee/list">
                  <Link to="/employee/list">
                    <i className="icon icon-profile" />
                    <IntlMessages id="Employees" />
                  </Link>
                </Menu.Item>
                
                <Menu.Item key="/business/list">
                  <BusinessList />
                </Menu.Item> 
              </MenuItemGroup>
            </Menu>
          </CustomScrollbars>
        </div>
      </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({ settings }) => {
  const { navStyle, themeType, locale, pathname } = settings;
  return { navStyle, themeType, locale, pathname };
};
export default connect(mapStateToProps)(SidebarContent);
