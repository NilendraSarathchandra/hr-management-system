import React, { Component } from "react";
import { connect } from "react-redux";
import { Avatar, Popover } from "antd";
import { userSignOut } from "appRedux/actions/Auth";
import { resetPassword } from "appRedux/actions/Business";
import { Modal, Button, Form, Input, message } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";

const FormItem = Form.Item;
class UserProfile extends Component {
  state = {
    modalVisible: false
  };

  handleCancel = () => {
    const { resetFields } = this.props.form;
    resetFields();
    this.setState({
      modalVisible: false
    });
  };

  showModal = () => {
    this.setState({
      modalVisible: true
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { resetFields } = this.props.form;
    const { resetPassword } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        resetPassword(values);
      }
    });
  };

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    const { getFieldDecorator } = this.props.form;
    const { modalVisible } = this.state;
    const userMenuOptions = (
      <ul className="gx-user-popover">
        <li
          onClick={() => {
            this.showModal();
          }}
        >
          Reset Password
        </li>
        <li
          onClick={() => {
            localStorage.removeItem("auth_data");
            this.props.userSignOut();
          }}
        >
          Logout
        </li>
      </ul>
    );

    const NotificationMessage = () => {
      const { business } = this.props;
      if (business && business.notificationR) {
        switch (business.notificationR.type) {
          case "success":
            return message.success(business.notificationR.message);
          case "error":
            return message.error(business.notificationR.message);

          default:
            return null;
        }

      } else {
        return null;
      }
    };

    return (
      <>
        <NotificationMessage />
        <div className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row">
          <Popover
            placement="bottomRight"
            content={userMenuOptions}
            trigger="click"
          >
            <Avatar
              src="https://via.placeholder.com/150x150"
              className="gx-size-40 gx-pointer gx-mr-3"
              alt=""
            />
            <span className="gx-avatar-name">
            {JSON.parse(localStorage.getItem("auth_data")).userId}
              <i className="icon icon-chevron-down gx-fs-xxs gx-ml-2" />
            </span>
          </Popover>
        </div>
        <Modal
          visible={modalVisible}
          title="Reset password"
          onCancel={this.handleCancel}
          footer={[
            <Button
              key="submit"
              type="primary"
              onClick={e => this.handleSubmit(e)}
            >
              Reset Password
            </Button>
          ]}
        >
          <Form onSubmit={this.handleSubmit}>
            <FormItem label="Current Password" {...formItemLayout}>
              {getFieldDecorator("oldPassword", {
                initialValue: "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(
                <Input.Password
                  maxLength="10"
                  iconRender={visible =>
                    visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                  }
                />
              )}
            </FormItem>

            <FormItem label="New Password" {...formItemLayout}>
              {getFieldDecorator("newPassword", {
                initialValue: "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(
                <Input.Password
                  maxLength="10"
                  iconRender={visible =>
                    visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                  }
                />
              )}
            </FormItem>
          </Form>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = state => ({
  business: state.business
});

const mapDispatchToProps = dispatch => {
  return {
    userSignOut: () => {
      dispatch(userSignOut());
    },
    resetPassword: businessInfo => {
      dispatch(resetPassword(businessInfo));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(UserProfile));
