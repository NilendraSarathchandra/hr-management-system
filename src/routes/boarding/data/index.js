import React from "react";
import { Col, Row } from "antd";

import Boarding from "./boarding";

const PositionBoarding = () => {
  return (
    <Row>
      <Col span={24}>
        <Boarding />
      </Col>
    </Row>
  );
};

export default PositionBoarding;