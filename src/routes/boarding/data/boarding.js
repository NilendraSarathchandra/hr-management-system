import { Col, Input, Select, Radio, Form, Card, Button } from "antd";
import React from "react";
import DataTable from "./table"
import AddModal from "./AddModal"
import { connect } from "react-redux";
import {
    getMetaData,
    getEmployeeList, getEmployeeTypeList
} from "../../../appRedux/actions/boarding";
const Search = Input.Search;
const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 3 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
        md: { span: 16 },
        lg: { span: 12 },
    },
};


class Boarding extends React.Component {
    state = {
        loading: false,
        modalVisible: false,
        search: ''
    };

    selectOption = (e) => {
        this.setState({ formLayout: e.target.value });
    }

    constructor(props) {
        super(props);
        const { getEmployeeTypeList } = this.props;
        getEmployeeTypeList();
    }

    showModal = (status) => {
        const { metaData } = this.props;
        this.setState({
            modalVisible: status,
            metaData: metaData,
            editItem: null
        });
    };

    handleEdit = (item) => {
        const newItem = this.props.metaData.employeeList.filter(res => {
            return res.id === item.id;
        })
        if (newItem.length > 0) {
            this.setState({ ...this.state, editItem: newItem[0], modalVisible: true });
        }
    };

    onSearch = (value) => {
        const { serchCriteria } = this.state;
        const employeeList = this.props.metaData.employeeList;
        if (value) {
            if (serchCriteria) {
                const serchValue = value.target.value.toLowerCase();
                const filterdList = employeeList.filter(obj => {
                    const objSerchValue = obj.ciCustomer[serchCriteria].toLowerCase();
                    if (objSerchValue.includes(serchValue)) {
                        return obj;
                    }
                });
                this.setState({
                    employeeList: filterdList
                });
            } else {
                const serchValue = value.target.value.toLowerCase();
                const filterdList = employeeList.filter(obj => {
                    const objSerchValue = obj.ciCustomer.customerName.toLowerCase();
                    if (objSerchValue.includes(serchValue)) {
                        return obj;
                    }
                });
                this.setState({
                    employeeList: filterdList
                });
            }
        } else {
            this.setState({
                employeeList: employeeList
            });
        }
    };

    dataSet = (value) => {
        this.setState({
            serchCriteria: value
        });
    };

    componentDidMount() {
        const { getMetaData } = this.props;
        getMetaData();
        const { getEmployeeList } = this.props;
        getEmployeeList();

        const { employeeList } = this.props.metaData;
        this.setState({
            employeeList: employeeList
        });
    }

    render() {
        let { modalVisible, metaData, employeeList, editItem } = this.state;
        employeeList = employeeList ? employeeList : this.props.metaData.employeeList;
        const formatedList = employeeList ? employeeList.map((res) => {
            const data = (res || {}).ciCustomer;
            if (data && data.id) {
                data.id = (res || {}).id;
                Object.assign(data, { action: 'Edit', key: (res || {}).id });
            }
            return data;
        }) : [];
        const tableData = formatedList;
        const serchCriteria = [{
            title: "Number",
            dataIndex: "id",

        },
        {
            title: "Name",
            dataIndex: "customerName",
        },
        {
            title: "NIC No",
            dataIndex: "nic",
        },
        {
            title: "Address",
            dataIndex: "address",
        },
        {
            title: "Telephone",
            dataIndex: "phoneNumber",
        },
        {
            title: "Email",
            dataIndex: "email",
        },
        {
            title: "Sex",
            dataIndex: "gender",
        },
        {
            title: "Status",
            dataIndex: "active"
        }];
        return (
            <>
                <div>
                    <Card title="Position Boarding">
                        <Form >
                            <FormItem
                                label="Add New"
                                {...formItemLayout}
                            >
                                <Button type="primary" className="gx-text-right" onClick={() => this.showModal(true)}>Add</Button>
                            </FormItem>
                            <FormItem
                                label="More Filters"
                                {...formItemLayout}
                            >
                                <Select placeholder="Please select a option !" key={Math.random} onChange={this.dataSet} >
                                    {serchCriteria.map(option => {
                                        return (
                                            <Option key={Math.random} value={option.dataIndex}  >
                                                {option.title}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            </FormItem>
                            <FormItem
                                {...formItemLayout}
                                label="Search">
                                <Search
                                    placeholder="Serch text"
                                    onChange={value => this.onSearch(value)}
                                    style={{ width: 200 }}
                                />
                                {/* <Input placeholder="Serch item" id="error" /> */}
                            </FormItem>
                        </Form>
                        {tableData ? <DataTable tableData={tableData} handleEdit={this.handleEdit} /> : ''}

                        <AddModal modalVisible={modalVisible} editItem={editItem && editItem} metaData={metaData} modalHanndler={(state) => this.showModal(state)} />
                    </Card>
                </div>

            </>
        );
    }
}


const mapStateToProps = state => ({
    metaData: state.boarding
});

const mapDispatchToProps = dispatch => {
    return {
        getMetaData: () => {
            dispatch(getMetaData());
        },
        getEmployeeList: () => {
            dispatch(getEmployeeList());
        },
        getEmployeeTypeList: () => {
            dispatch(getEmployeeTypeList());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Boarding);
