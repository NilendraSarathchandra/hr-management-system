import { Table, Tag } from "antd";
import React from "react";


class DataTable extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleEdit } = this.props;

    const columns = [
      {
        title: "Number",
        dataIndex: "id",
        key: "id",
        render: text => <span className="gx-link">{text}</span>
      },
      {
        title: "Name",
        dataIndex: "customerName",
        key: "customerName"
      },
      {
        title: "NIC No",
        dataIndex: "nic",
        key: "nic"
      },
      {
        title: "Telephone",
        dataIndex: "phoneNumber",
        key: "phoneNumber"
      },
      {
        title: "Email",
        dataIndex: "email",
        key: "email"
      },
      {
        title: "Sex",
        dataIndex: "gender",
        key: "gender"
      },
      {
        title: "Status",
        dataIndex: "active",
        key: "active",
        render: (text) => (
          <>
            {!text ? <Tag color="red">Desable</Tag> : <Tag color="green">Active</Tag>}
          </>
        ),
      },
      {
        title: "Action",
        key: "action",
        dataIndex: "action",
        render: (text, record) => (
          <span>
            <span className="gx-link" onClick={() => handleEdit(record)}>
              Edit
            </span>
          </span>
        )
      }
    ];
    console.log('object', this.props.tableData);
    return (
      <Table
        pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15'] }}
        className="gx-table-responsive"
        columns={columns}
        dataSource={this.props.tableData}
        expandedRowRender={record => (
          <>
            
              Address: <div style={{ color: "#038fdd" }}>{record.address}</div>
            
          </>
        )}
      />
    );

  }
}

export default DataTable ;
