import React, { Component } from "react";
import { Button, message, Form, Input, Select, Modal, DatePicker } from "antd";
import { connect } from "react-redux";
import {
  getDevisionMetaData, addEmployee, updateEmployee
} from "../../../appRedux/actions/boarding";
import moment from "moment";

const FormItem = Form.Item;
const Option = Select.Option;
class AddModal extends Component {

  handleFormLayoutChange = (e) => {
    this.setState({ formLayout: e.target.value });
  }

  constructor(props) {
    super(props);
    this.state = {
      formLayout: 'horizontal',
      reportToList: []
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      //console.log("values ", values.category);
      if (!err) {
        //console.log("Received values of form: ", values);
      }
    });
  };

  handleCancel = () => {
    this.props.modalHanndler(false);
  };

  handleSave = (state) => {

  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { addEmployee, editItem, updateEmployee } = this.props;
    const { resetFields } = this.props.form;
    // console.log(getFieldsValue, this.props.form)
    this.props.form.validateFields((err, values) => {
      const req = {
        customerId: values.employee,
        positionMasterId: values.position,
        hrEmploymentTypeId: values.type,
        reportToId: values.report_to,
        divisionMasterId: values.devision,
        employmentTypeId: values.employee,
        totalLeave: values.total_leave,
        hireFrom: values.hire_from.format("YYYY-MM-DD"),
        hireTo: values.hire_to.format("YYYY-MM-DD")
      }
      if (!err && !editItem) {
        addEmployee(req);
        message.success('Successfully added');
        resetFields();
        this.handleCancel()
        // console.log('Received values of form: ', values);
      } else if (!err && editItem) {
        updateEmployee(req);
        message.success('Successfully Updated');
        resetFields();
        this.handleCancel()
      } else {
        message.error('Adding Fail');
      }
    });
  }

  setFieldsValue = res => {
    // console.log(res);
    const { getDevisionMetaData } = this.props;
    getDevisionMetaData(res);
    // const { addEmployee } = this.props;
    // addEmployee(res);
  };

  getInitialLevel = id => {
    const { metaData } = this.props;
    const employeeList = metaData.employeeList;
    const position = metaData.positionList.find(item => item.id === id);
    console.log(employeeList, (position || {}).reportTo);
    if ((position || {}).reportTo) {
      let filteredLevels = employeeList.filter(
        item => { return +(((item || {}).hrPositionMaster || {}).reportTo || {}).level > +(position || {}).reportTo.level }
      );
      if (filteredLevels.length <= 0) {
        filteredLevels = employeeList.filter(
          item => (((item || {}).hrPositionMaster || {}).reportTo || {}).level === (position || {}).reportTo.level
        );
      }
      return filteredLevels;
    }
  };

  render() {
    const { formLayout } = this.state;
    const formItemLayout = formLayout === 'horizontal' ? {
      labelCol: { xs: 24, sm: 6 },
      wrapperCol: { xs: 24, sm: 18 },
    } : null;

    const { modalVisible, metaData, editItem } = this.props;
    const positionList = (metaData || {}).positionList ? (metaData || {}).positionList : [];
    const branchList = (metaData || {}).branchList ? (metaData || {}).branchList : [];
    const devisionList = (metaData || {}).devisionList ? (metaData || {}).devisionList : [];
    const empType = (metaData || {}).employeeTypeList ? (metaData || {}).employeeTypeList : [];
    const employeeList = (metaData || {}).employeeList ? (metaData || {}).employeeList : [];
    const customerList = (metaData || {}).customerList ? (metaData || {}).customerList : [];

    const { getFieldDecorator } = this.props.form;
    const dateConfig = {
      initialValue: editItem ? moment(editItem.hireFrom) : "",
      rules: [{ type: 'object', required: true, message: 'Please select date!' }],
    };

    const dateConfig1 = {
      initialValue: editItem ? moment(editItem.hireTo) : "",
      rules: [{ required: true, message: "Please select a date" }]
    };

    const hrPositionLevelId = this.props.form.getFieldValue(
      "position"
    );
    let reportToList = [];
    console.log(hrPositionLevelId)

    if (editItem !== null) {
      if (hrPositionLevelId === "") {
        reportToList = this.getInitialLevel(
          ((editItem || {}).reportToEmployee || {}).id
        );
      } else {
        reportToList = this.getInitialLevel(hrPositionLevelId);
      }
    } else {
      if (this.state.reportToList.length === 0) {
        reportToList = this.getInitialLevel(hrPositionLevelId);
      }
    }

    // console.log(this.state, this.props, metaData, 'AAAAA')
    return (
      <>
        <Modal visible={modalVisible} title="Position boarding" onCancel={this.handleCancel}
          footer={[
            <Button
              key="submit"
              type="primary"
              onClick={e => this.handleSubmit(e)}
            >
              {editItem ? 'Update' : 'Save'}
            </Button>
          ]} >
          <Form layout={formLayout} onSubmit={this.handleSubmit}>
            <FormItem
              label="Employee Name"
              {...formItemLayout}
            >
              {getFieldDecorator('employee', {
                initialValue: editItem ? editItem.ciCustomer.id : "",
                rules: [
                  { required: true, message: 'Employee name not add!' },
                ],
              })(
                <Select placeholder="Please select a option !" key={Math.random}>
                  {customerList.map(option => {
                    return (
                      <Option key={Math.random} value={option.id}>
                        {option.customerName}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem
              label="Position"
              {...formItemLayout}
            >
              {getFieldDecorator('position', {
                initialValue: editItem ? editItem.hrPositionMaster.id : "",
                rules: [
                  { required: true, message: 'Position no position selected!' },
                ],
              })(
                <Select placeholder="Please select a position" key={Math.random}>
                  {positionList.map(option => {
                    return (
                      <Option key={Math.random} value={option.id}>
                        {option.positionName}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem
              label="Employee Type"
              {...formItemLayout}
            >

              {getFieldDecorator('type', {
                initialValue: editItem && editItem.hrEmploymentType ? editItem.hrEmploymentType.id : "",
                rules: [
                  { required: true, message: 'Type not selected!' },
                ],
              })(
                <Select placeholder="Please select a type" key={Math.random}>
                  {empType.map(option => {
                    return (
                      <Option key={Math.random} value={option.id}>
                        {option.employmentType}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem
              label="Report to"
              {...formItemLayout}
            >
              {getFieldDecorator('report_to', {
                initialValue: editItem && editItem.reportToEmployee ? editItem.reportToEmployee.id : "",
                rules: [
                  { required: true, message: 'Report person not selected!' },
                ],
              })(
                <Select placeholder="Please select report person" key={Math.random}>
                  {reportToList && reportToList.map(option => {
                    return (
                      <Option key={Math.random} value={option.id}>
                        {((option || {}).ciCustomer || {}).customerName}
                      </Option>
                    );
                  })}
                </Select>
              )}

            </FormItem>
            <FormItem
              label="Branch"
              {...formItemLayout}
            >
              {getFieldDecorator('branch', {
                initialValue: editItem ? editItem.divisionMaster.branchMaster.id : "",
                rules: [
                  { required: true, message: 'Branch not selected!' },
                ],
              })(
                <Select placeholder="Please select a branch" key={Math.random} onChange={value =>
                  this.setFieldsValue({ value: value })
                }>
                  {branchList.map(option => {
                    return (
                      <Option key={Math.random} value={option.id}>
                        {option.branchName}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem
              label="Devision"
              {...formItemLayout}
            >
              {getFieldDecorator('devision', {
                initialValue: editItem ? editItem.divisionMaster.id : "",
                rules: [
                  { required: true, message: 'Devision not selected!' },
                ],
              })(
                <Select placeholder="Please select a devision" key={Math.random}>
                  {devisionList.map(option => {
                    return (
                      <Option key={Math.random} value={option.id}>
                        {option.divisionName}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem
              label="Hire From"
              {...formItemLayout}
            >
              {getFieldDecorator('hire_from', dateConfig)(
                <DatePicker className="gx-mb-3 gx-w-100" />
              )}
            </FormItem>
            <FormItem
              label="Hire To"
              {...formItemLayout}
            >
              {getFieldDecorator('hire_to', dateConfig1)(
                <DatePicker className="gx-mb-3 gx-w-100" />
              )}
            </FormItem>
            <FormItem
              label="Total Leave"
              {...formItemLayout}
            >
              {getFieldDecorator('total_leave', {
                initialValue: editItem ? editItem.totalLeave : "",
                rules: [{ required: true, message: 'Total Leave to should be add!' }],
              })(
                <Input />
              )}
            </FormItem>
          </Form>
        </Modal>
      </>
    );
  }

}

const FormModal = Form.create()(AddModal);

const mapStateToProps = state => ({
  metaData: state.boarding
});

const mapDispatchToProps = dispatch => {
  return {
    getDevisionMetaData: (res) => {
      dispatch(getDevisionMetaData(res));
    },
    addEmployee: (req) => {
      dispatch(addEmployee(req));
    },

    updateEmployee: (req) => {
      dispatch(updateEmployee(req));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormModal);






