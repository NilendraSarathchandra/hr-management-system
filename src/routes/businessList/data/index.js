import React from "react";
import { Col, Row } from "antd";

import BusinessList from "./businessList";

const Business = () => {
  return (
    <Row>
      <Col span={24}>
        <BusinessList />
      </Col>
    </Row>
  );
};

export default Business;