import {
  Dropdown,
  Input,
  Select,
  Radio,
  Form,
  Card,
  Button,
  message
} from "antd";
import React from "react";
import DropdownList from "./Dropdown";
import DataTable from "./table";
import AddModal from "./addModal";
import UserModal from "./userModal";
import SigatureForm from "./signatureForm";
import PropertyForm from "./property";
import AttachmentForm from "./attachment";
import { connect } from "react-redux";
import {
  getAllBusinessList,
  AddNewBusiness,
  UpdateBusinsess,
  getAllUsers
} from "../../../appRedux/actions/Business";

const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 3 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 18 },
    md: { span: 16 },
    lg: { span: 12 }
  }
};

class BusinessList extends React.Component {
  state = {
    loading: false,
    modalVisible: false,
    userModalVisible: false,
    companyDetails: null,
    editItem: null
  };

  componentDidMount() {
    const { getAllBusinessList } = this.props;

    getAllBusinessList();
  }

  selectOption = e => {
    this.setState({ formLayout: e.target.value });
  };

  constructor(props) {
    super(props);
  }

  showModal = status => {
    this.setState({
      modalVisible: status,
      editItem: null
    });
  };

  showUserModal = status => {
    this.setState({
      userModalVisible: status
    });
  };

  setFieldsValue = object => {};

  getDivisionListMetaData = branchId => {
    const { getDivisionListMetaData } = this.props;
    getDivisionListMetaData(branchId);
  };

  handleSubmitbusinessAdd = businessInfo => {
    const { AddNewBusiness } = this.props;
    AddNewBusiness(businessInfo);
  };

  handleEditBusinessList = item => {
    this.setState({ ...this.state, editItem: item, modalVisible: true });
  };

  handleManageUsers = item => {
    const { getAllUsers } = this.props;
    this.setState({
      ...this.state,
      companyDetails: item,
      userModalVisible: true
    });
    getAllUsers(item.businessMasterId);
  };

  handleSubmitBusinessUpdate = body => {
    const { UpdateBusinsess } = this.props;
    UpdateBusinsess(body);
  };

  render() {
    const { modalVisible, userModalVisible, companyDetails } = this.state;
    const { business } = this.props;

    const NotificationMessage = () => {
      const { business } = this.props;
      if (business && business.notification) {
        switch (business.notification.type) {
          case "success":
            return message.success(business.notification.message);
          case "error":
            return message.error(business.notification.message);

          default:
            return null;
        }
      } else {
        return null;
      }
    };

    return (
      <>
        <NotificationMessage />
        <div>
          <Card title="Business List">
            <Button
              type="primary"
              className="gx-text-right"
              onClick={() => this.showModal(true)}
            >
              Add
            </Button>
            <DataTable
              businessList={business && business.businessList}
              handleEditBusinessList={this.handleEditBusinessList}
              handleManageUsers={this.handleManageUsers}
            />
            <AddModal
              modalVisible={modalVisible}
              modalHandler={(state, page) => this.showModal(state, page)}
              handleSubmitbusinessAdd={this.handleSubmitbusinessAdd}
              handleSubmitBusinessUpdate={this.handleSubmitBusinessUpdate}
              editItem={this.state.editItem && this.state.editItem}
            />
            {companyDetails != null ? (
              <UserModal
                userModalVisible={userModalVisible}
                modalHandler={state => this.showUserModal(state)}
                companyDetails={companyDetails}
              />
            ) : (
              ""
            )}
          </Card>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  business: state.business
});

const mapDispatchToProps = dispatch => {
  return {
    getAllBusinessList: () => {
      dispatch(getAllBusinessList());
    },
    AddNewBusiness: businessInfo => {
      dispatch(AddNewBusiness(businessInfo));
    },
    UpdateBusinsess: businessInfo => {
      dispatch(UpdateBusinsess(businessInfo));
    },
    getAllUsers: companyId => {
      dispatch(getAllUsers(companyId))
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessList);
