import { Button, Form, Modal, Table, Upload, Icon, message } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getAttachmentListMetaData,
  addCustomerAttachments
} from "../../../appRedux/actions/Customer";

class Attachment extends Component {
  componentDidMount() {
    const { getAttachmentListMetaData, customerInfo } = this.props;
    getAttachmentListMetaData(customerInfo.customerType.id);
  }

  state = {
    confirmDirty: false,
    autoCompleteResult: []
  };

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  handleCancel = () => {
    this.props.modalHandler(false, "AT");
  };

  handleSubmit = e => {
    e.preventDefault();
    const { customer, customerInfo, addCustomerAttachments } = this.props;

    let isFileEmpty = false;
    customer.attachmentList.map(item => {
      if (this.state[item.id] === undefined) {
        isFileEmpty = true;
      }
    });

    if (isFileEmpty) {
      message.error("All the files are required");
    }
    if (!isFileEmpty) {
      addCustomerAttachments(
        customer.attachmentList,
        this.state,
        customerInfo.id
      );
      this.props.modalHandler(false, "AT");
    }
  };

  getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  beforeUpload(file) {
    // accept only images, word, pdf
    const isValidType =
      file.type === "image/jpeg" ||
      file.type === "image/png" ||
      file.type === "application/pdf" ||
      file.type === "application/msword" ||
      file.type ===
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    if (!isValidType) {
      message.error("You can only upload JPG, PNG, DOC, DOCX, PDF files!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isValidType && isLt2M;
  }

  handleChange = (info, name) => {
    if (info.file.status === "uploading") {
      this.setState({ ...this.state, [name]: { loading: true } });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          ...this.state,
          [name]: {
            imageUrl,
            loading: false,
            file: info.file.originFileObj
          }
        })
      );
    }
  };

  onCustomRequest = file => {
    return new Promise((resolve, reject) => {
      const ajaxResponseWasFine = true;

      setTimeout(() => {
        if (ajaxResponseWasFine) {
          const reader = new FileReader();

          reader.addEventListener(
            "load",
            () => {
              resolve(reader.result);
            },
            false
          );

          if (file) {
            reader.readAsDataURL(file);
          }
        } else {
          reject("error");
        }
      }, 1000);
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      attachmentModal,
      loading,
      handleCancel,
      handleOk,
      customer
    } = this.props;
    const { current } = this.state;

    const uploadButtonImage = (id, name) => {
      return (
        <div>
          <Icon
            type={this.state.id && this.state.id.loading ? "loading" : "plus"}
          />
          <div className="ant-upload-text">{name}</div>
        </div>
      );
    };

    return (
      <>
        <Modal
          visible={attachmentModal}
          title="Upload Client Other Attachments"
          onOk={() => handleOk()}
          onCancel={this.handleCancel}
          footer={[
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={e => this.handleSubmit(e)}
            >
              Finish
            </Button>
          ]}
        >
          {customer &&
            customer.attachmentList &&
            customer.attachmentList.map(item => {
              return (
                <Upload
                  name={item.id}
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  beforeUpload={this.beforeUpload}
                  onChange={info => this.handleChange(info, item.id)}
                  action={this.onCustomRequest}
                >
                  {this.state[item.id] && this.state[item.id].imageUrl ? (
                    <img src={this.state[item.id].imageUrl} alt="" />
                  ) : (
                    uploadButtonImage(item.id, item.attachmentName)
                  )}
                </Upload>
              );
            })}
        </Modal>
      </>
    );
  }
}

const mapStateToProps = state => ({
  customer: state.customer
});

const mapDispatchToProps = dispatch => {
  return {
    getAttachmentListMetaData: customerTypeId => {
      dispatch(getAttachmentListMetaData(customerTypeId));
    },
    addCustomerAttachments: (attachmentList, fileList, customerId) => {
      dispatch(addCustomerAttachments(attachmentList, fileList, customerId));
    }
  };
};

const AttachmentForm = Form.create()(Attachment);
export default connect(mapStateToProps, mapDispatchToProps)(AttachmentForm);
