import React, { Component } from "react";
import { Card, Divider, Icon, Table, Button, Input } from "antd";
import Highlighter from "react-highlight-words";
import moment from "moment";
import { cloneableGenerator } from "redux-saga/utils";

class DataTable extends Component {
  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record !== undefined
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      )
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  handleChange = (pagination, filters, sorter) => {
    this.setState({
      filteredInfo: filters
    });
  };

  clearFilters = () => {
    this.setState({ filteredInfo: null });
  };

  render() {
    let { filteredInfo } = this.state;
    const { handleEditBusinessList, handleManageUsers } = this.props;
    filteredInfo = filteredInfo || {};
    const dateFormat = "YYYY/MM/DD";
    const columns = [
      {
        title: "Number of Employees",
        dataIndex: "noOfEmployees",
        key: "noOfEmployees",
        width: 150,
        sorter: (a, b) =>
          Math.round(a.noOfEmployees) - Math.round(b.noOfEmployees),
        ...this.getColumnSearchProps("noOfEmployees")
      },
      {
        title: "Annual Turnover",
        dataIndex: "annualTurnover",
        key: "annualTurnover",
        width: 150,
        sorter: (a, b) =>
          Math.round(a.annualTurnover) - Math.round(b.annualTurnover),
        ...this.getColumnSearchProps("annualTurnover")
      },
      {
        title: "Business Name",
        dataIndex: "businessName",
        key: "businessName",
        width: 150,
        sorter: (a, b) => a.businessName - b.businessName,
        ...this.getColumnSearchProps("businessName")
      },
      {
        title: "Address",
        dataIndex: "address",
        key: "address",
        width: 150,
        sorter: (a, b) => Math.round(a.address) - Math.round(b.address),
        ...this.getColumnSearchProps("address")
      },
      {
        title: "City",
        dataIndex: "city",
        key: "city",
        width: 150,
        sorter: (a, b) => Math.round(a.city) - Math.round(b.city),
        ...this.getColumnSearchProps("city")
      },
      {
        title: "Country",
        dataIndex: "country",
        key: "country",
        width: 150,
        sorter: (a, b) => Math.round(a.country) - Math.round(b.country),
        ...this.getColumnSearchProps("country")
      },
      {
        title: "District",
        dataIndex: "district",
        key: "district",
        width: 150,
        sorter: (a, b) => Math.round(a.district) - Math.round(b.district),
        ...this.getColumnSearchProps("district")
      },
      {
        title: "Registration Number",
        dataIndex: "registerNumber",
        key: "registerNumber",
        width: 150,
        sorter: (a, b) =>
          Math.round(a.registerNumber) - Math.round(b.registerNumber),
        ...this.getColumnSearchProps("registerNumber")
      },
      {
        title: "Registration Date",
        //dataIndex: "registerDate",
        key: "registerDate",
        width: 150,
        render: (text, record) => (
          <span>{moment(text.registerDate).format(dateFormat)}</span>
        )
        // sorter: (a, b) =>
        //   Math.round(a.registerDate) - Math.round(b.registerDate),
        // ...this.getColumnSearchProps("registerDate")
      },
      {
        title: "Business Start Date",
        // dataIndex: "businessStart",
        key: "businessStart",
        width: 150,
        render: (text, record) => (
          <span>{moment(text.businessStart).format(dateFormat)}</span>
        )
        // sorter: (a, b) => Math.round(a.businessStart) - Math.round(b.businessStart),
        // ...this.getColumnSearchProps("businessStart")
      },
      {
        title: "Action",
        key: "action",
        render: (text, record) => (
          <span>
            <span
              className="gx-link"
              onClick={() => handleEditBusinessList(text)}
            >
              Edit
            </span>
          </span>
        )
      },
      {
        title: "User",
        key: "user",
        render: (text, record) => (
          <span>
            <span className="gx-link" onClick={() => handleManageUsers(text)}>
              Manage User
            </span>
          </span>
        )
      }
    ];

    const { businessList } = this.props;
    let sizeScreen = window.innerHeight;
    return (
      <Table
        rowKey="businessMasterId"
        pagination={{
          defaultPageSize: 10,
          showSizeChanger: true,
          pageSizeOptions: ["5", "10", "15"]
        }}
        className="gx-table-responsive"
        columns={columns}
        dataSource={businessList}
        scroll={{ y: sizeScreen - 500 }}
        onChange={this.handleChange}
      />
    );
  }
}

export default DataTable;
