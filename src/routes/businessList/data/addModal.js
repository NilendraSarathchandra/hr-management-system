import React, { Component } from "react";
import {
  Button,
  Card,
  Form,
  Input,
  Select,
  Modal,
  DatePicker,
  message,
  InputNumber
} from "antd";
import { NAV_STYLE_DEFAULT_HORIZONTAL } from "../../../constants/ThemeSetting";
import moment from "moment";

const FormItem = Form.Item;
const Option = Select.Option;
class AddModal extends Component {
  handleFormLayoutChange = e => {
    this.setState({ formLayout: e.target.value });
  };

  state = {
    branchName: null
  };

  constructor(props) {
    super(props);
    this.state = {
      formLayout: "horizontal"
    };
  }

  handleCancel = () => {
    const { resetFields } = this.props.form;
    resetFields();
    this.setState({ branchName: null });
    this.props.modalHandler(false, "AD");
  };

  getFormattedDate(date) {
    return date.format("YYYY-MM-DD");
  }

  handleSubmit = e => {
    e.preventDefault();
    const { resetFields } = this.props.form;
    const {
      handleSubmitbusinessAdd,
      editItem,
      handleSubmitBusinessUpdate
    } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        values.registerDate = this.getFormattedDate(values.registerDate);
        values.businessStart = this.getFormattedDate(values.businessStart);
        values.licenseStartDate = this.getFormattedDate(
          values.licenseStartDate
        );
        values.licenseEndDate = this.getFormattedDate(values.licenseEndDate);

        if (editItem) {
          values.businessMasterId = editItem.businessMasterId;
          handleSubmitBusinessUpdate(values);
          this.props.modalHandler(false);
          resetFields();
          message.success("Record Updated");
        } else {
          handleSubmitbusinessAdd(values);
          this.props.modalHandler(false);
          resetFields();
          message.success("Record Added");
        }
      }
    });
  };

  disabledDate = value => {
    const form = this.props.form;

    return false;
  };

  render() {
    const { formLayout } = this.state;
    const { editItem } = this.props;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    const { modalVisible } = this.props;
    const { getFieldDecorator } = this.props.form;
    const dateFormat = "YYYY/MM/DD";

    return (
      <>
        <Modal
          visible={modalVisible}
          title={editItem ? "Update Business" : "Add Business"}
          onCancel={this.handleCancel}
          footer={[
            <Button
              key="submit"
              type="primary"
              onClick={e => this.handleSubmit(e)}
            >
              {editItem ? "Update" : "Add"}
            </Button>
          ]}
        >
          <Form layout={formLayout} onSubmit={this.handleSubmit}>
            <FormItem label="Business Name" {...formItemLayout}>
              {getFieldDecorator("businessName", {
                initialValue: editItem ? editItem.businessName : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input disabled={editItem ? true : false} />)}
            </FormItem>

            <FormItem label="Address" {...formItemLayout}>
              {getFieldDecorator("address", {
                initialValue: editItem ? editItem.address : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem label="City" {...formItemLayout}>
              {getFieldDecorator("city", {
                initialValue: editItem ? editItem.city : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem label="Country" {...formItemLayout}>
              {getFieldDecorator("country", {
                initialValue: editItem ? editItem.country : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem label="State/ District" {...formItemLayout}>
              {getFieldDecorator("stateDistrict", {
                initialValue: editItem ? editItem.stateDistrict : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input />)}
            </FormItem>

            <FormItem label="Number of Employees" {...formItemLayout}>
              {getFieldDecorator("noOfEmployees", {
                initialValue: editItem ? editItem.noOfEmployees : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<InputNumber style={{ width: "100%" }} />)}
            </FormItem>

            <FormItem label="Annual Turnover" {...formItemLayout}>
              {getFieldDecorator("annualTurnover", {
                initialValue: editItem ? editItem.annualTurnover : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<InputNumber style={{ width: "100%" }} />)}
            </FormItem>

            <FormItem label="Registration Number" {...formItemLayout}>
              {getFieldDecorator("registrationNumber", {
                initialValue: editItem ? editItem.registrationNumber : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<InputNumber style={{ width: "100%" }} />)}
            </FormItem>

            <FormItem label="Registration Date" {...formItemLayout}>
              {getFieldDecorator("registerDate", {
                initialValue: editItem ? moment(editItem.registerDate) : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(
                <DatePicker format={dateFormat} className="gx-mb-3 gx-w-100" />
              )}
            </FormItem>
            <FormItem label="Business Start" {...formItemLayout}>
              {getFieldDecorator("businessStart", {
                initialValue: editItem ? moment(editItem.businessStart) : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(
                <DatePicker format={dateFormat} className="gx-mb-3 gx-w-100" />
              )}
            </FormItem>
            <FormItem label="License Start Date" {...formItemLayout}>
              {getFieldDecorator("licenseStartDate", {
                initialValue: editItem ? moment(editItem.licenseStartDate) : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(
                <DatePicker format={dateFormat} className="gx-mb-3 gx-w-100" />
              )}
            </FormItem>
            <FormItem label="License End Date" {...formItemLayout}>
              {getFieldDecorator("licenseEndDate", {
                initialValue: editItem ? moment(editItem.licenseEndDate) : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(
                <DatePicker format={dateFormat} className="gx-mb-3 gx-w-100" />
              )}
            </FormItem>

            <FormItem label="Telephone" {...formItemLayout}>
              {getFieldDecorator("telephone", {
                initialValue: editItem ? editItem.telephone : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<InputNumber style={{ width: "100%" }} />)}
            </FormItem>

            <FormItem label="Email" {...formItemLayout}>
              {getFieldDecorator("email", {
                initialValue: editItem ? editItem.email : "",
                rules: [
                  {
                    type: "email",
                    message: "The input is not valid E-mail!"
                  },
                  { required: true, message: "Field should not empty!" }
                ]
              })(<Input type="email" />)}
            </FormItem>

            <FormItem label="Web" {...formItemLayout}>
              {getFieldDecorator("web", {
                initialValue: editItem ? editItem.web : "",
                rules: [{ required: true, message: "Field should not empty!" }]
              })(<Input />)}
            </FormItem>

            <FormItem label="Social Media" {...formItemLayout}>
              {getFieldDecorator("socialMedia", {
                initialValue: editItem ? editItem.socialMedia : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input />)}
            </FormItem>

            <FormItem label="Currency" {...formItemLayout}>
              {getFieldDecorator("currency", {
                initialValue: editItem ? editItem.currency : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input />)}
            </FormItem>
          </Form>
        </Modal>
      </>
    );
  }
}

export default Form.create()(AddModal);
