import React, { Component } from "react";
import { Button, Modal } from "antd";
import EditableTable from "./userTable";
import { connect } from "react-redux";
import {
    updateUser,
    addUser
  } from "../../../appRedux/actions/Business";

class UserModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formLayout: "horizontal"
    };
  }

  handleChange= (values) => {
    console.log('values')
  }


  handleCancel = () => {
    this.props.modalHandler(false);
  };

  handleUpdate = (info) => {
    this.props.updateUser(info);
  }

  handleNew = (info) => {
      this.props.addUser(info);
  }

  render() {
    const { userModalVisible, companyDetails, business } = this.props;
    console.log("users: ", companyDetails, business);
    return (
      <>
        <Modal
          visible={userModalVisible}
          title={
            "Manage Users - " +
            (companyDetails == null ? "" : companyDetails.businessName)
          }
          onCancel={this.handleCancel}
          footer={[
            <Button key="Cancel" type="primary" onClick={this.handleCancel}>
              Cancel
            </Button>
          ]}
         width="50%"
        >
         {business && business.businessUsers && <EditableTable businessUsers={business.businessUsers} handleChange={this.handleChange} handleUpdate={this.handleUpdate} handleNew={this.handleNew} companyDetails={companyDetails}/>}
        </Modal>
      </>
    );
  }
}

const mapStateToProps = state => ({
  business: state.business
});


const mapDispatchToProps = dispatch => {
    return {
        updateUser: (info) => {
        dispatch(updateUser(info));
      },
      addUser: (info) => {
        dispatch(addUser(info));
      },
    };
  };


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserModal);
