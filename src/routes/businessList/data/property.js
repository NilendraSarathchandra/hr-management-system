import { Button, Form, Modal, Table, Input } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getPropertyListMetaData,
  updateCustomerPropertyList
} from "../../../appRedux/actions/Customer";

const FormItem = Form.Item;

class Property extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formLayout: "horizontal"
    };
  }

  componentDidMount() {
    const { getPropertyListMetaData, customerInfo } = this.props;
    getPropertyListMetaData(customerInfo.customerType.id);
  }

  state = {
    confirmDirty: false,
    autoCompleteResult: []
  };

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  handleCancel = () => {
    const { resetFields } = this.props.form;
    resetFields();
    this.props.modalHandler(false, "PR");
  };

  handleSubmit = e => {
    e.preventDefault();
    const { resetFields } = this.props.form;
    const { updateCustomerPropertyList, customerInfo } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let propertyList = [];
        Object.keys(values).map(key => {
          propertyList = [
            ...propertyList,
            {
              propertyValue: values[key],
              customerId: customerInfo.id,
              languageI: 1,
              propertyTypeId: key
            }
          ];
        });
        updateCustomerPropertyList({ propertyList });
        resetFields();
        this.props.modalHandler(true, "AT");
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      propertyModal,
      loading,
      handleCancel,
      handleOk,
      customer
    } = this.props;
    const { current } = this.state;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    return (
      <>
        <Modal
          visible={propertyModal}
          title="Add Client Details"
          onOk={() => handleOk()}
          onCancel={this.handleCancel}
          footer={[
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={e => this.handleSubmit(e)}
            >
              Next
            </Button>
          ]}
        >
          {customer && customer.propertyList && (
            <Form onSubmit={this.handleSubmit}>
              {customer &&
                customer.propertyList &&
                customer.propertyList.map(item => {
                  return (
                    <FormItem label={item.propertyName} {...formItemLayout}>
                      {getFieldDecorator([item.id.toString()], {
                        rules: [
                          {
                            required: true,
                            message: "Field should not be empty!"
                          }
                        ]
                      })(<Input />)}
                    </FormItem>
                  );
                })}
            </Form>
          )}
        </Modal>
      </>
    );
  }
}

const mapStateToProps = state => ({
  customer: state.customer
});

const mapDispatchToProps = dispatch => {
  return {
    getPropertyListMetaData: customerTypeId => {
      dispatch(getPropertyListMetaData(customerTypeId));
    },
    updateCustomerPropertyList: propertyList => {
      dispatch(updateCustomerPropertyList(propertyList));
    }
  };
};

const PropertyForm = Form.create()(Property);
export default connect(mapStateToProps, mapDispatchToProps)(PropertyForm);
