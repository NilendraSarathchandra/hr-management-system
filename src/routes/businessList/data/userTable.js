import React from "react";
import { Table, Input, InputNumber, Popconfirm, Form, Button, message } from "antd";

const EditableContext = React.createContext();

class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === "number") {
      return <InputNumber style={{ width: "100%" }} min="0" />;
    }
    return <Input />;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: [
                {
                  required: true,
                  message: `Please Input ${title}!`
                }
              ],
              initialValue: record[dataIndex]
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}

class EditableTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = { data: null, editingKey: "" };
    this.columns = [
      {
        title: "Name",
        dataIndex: "name",
        editable: true
      },
      {
        title: "Email",
        dataIndex: "email",
        editable: true
      },
      {
        title: "Password",
        dataIndex: "password",
        editable: true
      },
      {
        title: "Mobile",
        dataIndex: "mobile",
        editable: true
      },
      {
        title: "operation",
        dataIndex: "operation",
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record.id)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                title="Sure to cancel?"
                onConfirm={() => this.cancel(record.id)}
              >
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <a
              disabled={editingKey !== ""}
              onClick={() => this.edit(record.id)}
            >
              Edit
            </a>
          );
        }
      }
    ];
  }

  isEditing = record => record.id === this.state.editingKey;

  cancel = () => {
    this.setState({ data: null, editingKey: "" });
  };

  save(form, key) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }

      this.setState({ ...this.state, editingKey: "", data: null });

      const { handleUpdate, handleNew, companyDetails } = this.props;
      row.userId = key;
      if (key == "new") {
        delete row.userId;
        row.companyId = companyDetails.businessMasterId;
        handleNew(row);
        message.success("Record Added");
        return;
      }
      handleUpdate(row);
      message.success("Record Updated");
    });
  }

  handleNew() {
    this.setState({
      ...this.state,
      data: {
        name: "",
        email: "",
        password: "",
        mobile: "",
        id: "new"
      },
      editingKey: "new"
    });
  }

  edit(key) {
    this.setState({ ...this.state, editingKey: key });
  }

  render() {
    const { handleChange } = this.props;

    let data =
      this.props.businessUsers === undefined ? [] : this.props.businessUsers;

    if (this.state.data !== null) {
      data = [...data, this.state.data];
    }

    const components = {
      body: {
        cell: EditableCell
      }
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === "mobile" ? "number" : "text",
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });
    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        handleChange(selectedRows);
      },
      getCheckboxProps: record => ({
        disabled: record.maxLimit === undefined,
        name: record.dataIndex
      })
    };
    return (
      <>
        <Button
          type="primary"
          className="gx-text-right"
          onClick={() => this.handleNew()}
        >
          Add
        </Button>
        <EditableContext.Provider value={this.props.form}>
          <Table
            className="gx-table-responsive"
            components={components}
            bordered
            dataSource={data}
            columns={columns}
            rowClassName="editable-row"
            //rowSelection={rowSelection}
            pagination={{
              onChange: this.cancel
            }}
          />
        </EditableContext.Provider>
      </>
    );
  }
}

const EditableFormTable = Form.create()(EditableTable);
export default EditableFormTable;
