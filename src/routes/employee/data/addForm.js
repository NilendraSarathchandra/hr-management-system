import React, { Component } from "react";
import { Form, Button, Modal, Select, Input, message, Divider, } from "antd";
import { connect } from "react-redux";
import {
    getPayrolItem, addAllEmployees, updateAllEmployees,
    getAllEmployees, reset
} from "../../../appRedux/actions/Employee";


const FormItem = Form.Item;
const Option = Select.Option;
class AddEmployee extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            formLayout: "horizontal",
            fieldList: [0, 1],
            payrollItems: [],
            payrollItem: [],
            emp: null
        };
    }

    handleCancel = () => {
        const { resetFields } = this.props.form;
        resetFields();
        this.props.modalHandler(false);
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const {  updateAllEmployees, editItem, updateData, getAllEmployees, selectedMonth } = this.props;
        const { resetFields } = this.props.form;
        this.props.form.validateFields((err, values) => {
            const payrollMasterDataList = Object.keys(values).filter(r => {
                if (r !== 'employee' && values[r] && r.includes('amount')) {
                    return r;
                }

            }).map(m => {
                const number = m.split('amount').join('');
                const editContd = editItem.data.payrollItem.filter(f => {
                    return f.hrEmployeePayrollMasterId === +number;
                })
                const data = {
                    employeePayrollId: parseInt(number),
                    month: values['month' + number],
                    amount: values['amount' + number],
                    editable: editContd[0].editable
                }
                return data;

            }).filter(f => {
                if (f.editable) {
                    return f;
                }
            });
            const filterReq = payrollMasterDataList.filter

           if (!err && payrollMasterDataList.length > 0 && editItem) {
                console.log(updateData, 'update success')
                const req = {
                    employeePayrollDataList: payrollMasterDataList
                }
                updateAllEmployees(req);
                this.handleCancel();
                setTimeout(() => {
                    getAllEmployees(selectedMonth);
                }, 2000);

            }
        });
    }

    getOptions = res => {
        const { resetFields } = this.props.form;
        resetFields();
        const { employeeList } = this.props;
        if (employeeList) {
            const val = employeeList.filter(val => {
                return val.id === res.value
            });
            const req = {
                positionMasterId: val[0].hrPositionMaster.id
            }
            const { getPositionMaster } = this.props;
            getPositionMaster(req);
        }

    };

    validator = (rule, value, callback) => {
        const { payrollItem } = this.state;
        const id = parseInt(rule.field.split('amount').join(''));
        const fieldValue = parseFloat(value);
        const val = payrollItem.filter(val => {
            return val.hrEmployeePayrollMasterId === id
        });
        if (value) {
            if (val.length > 0) {
                if (parseFloat(val[0].maxLimit) < fieldValue) {
                    return Promise.reject('Max value should < ' + val[0].maxLimit);
                } else if (parseFloat(val[0].minLimit) > fieldValue) {
                    return Promise.reject('Min value should < ' + val[0].minLimit);
                } else {
                    return Promise.resolve()
                }

            } else {
                return Promise.resolve();
            }
        } else {
            return Promise.reject('This field is required !');
        }
    }

    getOptions = res => {
        const { resetFields } = this.props.form;
        resetFields();
        const { employeeList } = this.props;
        if (employeeList) {
            const val = employeeList.filter(val => {
                return val.employeeMasterId === res.value
            });
            this.setState({ payrollItem: val[0].hrEmployeePayrollFieldList });
        }

    };

    render() {
        const { modalVisible, employeeList, months, editItem, addData, updateData, reset, selectedMonth } = this.props;
        const { payrollItem } = this.state;
        const { getFieldDecorator } = this.props.form;
        const { formLayout, fieldList } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { xs: 24, sm: 6 },
            wrapperCol: { xs: 24, sm: 18 },
        } : null;

        if (this.props.employee && this.props.employee.updateData && this.props.employee.updateData.success) {
            message.success('Successfully updated');
            reset();
        } else if (this.props.employee && this.props.employee.updateData && !this.props.employee.updateData.success) {
            message.error('Updating Fail');
            reset()
        }
        return (
            <>
                <Modal
                    visible={modalVisible}
                    title={!editItem ? "Create" : "Update"}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button
                            key="submit"
                            type="primary"
                            onClick={e => this.handleSubmit(e)}>
                            {editItem ? 'Update' : 'Save'}</Button>
                    ]}>
                    <Form layout={formLayout} onSubmit={this.handleSubmit}>

                        <FormItem
                            key={'employee'}
                            label="Employee"
                            {...formItemLayout}
                        >
                            {getFieldDecorator('employeePayrollMasterId', {
                                initialValue: editItem ? editItem.data.empId : "",
                                rules: [{ required: true, message: 'Employee should select!' }],
                            })(
                                <Select placeholder="Please select a employee !" key={'emp2'} onChange={value =>
                                    this.getOptions({ value: value })
                                } disabled={editItem ? true : false}>
                                    {employeeList.map((option, index) => {
                                        return (
                                            <Option key={index} value={option.employeeMasterId}>
                                                {option.customerName}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        {editItem ? <><FormItem
                            key={'employeeNo'}
                            label="Employee No"
                            {...formItemLayout}
                        >
                            <div>{editItem.data.empId}</div>
                        </FormItem>
                        <FormItem
                            key={'employeeNNic'}
                            label="NIC"
                            {...formItemLayout}
                        >
                            <div>{editItem.data.nicNo}</div>
                        </FormItem></>:""

                        }
                        
                        {!editItem && payrollItem.map((item, index) => (<>
                            <Divider orientation="left">{item.payrollItemName}</Divider>
                            <FormItem
                                key={'month' + item.hrEmployeePayrollMasterId}
                                label="Month"
                                {...formItemLayout}
                            >
                                {getFieldDecorator('month' + item.hrEmployeePayrollMasterId, {
                                    initialValue: selectedMonth ? selectedMonth : "",
                                    rules: [{ required: true, message: 'Month should be select!' }],
                                })(
                                    <Select placeholder="Please select a month !" key={'month1'} disabled={selectedMonth ? true : false}>
                                        {months.map((option, index) => {
                                            return (
                                                <Option key={index} value={option.id}>
                                                    {option.month}
                                                </Option>
                                            );
                                        })}
                                    </Select>
                                )}
                            </FormItem>
                            <FormItem
                                key={'amount' + item.hrEmployeePayrollMasterId}
                                label="Amount"
                                {...formItemLayout}
                            >
                                {getFieldDecorator('amount' + item.hrEmployeePayrollMasterId, {
                                    rules: [{ validator: this.validator },],
                                })(
                                    <Input />
                                )}
                            </FormItem>
                        </>))}
                        {editItem && editItem.data.payrollItem.map((item, index) => (<>
                            <Divider orientation="left">{item.payrollItemName}</Divider>
                            {item.editable ? <FormItem
                                key={'minmax' + item.hrEmployeePayrollMasterId}
                                label="Min Max Values"
                                {...formItemLayout}
                            >
                                <div><span>Min: {item.minLimit} LKR </span>/<span>Max: {item.maxLimit} LKR</span></div>
                            </FormItem> : ''}
                            
                            <FormItem
                                key={'month' + item.hrEmployeePayrollMasterId}
                                label="Month"
                                {...formItemLayout}
                            >
                                {getFieldDecorator('month' + item.hrEmployeePayrollMasterId, {
                                    initialValue: editItem ? item.month.id : "",
                                    rules: [{ required: true, message: 'Month should be select!' }],
                                })(
                                    <Select placeholder="Please select a month !" key={'month1'} disabled={selectedMonth ? true : false} >
                                        {months.map((option, index) => {
                                            return (
                                                <Option key={index} value={option.id}>
                                                    {option.month}
                                                </Option>
                                            );
                                        })}
                                    </Select>
                                )}
                            </FormItem>
                            <FormItem
                                key={'amount' + item.hrEmployeePayrollMasterId}
                                label="Amount"
                                {...formItemLayout}
                            >
                                {getFieldDecorator('amount' + item.hrEmployeePayrollMasterId, {
                                    initialValue: item ? item.amount : "",
                                    rules: [{ validator: this.validator }],
                                })(
                                    <Input type="number" disabled={!item.editable ? true : false} />
                                )}
                            </FormItem>
                        </>))}
                    </Form>
                </Modal>
            </>
        );
    }
}

const FormModal = Form.create()(AddEmployee);

const mapStateToProps = state => ({
    employee: state.employee
});

const mapDispatchToProps = dispatch => {
    return {
        getPayrolItem: (req) => {
            dispatch(getPayrolItem(req));
        },
        addAllEmployees: (req) => {
            dispatch(addAllEmployees(req));
        },
        updateAllEmployees: (req) => {
            dispatch(updateAllEmployees(req));
        },
        getAllEmployees: (req) => {
            dispatch(getAllEmployees(req));
        },
        reset: () => {
            dispatch(reset());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormModal);