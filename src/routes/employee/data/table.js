import { Table, Input, Button, Icon } from "antd";
import React from "react";
import Highlighter from "react-highlight-words";

class DataTable extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => {
              this.searchInput = node;
            }}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() =>
              this.handleSearch(selectedKeys, confirm, dataIndex)
            }
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8 }}
          >
            Search
        </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
        </Button>
        </div>
      ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record !== undefined
        ? record[dataIndex]
          .toString()
          .toLowerCase()
          .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
          text
        )
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  render() {
    const { handleEdit } = this.props;
    const { tableData } = this.props;
    const formatTable = tableData.map(m => {
      const sub = m.payrollList.map(m1 => {
        return {
          month: m1.month.month, amount: m1.amount, maxLimit: m1.hrEmployeePayrollMaster.hrPayrollMaster.maxLimit,
          minLimit: m1.hrEmployeePayrollMaster.hrPayrollMaster.minLimit,
          itemName: m1.hrEmployeePayrollMaster.hrPayrollMaster.payrollItem.itemName
        }
      });
      console.log(sub)
      return Object.assign(m, { sub: sub });
    });
    console.log(formatTable);
    const columns = [
      {
        title: "Employee Number",
        dataIndex: "empNo",
        key: "empNo",
        sorter: (a, b) => a.empNo.length - b.empNo.length,
        ...this.getColumnSearchProps("empNo"),
      },
      {
        title: "Employee Name",
        dataIndex: "name",
        key: "name",
        sorter: (a, b) => a.name.length - b.name.length,
        ...this.getColumnSearchProps("name"),
      },
      {
        title: "Nic",
        dataIndex: "nicNo",
        key: "nicNo",
        sorter: (a, b) => a.nicNo.length - b.nicNo.length,
        ...this.getColumnSearchProps("nicNo")
      },
      {
        title: "Net Pay",
        dataIndex: "netPay",
        key: "netPay",
        sorter: (a, b) => a.netPay.length - b.netPay.length,
        ...this.getColumnSearchProps("netPay")
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <span className="gx-link" onClick={() => handleEdit(record)}>
              Edit
            </span>
          </span>
        )
      }
    ];

    const subColumn = [
    //   {
    //   title: "Month",
    //   dataIndex: "month",
    //   key: "month",
    //   sorter: (a, b) => a.month.length - b.month.length,
    //   ...this.getColumnSearchProps("month")
    // },
    // {
    //   title: "Maximum value (LKR)",
    //   dataIndex: "maxLimit",
    //   key: "maxLimit",
    //   sorter: (a, b) => a.maxLimit.length - b.maxLimit.length,
    //   ...this.getColumnSearchProps("maxLimit")
    // },
    {
      title: "Item Name",
      dataIndex: "itemName",
      key: "itemName",
      sorter: (a, b) => a.itemName.length - b.itemName.length,
      ...this.getColumnSearchProps("itemName")
    },
    {
      title: "Amount (LKR)",
      dataIndex: "amount",
      key: "amount",
      sorter: (a, b) => a.amount.length - b.amount.length,
      ...this.getColumnSearchProps("amount")
    }];

    return (
      <Table
        pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15'] }}
        className="gx-table-responsive"
        columns={columns}
        dataSource={formatTable}
        expandedRowRender={record => (
          <>
            <Table
              columns={subColumn}
              pagination={false}
              dataSource={record.sub}
            >
            </Table>
          </>
        )}
      />
    );

  }
}

export default DataTable;
