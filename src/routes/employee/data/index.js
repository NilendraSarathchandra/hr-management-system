import React from "react";
import { Col, Row } from "antd";

import Employee from "./employee";

const Employees = () => {
  return (
    <Row>
      <Col span={24}>
        <Employee />
      </Col>
    </Row>
  );
};

export default Employees;