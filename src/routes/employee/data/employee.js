import { Col, Input, Select, Radio, Form, Card, Button , message} from "antd";
import React from "react";
import { connect } from "react-redux";
import {
    getAllEmployees, getAllmonths, getEmployeeList, getPayrolItem, addAllEmployees , reset
} from "../../../appRedux/actions/Employee";
import DataTable from "./table"
import AddEmployee from "./addForm"

const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 3 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
        md: { span: 16 },
        lg: { span: 12 },
    },
};
class Employee extends React.Component {
    state = {
        loading: false,
        modalVisible: false,
        search: '',
        editItem: null,
        employeeList: [],
        selectedMonth: null
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { getAllEmployees, getAllmonths, getEmployeeList } = this.props;
        const months = this.props.employee.months ? this.props.employee.months : [];
        if (months.length > 0) {
            this.setState({
                selectedMonth: months[0].id
            });
            getAllEmployees(months[0].id);
        }
        getAllmonths();
        getEmployeeList();
    }

    showModal = (status) => {
        this.setState({
            modalVisible: status,
            editItem: null
        });
    };

    create = (status) => {
        const { addAllEmployees, getAllEmployees } = this.props
        const { selectedMonth } = this.state;
        if (selectedMonth) {
            const req = {
                monthId: selectedMonth
            }
            addAllEmployees(req);
            setTimeout(() => {
                getAllEmployees(selectedMonth);
            }, 2000);
        }
    };

    handleEdit = (item) => {
        let empId;
        const editItem = item.payrollList.map(m => {
            empId = m.employeeMaster.id;
            return {
                hrEmployeePayrollMasterId: m.id,
                payrollMasterId: m.hrEmployeePayrollMaster.hrPayrollMaster.id,
                payrollItemName: m.hrEmployeePayrollMaster.hrPayrollMaster.payrollItem.itemName,
                amount: m.amount,
                maxLimit: m.hrEmployeePayrollMaster.hrPayrollMaster.maxLimit,
                minLimit: m.hrEmployeePayrollMaster.hrPayrollMaster.minLimit,
                month: m.month,
                editable: m.editable
            }
        });
        const data = { empId: empId, payrollItem: editItem, nicNo: item.nicNo }
        this.setState({ ...this.state, editItem: { data }, modalVisible: true });
    };

    getOptions = (value) => {
        const { getAllEmployees } = this.props;
        getAllEmployees(value);
        this.setState({
            selectedMonth: value
        });
    };

    render() {
        const employees = (this.props.employee && this.props.employee.employees.dataList) ? this.props.employee.employees.dataList : [];
        const createEnabled = (this.props.employee && this.props.employee.employees.createEnabled) ? this.props.employee.employees.createEnabled : null;
        const employeeList = this.props.employee.employeeList ? this.props.employee.employeeList : [];
        const months = this.props.employee.months ? this.props.employee.months : [];
        const { modalVisible, editItem, selectedMonth } = this.state;
        console.log(this.state)
        const tableData = employees;
        const { reset } = this.props;
        if (this.props.employee && this.props.employee.addData && this.props.employee.addData.success) {
            message.success('Successfully added');
            reset();
        } else if (this.props.employee && this.props.employee.addData && !this.props.employee.addData.success) {
            message.error('Adding Fail');
            reset();
        }
        return (
            <>
                <div>
                    <Card title="Employee">
                        <Form >
                            <FormItem
                                key={'month'}
                                label="Month"
                                {...formItemLayout}
                            >
                                {months.length > 0 ? <>
                                    <Select defaultValue={months[0].id} placeholder="Please select a month !" onChange={value =>
                                        this.getOptions(value)
                                    } key={'month1'}>
                                        {months.map((option, index) => {
                                            return (
                                                <Option key={index} value={option.id}>
                                                    {option.month}
                                                </Option>
                                            );
                                        })}
                                    </Select></> : ''}

                            </FormItem>
                            {createEnabled ? <> <FormItem
                                label="Add New"
                                {...formItemLayout}
                            >
                                <Button type="primary" className="gx-text-right" onClick={() => this.create(true)}>Create</Button>
                            </FormItem></> : ''}


                        </Form>
                        {tableData ? <DataTable tableData={tableData} handleEdit={this.handleEdit} /> : ''}
                        <AddEmployee
                            selectedMonth={selectedMonth}
                            modalVisible={modalVisible}
                            editItem={editItem && editItem}
                            employeeList={employeeList}
                            months={months}
                            modalHandler={(state) => this.showModal(state)} />
                    </Card>
                </div>

            </>
        );
    }
}


const mapStateToProps = state => ({
    employee: state.employee
});

const mapDispatchToProps = dispatch => {
    return {
        getAllEmployees: (month) => {
            dispatch(getAllEmployees(month));
        },
        getAllmonths: () => {
            dispatch(getAllmonths());
        },
        getEmployeeList: () => {
            dispatch(getEmployeeList());
        },
        getPayrolItem: (req) => {
            dispatch(getPayrolItem(req));
        },
        addAllEmployees: (req) => {
            dispatch(addAllEmployees(req));
        },
        reset: () => {
            dispatch(reset());
        }

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Employee);
