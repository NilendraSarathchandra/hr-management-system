import React from "react";
import { Button, Card, Modal, Select, Input } from "antd";
import PayrolItemForm from "./PayrolItemForm";

const Option = Select.Option;

class AddPayrollItem extends React.Component {
  state = {
    loading: false,
    visible: false
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  };
  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleChange(value) {
    console.log(`selected ${value}`);
  }

  handleBlur() {
    console.log("blur");
  }

  handleFocus() {
    console.log("focus");
  }

  render() {
    const { visible, loading } = this.state;
    return (
      <>
        <Button type="primary" onClick={this.showModal}>
          Add
        </Button>

        <PayrolItemForm loading={loading} visible={visible} handleOk={this.handleOk} handleCancel={this.handleCancel} />
      </>
    );
  }
}

export default AddPayrollItem;
