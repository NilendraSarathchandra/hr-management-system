import React, { Component } from "react";
import {
  AutoComplete,
  Button,
  Card,
  Cascader,
  Checkbox,
  Col,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Tooltip,
  Modal
} from "antd";
import Dropdown from "./Dropdown";

const FormItem = Form.Item;
const Option = Select.Option;

class PayrolItem extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: []
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      console.log("values ", values.category);
      if (!err) {
        console.log("Received values of form: ", values);
      }
    });
  };

  setFieldsValue = obj => {
    this.props.form.setFieldsValue({ [obj.name]: obj.value });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible, loading, handleCancel, handleOk } = this.props;
    const categorySource = [
      { key: "basicSalary", name: "Basic Salary" },
      { key: "incentive", name: "Incentive" },
      { key: "taxDeduction", name: "Tax Deduction" }
    ];
    const payTypeSource = [
      { key: "1", name: "1" },
      { key: "2", name: "2" },
      { key: "3", name: "3" },
      { key: "4", name: "4" }
    ];
    const typesAllowed = [
      { key: "yes", name: "Yes" },
      { key: "no", name: "No" }
    ];
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    return (
      <>
        <Modal
          visible={visible}
          title="Add New Payroll Item"
          onOk={() => handleOk()}
          onCancel={() => handleCancel()}
          footer={[
            <Button key="back" onClick={() => handleCancel()}>
              Back
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={e => this.handleSubmit(e)}
            >
              Save
            </Button>
          ]}
        >
          <Form onSubmit={this.handleSubmit}>
            <FormItem {...formItemLayout} label="Item Name">
              {getFieldDecorator("itemName", {
                rules: [
                  {
                    required: true,
                    message: "Please enter name!"
                  }
                ]
              })(<Input id="itemName" />)}
            </FormItem>
            <FormItem {...formItemLayout} label="Category">
              {getFieldDecorator("category", {
                rules: [
                  {
                    required: true,
                    message: "Please enter a category!"
                  }
                ]
              })(
                <Dropdown
                  options={categorySource}
                  setFieldsValue={this.setFieldsValue}
                  fieldName={"category"}
                />
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Pay Type">
              {getFieldDecorator("payType", {
                rules: [
                  {
                    required: true,
                    message: "Please enter a pay type!"
                  }
                ]
              })(
                <Dropdown
                  options={payTypeSource}
                  setFieldsValue={this.setFieldsValue}
                  fieldName={"payType"}
                />
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Allow Statutory">
              {getFieldDecorator("allowStatutory", {
                rules: [
                  {
                    required: true,
                    message: "Please enter a statutory!"
                  }
                ]
              })(
                <Dropdown
                  options={typesAllowed}
                  setFieldsValue={this.setFieldsValue}
                  fieldName={"allowStatutory"}
                />
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Allow Tax">
              {getFieldDecorator("allowTax", {
                rules: [
                  {
                    required: true,
                    message: "Please enter a Tax!"
                  }
                ]
              })(
                <Dropdown
                  options={typesAllowed}
                  setFieldsValue={this.setFieldsValue}
                  fieldName={"allowTax"}
                />
              )}
            </FormItem>
          </Form>
        </Modal>
      </>
    );
  }
}

const PayrolItemForm = Form.create()(PayrolItem);
export default PayrolItemForm;
