import React from "react";
import { Card, Divider, Icon, Table, Button } from "antd";
import AddPayrollItem from './AddPayrolItem';
const columns = [
  {
    title: "Category",
    dataIndex: "category",
    key: "category",
    render: text => <span className="gx-link">{text}</span>
  },
  {
    title: "Item Name",
    dataIndex: "itemName",
    key: "itemName"
  },
  {
    title: "Pay Type",
    dataIndex: "payType",
    key: "payType"
  },
  {
    title: "Allow Statutory",
    dataIndex: "allowStatutory",
    key: "allowStatutory"
  },
  {
    title: "Allow Tax",
    dataIndex: "allowTax",
    key: "allowTax"
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status"
  },
  {
    title: "Action",
    key: "action",
    render: (text, record) => (
      <span>
        <span className="gx-link">Action 一 {record.name}</span>
        <Divider type="vertical" />
        <span className="gx-link">Delete</span>
        <Divider type="vertical" />
        <span className="gx-link ant-dropdown-link">
          More actions <Icon type="down" />
        </span>
      </span>
    )
  }
];

const data = [
  {
    key: "1",
    category: "Basic Salary",
    itemName: "Basic salary",
    payType: 1,
    allowStatutory: "Yes",
    allowTax: "Yes",
    status: "Active"
  },
  {
    key: "2",
    category: "Incentive",
    itemName: "Travelling incentive",
    payType: 3,
    allowStatutory: "No",
    allowTax: "No",
    status: "Active"
  },
  {
    key: "3",
    category: "Statutary Deduction",
    itemName: "E.T.F.",
    payType: 5,
    allowStatutory: "Yes",
    allowTax: "Yes",
    status: "Active"
  }
];

const Simple = () => {
  return (
    <Card title="Payroll Items">
       <AddPayrollItem  />
      <Table
        className="gx-table-responsive"
        columns={columns}
        dataSource={data}
      />
    </Card>
  );
};

export default Simple;
