import React from "react";
import { Col, Row } from "antd";

import Login from "./login";

const Auth = () => {
  return (
    <Row>
      <Col span={24}>
        <Login />
      </Col>
    </Row>
  );
};

export default Auth;