import React from "react";
import { Button, Checkbox, Form, Icon, Input, message } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import IntlMessages from "util/IntlMessages";
import {
    customUserSignIn
} from "../../../appRedux/actions/Auth";

const FormItem = Form.Item;

class Login extends React.Component {

    handleSubmit = (e) => {
        e.preventDefault();
        const { customUserSignIn } = this.props;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const req = {
                    userName: values.userName,
                    password: values.password
                }
                customUserSignIn(req);
            }
        });
    };

    componentDidUpdate() {
        if (this.props.showMessage) {
            setTimeout(() => {
                this.props.hideMessage();
            }, 100);
        }
        if (this.props.authUser !== null) {
            // this.props.history.push('/');
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const { showMessage, loader, alertMessage } = this.props;

        return (
            <div className="gx-app-login-wrap">
                <div className="gx-app-login-container">
                    <div className="gx-app-login-main-content">
                        <div className="gx-app-logo-content">
                            <div className="gx-app-logo-content-bg">
                                {/* <img src="https://via.placeholder.com/272x395" alt='Neature' /> */}
                            </div>
                            <div className="gx-app-logo-wid">
                                <h1><IntlMessages id="app.userAuth.signIn" /></h1>
                                <p><IntlMessages id="app.userAuth.bySigning" /></p>
                                <p><IntlMessages id="app.userAuth.getAccount" /></p>
                            </div>
                            <div className="gx-app-logo">
                                {/* <img alt="example" src={require("assets/images/logo.png")} /> */}
                            </div>
                        </div>
                        <div className="gx-app-login-content">
                            <Form onSubmit={this.handleSubmit} className="gx-signin-form gx-form-row0">

                                <FormItem>
                                    {getFieldDecorator('userName', {
                                        initialValue: "0713932962",
                                        rules: [{
                                            required: true, message: 'The input is not valid E-mail!',
                                        }],
                                    })(
                                        <Input placeholder="User Name" />
                                    )}
                                </FormItem>
                                <FormItem>
                                    {getFieldDecorator('password', {
                                        initialValue: "1234",
                                        rules: [{ required: true, message: 'Please input your Password!' }],
                                    })(
                                        <Input type="password" placeholder="Password" />
                                    )}
                                </FormItem>

                                <FormItem>
                                    <Button type="primary" className="gx-mb-0" htmlType="submit">
                                        <IntlMessages id="app.userAuth.signIn" />
                                    </Button>
                                </FormItem>
                            </Form>
                        </div>

                        {/* {loader ?
                            <div className="gx-loader-view">
                                <CircularProgress />
                            </div> : null}
                        {showMessage ?
                            message.error(alertMessage.toString()) : null} */}
                    </div>
                </div>
            </div>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(Login);

const mapStateToProps = state => ({
  });

const mapDispatchToProps = dispatch => {
    return {
        customUserSignIn: (req) => {
            dispatch(customUserSignIn(req));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm);
