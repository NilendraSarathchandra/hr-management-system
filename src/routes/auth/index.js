import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import asyncComponent from "util/asyncComponent";


const Boarding = ({match}) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/login`}/>
    <Route path={`${match.url}/login`} component={asyncComponent(() => import('./data'))}/>
  </Switch>
);

export default Boarding;