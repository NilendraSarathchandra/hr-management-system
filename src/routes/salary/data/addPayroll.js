import React, { Component } from "react";
import { Form, Button, Modal, Select, Input, message, Spin,  } from "antd";
import { getPositionMaster, addEmployeePayrolMaster, updateEmployeePayrolMaster, getAllEmployeePayroll, reset } from "../../../appRedux/actions/Salary";
import { connect } from "react-redux";
import Highlighter from "react-highlight-words";

const FormItem = Form.Item;
const Option = Select.Option;
class AddPayroll extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            formLayout: "horizontal",
            fieldList: [0, 1],
            payrollItems: []
        };
    }

    handleCancel = () => {
        const { resetFields } = this.props.form;
        resetFields();
        this.props.modalHandler(false);
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { addEmployeePayrolMaster, updateEmployeePayrolMaster, addSuccess, editItem, updateSuccess,getAllEmployeePayroll } = this.props;
        const { resetFields } = this.props.form;
        this.props.form.validateFields((err, values) => {
            const payrollMasterDataList = Object.keys(values).filter(r => {
                console.log(values[r])
                if (r !== 'employee' && values[r]) {
                    return r;
                }
                
            }).map(m => {
                // console.log(values[toString(m)],m)
                const data = {
                    employeePayrollMasterId: parseInt(m),
                    amount: values[m]
                }
                return data;
                
            });
            if (!err  && !editItem) {
                const payrollMasterDataList = Object.keys(values).filter(r => {
                    if (r !== 'employee' && values[r]) {
                        return r;
                    }
                }).map(m => {
                    const data = {
                        payrollMasterId: parseInt(m),
                        amount: values[m]
                    }
                    return data;
                    
                });
                const req = {
                    employeeMasterId: values.employee,
                    payrollMasterDataList: payrollMasterDataList
                }
                addEmployeePayrolMaster(req);
                this.handleCancel();
                setTimeout(() => {
                    getAllEmployeePayroll();
                }, 2000);
                
            } else if (!err && payrollMasterDataList.length > 0 && editItem){
                console.log(updateSuccess, 'update success')
                const req = {
                    employeePayrollMasterDataList: payrollMasterDataList
                }
                updateEmployeePayrolMaster(req);
                this.handleCancel();
                setTimeout(() => {
                    getAllEmployeePayroll();
                }, 2000);
                
            }
        });
    }

    getOptions = res => {
        const { resetFields } = this.props.form;
        resetFields();
        const { employeeList } = this.props;
        if (employeeList) {
            const val = employeeList.filter(val => {
                return val.id === res.value
            });
            const req = {
                positionMasterId: val[0].hrPositionMaster.id
            }
            const { getPositionMaster } = this.props;
            getPositionMaster(req);
        }

    };

    validator = (rule, value, callback) => {
        const { positionList } = this.props;
        const id = parseInt(rule.field);
        const fieldValue = parseFloat(value);
        const val = positionList.filter(val => {
            return val.id === id
        });
        if (value) {
            if (val.length > 0) {
                if (parseFloat(val[0].maxLimit) < fieldValue) {
                    return Promise.reject('Max value should < ' + val[0].maxLimit);
                } else if (parseFloat(val[0].minLimit) > fieldValue) {
                    return Promise.reject('Min value should < ' + val[0].minLimit);
                } else {
                    return Promise.resolve()
                }

            } else {
                return Promise.resolve();
            }
        } else {
            return Promise.reject('This field is required !');
        }
    }

    validator1 = (rule, value, callback) => {
        const { editItem } = this.props;
        const id = parseInt(rule.field);
        const fieldValue = parseFloat(value);
        const val = editItem.list.filter(val => {
            return val.hrEmployeePayrollMasterId === id
        });
        if (value) {
            if (val.length > 0) {
                if (parseFloat(val[0].maxLimit) < fieldValue) {
                    return Promise.reject('Max value should < ' + val[0].maxLimit);
                } else if (parseFloat(val[0].minLimit) > fieldValue) {
                    return Promise.reject('Min value should < ' + val[0].minLimit);
                } else {
                    return Promise.resolve()
                }

            } else {
                return Promise.resolve();
            }
        } else {
            return Promise.reject('This field is required !');
        }
    }

    render() {
        const { modalVisible, employeeList, positionList, editItem, addSuccess, updateSuccess, reset } = this.props;
        const { getFieldDecorator } = this.props.form;
        const { formLayout, fieldList } = this.state;
        const formItemLayout = formLayout === 'horizontal' ? {
            labelCol: { xs: 24, sm: 6 },
            wrapperCol: { xs: 24, sm: 18 },
        } : null;
        console.log(addSuccess, updateSuccess)
        if (this.props.updateSuccess && this.props.updateSuccess.success) {
            message.success('Successfully updated');
            reset();
        } else if(this.props.updateSuccess && !this.props.updateSuccess.success){
            message.error('Updating Fail');
            reset()
        }

        if (this.props.addSuccess && this.props.addSuccess.success) {
            message.success('Successfully added');
            reset();
        } else if(this.props.addSuccess && !this.props.addSuccess.success){
            message.error('Adding Fail');
            reset();
        }

        return (
            <>
                <Modal
                    visible={modalVisible}
                    title={!editItem ? "Add": "Update"}
                    onCancel={this.handleCancel}
                    footer={[
                        <Button
                            key="submit"
                            type="primary"
                            onClick={e => this.handleSubmit(e)}>
                            {editItem ? 'Update' : 'Save'}</Button>
                    ]}>
                    <Form layout={formLayout} onSubmit={this.handleSubmit}>
                        <FormItem
                            key={Math.random + ''}
                            label="Employee Name"
                            {...formItemLayout}
                        >
                            {getFieldDecorator('employee', {
                                initialValue: editItem ? editItem.item.id : ""

                            })(
                                <Select placeholder="Please select a option !" key={Math.random + ''} onChange={value =>
                                    this.getOptions({ value: value }) 
                                } disabled={editItem ? true : false}>
                                    {employeeList.map((option, index) => {
                                        return (
                                            <Option key={index} value={option.id}>
                                                {((option || {}).ciCustomer || {}).customerName}
                                            </Option>
                                        );
                                    })}
                                </Select>
                            )}
                        </FormItem>
                        {editItem && editItem.list.map((item, index) => (
                            <FormItem
                                key={item.hrEmployeePayrollMasterId}
                                label={item.payrollItemName}
                                {...formItemLayout}
                            >
                                {getFieldDecorator(item.hrEmployeePayrollMasterId + '', {
                                    initialValue: item ? item.amount : "",
                                    rules: [
                                        { validator: this.validator1 },

                                    ],
                                })(
                                    <Input type="number" />
                                )}
                            </FormItem>
                        ))}
                       
                        {!editItem && positionList.map((item, index) => (
                            
                            <FormItem
                                key={item.id}
                                label={(((item || {}).payrollItem || {})).itemName}
                                {...formItemLayout}
                            >
                                {getFieldDecorator(item.id + '', {
                                    initialValue: item ? item.amount : "",
                                    rules: [
                                        { validator: this.validator },

                                    ],
                                })(
                                    <Input type="number" />
                                )}
                            </FormItem>
                        ))}

                    </Form>
                </Modal>
            </>
        );
    }
}

const FormModal = Form.create()(AddPayroll);

const mapStateToProps = state => ({
    positionList: state.salary.positionList,
    addSuccess: state.salary.addSuccess,
    updateSuccess: state.salary.updateSuccess
});

const mapDispatchToProps = dispatch => {
    return {
        getPositionMaster: (req) => {
            dispatch(getPositionMaster(req));
        },
        addEmployeePayrolMaster: (req) => {
            dispatch(addEmployeePayrolMaster(req));
        },
        updateEmployeePayrolMaster: (req) => {
            dispatch(updateEmployeePayrolMaster(req));
        },
        getAllEmployeePayroll: () => {
            dispatch(getAllEmployeePayroll());
        },
        reset: () => {
            dispatch(reset());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormModal);