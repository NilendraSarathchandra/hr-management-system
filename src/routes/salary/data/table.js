import { Table, Input, Button, Icon } from "antd";
import React from "react";
import Highlighter from "react-highlight-words";

class DataTable extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record !== undefined
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      )
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  render() {
    const { handleEdit } = this.props;
    const columns = [
      {
        title: "Number",
        dataIndex: "employeeMasterId",
        key: "employeeMasterId",
        render: text => <span className="gx-link">{text}</span>
      },
      {
        title: "Name",
        dataIndex: "customerName",
        key: "customerName",
        sorter: (a, b) => a.customerName.length - b.customerName.length,
        ...this.getColumnSearchProps("customerName")
      },
      {
        title: "Position",
        dataIndex: "positionMasterName",
        key: "positionMasterName",
        sorter: (a, b) => a.positionMasterName.length - b.positionMasterName.length,
        ...this.getColumnSearchProps("positionMasterName")
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        render: (text, record) => (
          <span>
            <span className="gx-link" onClick={() => handleEdit(record)}>
              Edit
            </span>
          </span>
        )
      }
    ];
    const subColumn = [{
      title: "Payroll Item Name",
      dataIndex: "payrollItemName",
      key: "payrollItemName",
      sorter: (a, b) => a.payrollItemName.length - b.payrollItemName.length,
        ...this.getColumnSearchProps("payrollItemName")
    },
    {
      title: "Amount (LKR)",
      dataIndex: "amount",
      key: "amount",
      sorter: (a, b) => a.amount.length - b.amount.length,
        ...this.getColumnSearchProps("amount")
    }];
    console.log('object', this.props.tableData)
    return (
     
      <Table
        pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15'] }}
        className="gx-table-responsive"
        columns={columns}
        dataSource={this.props.tableData}
        expandedRowRender={record => (
          <>
            <Table
              columns={subColumn}
              pagination={false}
              dataSource={record.hrEmployeePayrollFieldList}
            >
            </Table>
          </>
        )}
      />
    );

  }
}

export default DataTable;
