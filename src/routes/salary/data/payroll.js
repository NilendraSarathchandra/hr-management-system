import { Col, Input, Select, Radio, Form, Card, Button } from "antd";
import React from "react";
import DataTable from "./table"
import AddPayroll from "./addPayroll"
import { connect } from "react-redux";
import {
    getAllEmployeePayroll, getEmployeeList, getPositionMaster
} from "../../../appRedux/actions/Salary";
const Search = Input.Search;
const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 3 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 },
        md: { span: 16 },
        lg: { span: 12 },
    },
};


class Payroll extends React.Component {
    state = {
        loading: false,
        modalVisible: false,
        search: ''
    };

    constructor(props) {
        super(props);
    }

    showModal = (status) => {
        const { metaData } = this.props;
        this.setState({
            modalVisible: status,
            metaData: metaData,
            editItem: null
        });
    };

    handleEdit = (item) => {
        console.log(item);
        const newItem = this.props.payroll.employeeList.filter(res => {
            return res.id === item.employeeMasterId;
        });
        const req = {
            positionMasterId: item.positionMasterId
        }
        const { getPositionMaster, payroll } = this.props;
        getPositionMaster(req);
        if (newItem.length > 0) {
            this.setState({ ...this.state, editItem: { item: newItem[0], list: item.hrEmployeePayrollFieldList }, modalVisible: true });
        }
    };

    onSearch = (value) => {

    };

    dataSet = (value) => {
        this.setState({
            serchCriteria: value
        });
    };

    componentDidMount() {
        const { getAllEmployeePayroll, getEmployeeList } = this.props;
        getAllEmployeePayroll();
        getEmployeeList();
    }

    render() {
        const { employeePayroll } = this.props.payroll;
        const employeeList = (this.props.payroll && this.props.payroll.employeeList) ? this.props.payroll.employeeList : []
        const { modalVisible, editItem } = this.state;
        const formatedList = employeePayroll ? employeePayroll.map((res) => {
            const data = res;
            Object.assign(data, { action: 'Edit' });
            return data;
        }) : [];
        const tableData = formatedList ? formatedList.sort((a, b) => {

            if (a.id !== b.id) {
                return a
            }
        }) : [];
        // console.log(employeeList);
        return (
            <>
                <div>
                    <Card title="Salary">
                        <Form >
                            <FormItem
                                label="Add New"
                                {...formItemLayout}
                            >
                                <Button type="primary" className="gx-text-right" onClick={() => this.showModal(true)}>Add</Button>
                            </FormItem>
                            
                        </Form>
                        {tableData ? <DataTable tableData={tableData} handleEdit={this.handleEdit} /> : ''}
                        <AddPayroll modalVisible={modalVisible} editItem={editItem && editItem} employeeList={employeeList} modalHandler={(state) => this.showModal(state)} />
                    </Card>
                </div>

            </>
        );
    }
}


const mapStateToProps = state => ({
    payroll: state.salary
});

const mapDispatchToProps = dispatch => {
    return {
        getAllEmployeePayroll: () => {
            dispatch(getAllEmployeePayroll());
        },
        getEmployeeList: () => {
            dispatch(getEmployeeList());
        },
        getPositionMaster: (req) => {
            dispatch(getPositionMaster(req));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Payroll);
