import React from "react";
import { Col, Row } from "antd";

import Payroll from "./payroll";

const Salary = () => {
  return (
    <Row>
      <Col span={24}>
        <Payroll />
      </Col>
    </Row>
  );
};

export default Salary;