import React, { Component } from "react";
import { Button, Form, Select, InputNumber, Modal } from "antd";
import Dropdown from "./Dropdown";
import payrollSvc from "../../../svc/payrollSvc";
import EditableFormTable from "./EditableTable";

const FormItem = Form.Item;
const Option = Select.Option;

class PayrollUpdateMaster extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    selectedRows: []
  };
  handleSubmit = e => {
    const { handleSubmit, editItem } = this.props;
    const { resetFields } = this.props.form;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        handleSubmit({
          masterId: editItem.id,
          maxLimit: values.maxLimit,
          minLimit: values.minLimit
        });
        resetFields();
      }
    });
  };

  handleChange = rows => {
    this.setState({
      ...this.state,
      selectedRows: rows
    });
  };

  setFieldsValue = obj => {
    this.props.form.setFieldsValue({ [obj.name]: obj.value });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible, loading, handleCancel, handleOk, editItem } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    return (
      <>
        <Modal
          visible={visible}
          title="Update Payroll Master"
          onOk={() => handleOk()}
          onCancel={() => handleCancel()}
          footer={[
            <Button key="back" onClick={() => handleCancel()}>
              Close
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={e => this.handleSubmit(e)}
            >
              Save
            </Button>
          ]}
        >
          <Form onSubmit={this.handleSubmit}>
            <FormItem {...formItemLayout} label="Max Limit">
              {getFieldDecorator("maxLimit", {
                initialValue: editItem !== null ? editItem.maxLimit : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter Max Limit!"
                  }
                ]
              })(
                <InputNumber style={{ width: "100%" }} id="maxLimit" min="0" />
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Min Limit">
              {getFieldDecorator("minLimit", {
                initialValue: editItem !== null ? editItem.minLimit : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter Min Limit!"
                  }
                ]
              })(
                <InputNumber style={{ width: "100%" }} id="minLimit" min="0" />
              )}
            </FormItem>
          </Form>
        </Modal>
      </>
    );
  }
}

const PayrollMasterUpdateForm = Form.create()(PayrollUpdateMaster);
export default PayrollMasterUpdateForm;
