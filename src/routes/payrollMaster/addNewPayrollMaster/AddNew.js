import React from "react";
import { Button, Card, Modal, Select, Input, Col, Row } from "antd";
import PayrollMasterForm from "./PayrollMasterForm";
import Dropdown from "./Dropdown";

const Option = Select.Option;

class AddNew extends React.Component {
  state = {
    loading: false,
    visible: false,
    position: {}
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  };
  handleCancel = () => {
    this.setState({ visible: false });
  };

  // handleChange(value) {
  //   console.log(`selected ${value}`);
  // }

  // handleBlur() {
  //   console.log("blur");
  // }

  // handleFocus() {
  //   console.log("focus");
  // }

  setFieldsValue = (value, e) => {
    this.setState({
      ...this.state,
      position: { positionValue: e.props.value, text: e.props.children }
    });
    const { handleChangePositionMaster } = this.props;
    handleChangePositionMaster(value);
  };

  handleSubmit = list => {
    const { addPayrollMasterList } = this.props;
    addPayrollMasterList(list);
    this.setState({ visible: false });
  };

  render() {
    const { visible, loading } = this.state;
    const { positionMaster, payrollItemNotMapWithPosition } = this.props;

    return (
      <>
        {Object.keys(this.state.position).length > 0 && (
          <Button type="primary" onClick={this.showModal}>
            Add
          </Button>
        )}
        <div>
          <div className="ant-card-body">
            <Row>
              <Col span={6}>Position</Col>
              <Col span={18}>
                <Select
                  key={Math.random}
                  style={{ width: "100%" }}
                  onChange={(value, e) => this.setFieldsValue(value, e)}
                >
                  {positionMaster &&
                    positionMaster.map(item => {
                      return (
                        <Option key={Math.random} value={item.id}>
                          {item.positionName}
                        </Option>
                      );
                    })}
                </Select>
              </Col>
            </Row>
          </div>
        </div>
        {visible === true && (
          <PayrollMasterForm
            loading={loading}
            visible={visible}
            handleOk={this.handleOk}
            handleCancel={this.handleCancel}
            handleSubmit={this.handleSubmit}
            position={this.state.position}
            payrollItemNotMapWithPosition={payrollItemNotMapWithPosition}
          />
        )}
      </>
    );
  }
}

export default AddNew;
