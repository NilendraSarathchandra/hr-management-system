import React from "react";
import { Col, Row } from "antd";

import PayrollMasterItemTable from "./PayrollMasterItemTable";
import Selection from "./Selection";
import Size from "./Size";
import Title from "./Title";

const ShowPayrollItems = () => {
  return (
    <Row>
      <Col span={24}>
        <PayrollMasterItemTable />
      </Col>
    </Row>
  );
};

export default ShowPayrollItems;
