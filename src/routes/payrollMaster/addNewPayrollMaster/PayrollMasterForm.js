import React, { Component } from "react";
import {
  AutoComplete,
  Button,
  Card,
  Cascader,
  Checkbox,
  Col,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Tooltip,
  Modal
} from "antd";
import Dropdown from "./Dropdown";
import payrollSvc from "../../../svc/payrollSvc";
import EditableFormTable from "./EditableTable";

const FormItem = Form.Item;
const Option = Select.Option;

class PayrollMaster extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    selectedRows: []
  };
  handleSubmit = e => {
    e.preventDefault();
    const { handleSubmit, position } = this.props;
    let { selectedRows } = this.state;
    let structuredItems = [];
    selectedRows.map(item => {
      structuredItems = [
        ...structuredItems,
        {
          payrollItemId: item.id,
          maxLimit: item.maxLimit,
          minLimit: item.minLimit,
          payType: 1,
          status: 1
        }
      ];
    });

    handleSubmit({
      payrollMasterList: structuredItems,
      positionMasterId: position.positionValue
    });
  };

  handleChange = rows => {
    this.setState({
      ...this.state,
      selectedRows: rows
    });
  };

  setFieldsValue = obj => {
    this.props.form.setFieldsValue({ [obj.name]: obj.value });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      visible,
      loading,
      handleCancel,
      handleOk,
      position,
      payrollItemNotMapWithPosition
    } = this.props;

    return (
      <>
        <Modal
          width="80%"
          visible={visible}
          title="Create Payroll Master"
          onOk={() => handleOk()}
          onCancel={() => handleCancel()}
          footer={[
            <Button key="back" onClick={() => handleCancel()}>
              Close
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={e => this.handleSubmit(e)}
              disabled={this.state.selectedRows.length > 0 ? false : true}
            >
              Save
            </Button>
          ]}
        >
          <div className="ant-divider ant-divider-horizontal ant-divider-with-text-center">
            <span className="ant-divider-inner-text">
              {" "}
              Position: {position.text}
            </span>
          </div>
          {visible === true && (
            <EditableFormTable
              payrollItemNotMapWithPosition={payrollItemNotMapWithPosition}
              handleChange={this.handleChange}
            />
          )}
        </Modal>
      </>
    );
  }
}

const PayrollMasterForm = Form.create()(PayrollMaster);
export default PayrollMasterForm;
