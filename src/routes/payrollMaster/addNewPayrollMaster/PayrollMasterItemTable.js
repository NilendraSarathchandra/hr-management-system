import React, { Component } from "react";
import { Card, Divider, Icon, Table, Button, message } from "antd";
import AddNew from "./AddNew";
import PayrollMasterUpdateForm from "./PayrollMasterUpdateForm";
import { connect } from "react-redux";
import {
  getPayrollMasterItemList,
  getPayrollMasterByPositionMaster,
  addPayrollMasterList,
  updatePayrollMasteItem
} from "../../../appRedux/actions/PayrollMaster";

class PayrollMasterItemTable extends Component {
  componentDidMount() {
    const { getPayrollMasterItemList } = this.props;

    getPayrollMasterItemList();
  }

  handleEditPayrollMasterItem = record => {
    this.setState({
      ...this.state,
      editItem: record,
      editVisible: true
    });
  };

  handleCancel = () => {
    this.setState({
      ...this.state,
      editVisible: false
    });
  };

  editHandleSubmit = values => {
    const {updatePayrollMasteItem} = this.props;
    updatePayrollMasteItem(values);
    this.setState({
      ...this.state,
      editVisible: false
    });
  };

  handleChangePositionMaster = id => {
    const { getPayrollMasterByPositionMaster } = this.props;
    getPayrollMasterByPositionMaster(id);
  };

  state = {
    editVisible: false,
    editItem: {}
  };

  render() {
    const { payrollMaster, addPayrollMasterList } = this.props;

    const NotificationMessage = () => {
      if (payrollMaster && payrollMaster.notification) {
        switch (payrollMaster.notification.type) {
          case "success":
            return message.success(payrollMaster.notification.message);
          case "error":
            return message.error(payrollMaster.notification.message);
          default:
            return null;
        }
      } else {
        return null;
      }
    };

    const columns = [
      {
        title: "payroll Item",
        dataIndex: "payrollItem.itemName",
        key: "itemName",
        width: 150,
        sorter: (a, b) =>
          a.payrollItem.itemName.length - b.payrollItem.itemName.length
      },
      {
        title: "Max Limit",
        dataIndex: "maxLimit",
        key: "maxLimit",
        width: 150,
        sorter: (a, b) => a.maxLimit - b.maxLimit
      },
      {
        title: "Min Limit",
        dataIndex: "minLimit",
        key: "minLimit",
        width: 150,
        sorter: (a, b) => a.minLimit - b.minLimit
      },
      {
        title: "Allow Statutory",
        dataIndex: "payrollItem.allowStatutary",
        key: "allowStatutory",
        width: 150,
        sorter: (a, b) =>
          a.payrollItem.allowStatutary.length -
          b.payrollItem.allowStatutary.length
      },
      {
        title: "Allow Tax",
        dataIndex: "payrollItem.allowTax",
        key: "allowTax",
        width: 150,
        sorter: (a, b) =>
          a.payrollItem.allowTax.length - b.payrollItem.allowTax.length
      },
      {
        title: "Status",
        dataIndex: "payrollItem.status",
        key: "status",
        width: 150,
        sorter: (a, b) =>
          a.payrollItem.status.length - b.payrollItem.status.length
      },
      {
        title: "Action",
        key: "action",
        render: (text, record) => (
          <span
            className="gx-link"
            onClick={() => this.handleEditPayrollMasterItem(text)}
          >
            Edit
          </span>
        )
      }
    ];
    let sizeScreen = window.innerHeight;
    return (
      <Card title="Payroll Master">
        <NotificationMessage />
        <AddNew
          positionMaster={payrollMaster && payrollMaster.positionMaster}
          handleChangePositionMaster={this.handleChangePositionMaster}
          payrollItemNotMapWithPosition={
            payrollMaster && payrollMaster.payrollItemNotMapWithPosition
          }
          addPayrollMasterList={addPayrollMasterList}
        />
        <Table
          className="gx-table-responsive"
          columns={columns}
          dataSource={payrollMaster && payrollMaster.payrollMasterItemList}
          // scroll={{ y: sizeScreen - 600 }}
          pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15'] }}
        />
        {this.state.editVisible === true && (
          <PayrollMasterUpdateForm
            visible={this.state.editVisible}
            handleCancel={this.handleCancel}
            editItem={this.state.editItem}
            handleSubmit={this.editHandleSubmit}
          />
        )}
      </Card>
    );
  }
}


const mapStateToProps = state => ({
  payrollMaster: state.payrollMaster
});

const mapDispatchToProps = dispatch => {
  return {
    getPayrollMasterItemList: () => {
      dispatch(getPayrollMasterItemList());
    },
    getPayrollMasterByPositionMaster: id => {
      dispatch(getPayrollMasterByPositionMaster(id));
    },
    addPayrollMasterList: list => {
      dispatch(addPayrollMasterList(list));
    },
    updatePayrollMasteItem: item => {
      dispatch(updatePayrollMasteItem(item));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PayrollMasterItemTable);
