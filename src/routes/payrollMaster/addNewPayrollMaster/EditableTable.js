import React from "react";
import { Table, Input, InputNumber, Popconfirm, Form } from "antd";

const EditableContext = React.createContext();

class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === "number") {
      return <InputNumber style={{ width: "100%" }} min="0" />;
    }
    return <Input />;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: [
                {
                  required: true,
                  message: `Please Input ${title}!`
                }
              ],
              initialValue: record[dataIndex]
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return (
      <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
    );
  }
}

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    const data =
      props.payrollItemNotMapWithPosition === undefined
        ? []
        : props.payrollItemNotMapWithPosition;

    this.state = { data, editingKey: "" };
    this.columns = [
      {
        title: "Payroll Item",
        dataIndex: "itemName",
        editable: false
      },
      {
        title: "Max Limit",
        dataIndex: "maxLimit",
        editable: true
      },
      {
        title: "Min Limit",
        dataIndex: "minLimit",
        editable: true
      },
      {
        title: "Allow Statutory",
        dataIndex: "allowStatutary",
        editable: false
      },
      {
        title: "Allow Tax",
        dataIndex: "allowTax",
        editable: false
      },
      {
        title: "operation",
        dataIndex: "operation",
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <a
                    onClick={() => this.save(form, record.id)}
                    style={{ marginRight: 8 }}
                  >
                    Save
                  </a>
                )}
              </EditableContext.Consumer>
              <Popconfirm
                title="Sure to cancel?"
                onConfirm={() => this.cancel(record.id)}
              >
                <a>Cancel</a>
              </Popconfirm>
            </span>
          ) : (
            <a
              disabled={editingKey !== ""}
              onClick={() => this.edit(record.id)}
            >
              Edit
            </a>
          );
        }
      }
    ];
  }

  isEditing = record => record.id === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  save(form, key) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.data];
      const index = newData.findIndex(item => key === item.id);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row
        });
        this.setState({ data: newData, editingKey: "" });
      } else {
        newData.push(row);
        this.setState({ data: newData, editingKey: "" });
      }
    });
  }

  edit(key) {
    this.setState({ ...this.state, editingKey: key });
  }

  render() {
    const { handleChange, payrollItemNotMapWithPosition } = this.props;
    const components = {
      body: {
        cell: EditableCell
      }
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType:
            col.dataIndex === "maxLimit" || col.dataIndex === "minLimit"
              ? "number"
              : "text",
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });
    const rowSelection = {
      onChange: (selectedRowKeys, selectedRows) => {
        handleChange(selectedRows);
      },
      getCheckboxProps: record => ({
        disabled: record.maxLimit === undefined,
        name: record.dataIndex
      })
    };
    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          className="gx-table-responsive"
          components={components}
          bordered
          dataSource={this.state.data}
          columns={columns}
          rowClassName="editable-row"
          rowSelection={rowSelection}
          pagination={{
            onChange: this.cancel
          }}
        />
      </EditableContext.Provider>
    );
  }
}

const EditableFormTable = Form.create()(EditableTable);
export default EditableFormTable;
