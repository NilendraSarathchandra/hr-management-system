import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";

import asyncComponent from "util/asyncComponent";


const PayrollMaster = ({match}) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/add`}/>
    <Route path={`${match.url}/add`} component={asyncComponent(() => import('./addNewPayrollMaster'))}/>
  </Switch>
);

export default PayrollMaster;