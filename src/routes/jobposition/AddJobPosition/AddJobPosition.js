import React from "react";
import { Button, Card, Modal, Select, Input } from "antd";
import JobPositionItemForm from "./JobPositionItemForm";

const Option = Select.Option;

class AddJobPosition extends React.Component {
  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  };

  handleChange(value) {
    console.log(`selected ${value}`);
  }

  handleBlur() {
    console.log("blur");
  }

  handleFocus() {
    console.log("focus");
  }

  render() {
    const {
      visible,
      loading,
      addEmployeePosition,
      showModal,
      handleCancel,
      editEmployeePositionItemDetails,
      positionLevels,
      JDTemplateList
    } = this.props;

    return (
      <>
        <Button type="primary" onClick={showModal}>
          Add
        </Button>

        <JobPositionItemForm
          loading={loading}
          visible={visible}
          handleOk={this.handleOk}
          handleCancel={handleCancel}
          positionLevels={positionLevels}
          editEmployeePositionItemDetails={editEmployeePositionItemDetails}
          handleSubmit={addEmployeePosition}
          JDTemplateList={JDTemplateList}
        />
      </>
    );
  }
}

export default AddJobPosition;
