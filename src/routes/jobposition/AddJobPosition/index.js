import React from "react";
import {Col, Row} from "antd";

import JobPositionItemTable from "./JobPositionItemTable";
import Selection from "./Selection";
import Size from "./Size";
import Title from "./Title";


const ShowJobPositions = () => {
  return (
    <Row>
      <Col span={24}>
        <JobPositionItemTable/>
      </Col>
    </Row>
  );
};

export default ShowJobPositions;
