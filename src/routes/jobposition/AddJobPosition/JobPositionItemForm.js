import React, { Component } from "react";
import {
  AutoComplete,
  Button,
  Card,
  Cascader,
  Checkbox,
  Col,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Tooltip,
  Modal
} from "antd";
import Dropdown from "./Dropdown";

const FormItem = Form.Item;
const Option = Select.Option;

class JobPositionItem extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    reportToList: []
  };

  getInitialLevel = id => {
    const { positionLevels } = this.props;
    let filteredItem =
      positionLevels && positionLevels.find(item => item.id === id);
    if (filteredItem && Object.keys(filteredItem).length > 0) {
      debugger
      let filteredLevels = positionLevels.filter(
        item => item.level > filteredItem.level
      );
      // if there are no items return same level
      if (filteredLevels.length <= 0) {
        filteredLevels = positionLevels.filter(
          item => item.level === filteredItem.level
        );
      }
      return filteredLevels;
    }
  };

  handleSubmit = e => {
    const { handleSubmit } = this.props;
    const { resetFields } = this.props.form;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        handleSubmit(values);
        resetFields();
      }
    });
  };

  setFieldsValue = obj => {
    this.props.form.setFieldsValue({ [obj.name]: obj.value });
    const { positionLevels } = this.props;
    if (obj.name === "hrPositionLevelId") {
      let filteredItem = positionLevels.find(item => item.id === obj.value);

      if (filteredItem && Object.keys(filteredItem).length > 0) {
        let filteredLevels = positionLevels.filter(
          item => item.level > filteredItem.level
        );
        // if there are no items return same level
        if (filteredLevels.length <= 0) {
          filteredLevels = positionLevels.filter(
            item => item.level === filteredItem.level
          );
        }

        this.setState({
          ...this.state,
          reportToList: filteredLevels
        });
      }
    }
  };

  handleFormCancel = () => {
    const { handleCancel } = this.props;
    const { resetFields } = this.props.form;
    resetFields();
    handleCancel();
    this.setState({
      ...this.state,
      reportToList: []
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      visible,
      loading,
      handleCancel,
      handleOk,
      positionLevels,
      editEmployeePositionItemDetails,
      JDTemplateList
    } = this.props;
    // const { reportToList } = this.state;
    const hrPositionLevelId = this.props.form.getFieldValue(
      "hrPositionLevelId"
    );
    let reportToList = [];

    if (editEmployeePositionItemDetails !== null) {
      if (hrPositionLevelId === "") {
        reportToList = this.getInitialLevel(
          editEmployeePositionItemDetails.hrPositionLevel.id
        );
      } else {
        reportToList = this.getInitialLevel(hrPositionLevelId);
      }
    } else {
      if (this.state.reportToList.length !== 0) {
        reportToList = this.getInitialLevel(hrPositionLevelId);
      }
    }

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    let modalTitle =
      editEmployeePositionItemDetails !== null
        ? "Edit Employee Position"
        : "New Employee position";
    return (
      <>
        <Modal
          visible={visible}
          title={modalTitle}
          onOk={() => handleOk()}
          onCancel={() => this.handleFormCancel()}
          footer={[
            <Button key="back" onClick={() => this.handleFormCancel()}>
              Close
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={e => this.handleSubmit(e)}
            >
              Save
            </Button>
          ]}
        >
          <Form onSubmit={this.handleSubmit}>
            <FormItem {...formItemLayout} label="Position Name">
              {getFieldDecorator("positionName", {
                initialValue:
                  editEmployeePositionItemDetails !== null
                    ? editEmployeePositionItemDetails.positionName
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter a postion name!"
                  }
                ]
              })(<Input id="positionName" />)}
            </FormItem>
            <FormItem {...formItemLayout} label="Position Level">
              {getFieldDecorator("hrPositionLevelId", {
                initialValue:
                  editEmployeePositionItemDetails !== null
                    ? editEmployeePositionItemDetails.hrPositionLevel.id
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter a Position Level!"
                  }
                ]
              })(
                <Select
                  key={Math.random}
                  style={{ width: "100%" }}
                  onChange={value =>
                    this.setFieldsValue({
                      value: value,
                      name: "hrPositionLevelId"
                    })
                  }
                >
                  {positionLevels &&
                    positionLevels.map(item => {
                      return (
                        <Option key={Math.random} value={item.id}>
                          {item.description}
                        </Option>
                      );
                    })}
                </Select>
              )}
            </FormItem>

            <FormItem {...formItemLayout} label="HR JD Template">
              {getFieldDecorator("hrJdTemplateId", {
                initialValue:
                  editEmployeePositionItemDetails !== null
                    ? editEmployeePositionItemDetails.hrJdTemplate.id
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter a JD Template!"
                  }
                ]
              })(
                <Select
                  key={Math.random}
                  style={{ width: "100%" }}
                  onChange={value =>
                    this.setFieldsValue({
                      value: value,
                      name: "hrJdTemplateId"
                    })
                  }
                >
                  {JDTemplateList &&
                    JDTemplateList.map(item => {
                      if (item.status === 1) {
                        return (
                          <Option key={Math.random} value={item.id}>
                            {item.jdName}
                          </Option>
                        );
                      }
                    })}
                </Select>
              )}
            </FormItem>

            <FormItem {...formItemLayout} label="Report To">
              {getFieldDecorator("reportTo", {
                initialValue:
                  editEmployeePositionItemDetails !== null
                    ? editEmployeePositionItemDetails.reportTo.id
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter report to person!"
                  }
                ]
              })(
                <Select
                  key={Math.random}
                  style={{ width: "100%" }}
                  onChange={value =>
                    this.setFieldsValue({
                      value: value,
                      name: "reportTo"
                    })
                  }
                >
                  {/* if it is a new item load data */}
                  {reportToList &&
                    reportToList.map(item => {
                      return (
                        <Option key={Math.random} value={item.id}>
                          {item.description}
                        </Option>
                      );
                    })}
                </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="description">
              {getFieldDecorator("description", {
                initialValue:
                  editEmployeePositionItemDetails !== null
                    ? editEmployeePositionItemDetails.description
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter a description!"
                  }
                ]
              })(<Input id="description" />)}
            </FormItem>
            <FormItem {...formItemLayout} label="Privilege Rule">
              {getFieldDecorator("privilegeRule", {
                initialValue:
                  editEmployeePositionItemDetails !== null
                    ? editEmployeePositionItemDetails.privilegeRule
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter Privilege Rule!"
                  }
                ]
              })(<Input id="privilegeRule" />)}
            </FormItem>
          </Form>
        </Modal>
      </>
    );
  }
}

const JobPositionItemForm = Form.create()(JobPositionItem);
export default JobPositionItemForm;
