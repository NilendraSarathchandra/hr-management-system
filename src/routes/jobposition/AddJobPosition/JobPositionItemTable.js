import React, { Component } from "react";
import { Card, Divider, Icon, Table, Button, message } from "antd";
import AddJobPosition from "./AddJobPosition";
import { connect } from "react-redux";
import {
  getEmployeePositionItemList,
  addJobPosition,
  updateJobPosition
} from "../../../appRedux/actions/Payroll";

// just to show the data structure: sample data
const data = [
  {
    key: "1",
    positionName: "CEO",
    employeeLevel: 1,
    reportTo: "MD",
    description: "Description 01",
    status: "Active"
  },
  {
    key: "2",
    positionName: "Manager",
    employeeLevel: 2,
    reportTo: "CEO",
    description: "Description 02",
    status: "Active"
  },
  {
    key: "3",
    positionName: "Executive",
    employeeLevel: 3,
    reportTo: "Manager",
    description: "Description 03",
    status: "Active"
  }
];

class JobPositionItemTable extends Component {
  state = {
    loading: false,
    visible: false,
    editEmpPositionDetails: null
  };

  showModalEmpJobPosition = () => {
    this.setState({
      visible: true,
      editEmpPositionDetails: null
    });
  };

  handleCancel = () => {
    this.setState({ visible: false, editPayrollItemDetails: null });
  };

  // submit job position item data to Backend
  addEmployeePosition = item => {
    const { addJobPosition, updateJobPosition } = this.props;
    const { editEmpPositionDetails } = this.state;
    const request = {
      hrPositionLevelId: item.hrPositionLevelId,
      positionName: item.positionName,
      hrJdTemplateId: item.hrJdTemplateId,
      reportTo: item.reportTo,
      description: item.description,
      status: "1",
      privilegeRule: item.privilegeRule
    };

    if (editEmpPositionDetails === null) {
      addJobPosition(request);
    }

    if (editEmpPositionDetails !== null) {
      request.jobPositionId = editEmpPositionDetails.id;
      updateJobPosition(request);
    }

    this.setState({ visible: false, editPayrollItemDetails: null });
  };

  componentDidMount() {
    const { getEmployeePositionItemList } = this.props;

    getEmployeePositionItemList();
  }

  columns = [
    {
      title: "Position Name",
      dataIndex: "positionName",
      key: "positionName",
      width: 150,
      sorter: (a, b) => a.positionName.length - b.positionName.length
    },
    {
      title: "Employee Level",
      dataIndex: "hrPositionLevel.description",
      key: "level",
      width: 150,
      sorter: (a, b) =>
        a.hrPositionLevel.description.length -
        b.hrPositionLevel.description.length
    },
    {
      title: "JD Template",
      dataIndex: "hrJdTemplate.jdName",
      key: "jdTemplateName",
      width: 150,
      sorter: (a, b) =>
        a.hrJdTemplate.jdName.length - b.hrJdTemplate.jdName.length
    },
    {
      title: "Report to",
      dataIndex: "reportTo.description",
      key: "reportTo",
      width: 150,
      sorter: (a, b) =>
        a.reportTo.description.length - b.reportTo.description.length
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
      width: 150,
      sorter: (a, b) => a.description.length - b.description.length
    },
    {
      title: "Privilege Rule",
      dataIndex: "privilegeRule",
      key: "privilegeRule",
      width: 150,
      sorter: (a, b) => a.privilegeRule.length - b.privilegeRule.length
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      width: 150,
      sorter: (a, b) => a.status.length - b.status.length
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <span>
          <span
            className="gx-link"
            onClick={() => this.handleEditEmpPositionItem(text)}
          >
            Edit
          </span>
        </span>
      )
    }
  ];

  handleEditEmpPositionItem = record => {
    this.setState({
      visible: true,
      editEmpPositionDetails: record
    });
  };

  render() {
    const { payroll } = this.props;
    const { loading, visible, editEmpPositionDetails } = this.state;

    const NotificationMessage = () => {
      const { payroll } = this.props;
      if (payroll && payroll.notification) {
        switch (payroll.notification.type) {
          case "success":
            return message.success(payroll.notification.message);
          case "error":
            return message.error(payroll.notification.message);

          default:
            return null;
        }
      } else {
        return null;
      }
    };
    let sizeScreen = window.innerHeight;
    return (
      <Card title="Employee positions">
        <NotificationMessage />
        <AddJobPosition
          positionLevels={payroll && payroll.positionLevelList}
          addEmployeePosition={this.addEmployeePosition}
          loading={loading}
          visible={visible}
          showModal={this.showModalEmpJobPosition}
          handleCancel={this.handleCancel}
          editEmployeePositionItemDetails={editEmpPositionDetails}
          JDTemplateList={payroll && payroll.JDTemplateList}
        />
        <Table
          className="gx-table-responsive"
          columns={this.columns}
          dataSource={payroll && payroll.positionList}
          // scroll={{ y: sizeScreen - 500 }}
          pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15'] }}
        />
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  payroll: state.payroll
});

const mapDispatchToProps = dispatch => {
  return {
    getEmployeePositionItemList: () => {
      dispatch(getEmployeePositionItemList());
    },
    addJobPosition: data => {
      dispatch(addJobPosition(data));
    },
    updateJobPosition: data => {
      dispatch(updateJobPosition(data));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobPositionItemTable);
