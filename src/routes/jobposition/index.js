import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";

import asyncComponent from "util/asyncComponent";


const JobPosition = ({match}) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/addjobposition`}/>
    <Route path={`${match.url}/addjobposition`} component={asyncComponent(() => import('./AddJobPosition'))}/>
  </Switch>
);

export default JobPosition;