import React from "react";
import {Route, Switch} from "react-router-dom";

import Components from "./components/index";
import CustomViews from "./customViews/index";
import Extensions from "./extensions/index";
import ExtraComponents from "./extraComponents/index";
import InBuiltApps from "./inBuiltApps/index";
import SocialApps from "./socialApps/index";
import Main from "./main/index";
import Documents from "./documents/index";
import Payroll from "./payroll/index";
import ClientList from './clientList/index';
import BusinessList from './businessList/index';
import  Boarding from './boarding/index';
import  SalaryData from './salary/index';
import PayrollMaster from './payrollMaster/index';
import JobPosition from './jobposition/index';
import Employees from './employee/index';
import SignIn from './auth/index';

const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}main`} component={Main}/>
      <Route path={`${match.url}auth`} component={SignIn}/>
      <Route path={`${match.url}components`} component={Components}/>
      <Route path={`${match.url}custom-views`} component={CustomViews}/>
      <Route path={`${match.url}extensions`} component={Extensions}/>
      <Route path={`${match.url}extra-components`} component={ExtraComponents}/>
      <Route path={`${match.url}in-built-apps`} component={InBuiltApps}/>
      <Route path={`${match.url}social-apps`} component={SocialApps}/>
      <Route path={`${match.url}documents`} component={Documents}/>
      <Route path={`${match.url}documents`} component={Documents}/>
      <Route path={`${match.url}payroll`} component={Payroll}/>
      <Route path={`${match.url}boarding`} component={Boarding}/>
      <Route path={`${match.url}salary`} component={SalaryData}/>
      <Route path={`${match.url}client`} component={ClientList}/>
      <Route path={`${match.url}payrollMaster`} component={PayrollMaster}/>
      <Route path={`${match.url}jobposition`} component={JobPosition}/>
      <Route path={`${match.url}employee`} component={Employees}/>
      {JSON.parse(localStorage.getItem("auth_data")).userRole == "SUPER_ADMIN" && <Route path={`${match.url}business`} component={BusinessList}/>}
    </Switch>
  </div>
);

export default App;
