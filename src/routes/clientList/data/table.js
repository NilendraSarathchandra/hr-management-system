import React, { Component } from "react";
import { Card, Divider, Icon, Table, Button, Input } from "antd";
import Highlighter from "react-highlight-words";

class DataTable extends Component {
  state = {
    searchText: "",
    searchedColumn: "",
    filteredInfo: null
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record !== undefined
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      )
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  handleChange = (pagination, filters, sorter) => {
    this.setState({
      filteredInfo: filters
    });
  };

  clearFilters = () => {
    this.setState({ filteredInfo: null });
  };

  render() {
    let { filteredInfo } = this.state;
    const { handleEditCustomer } = this.props;
    filteredInfo = filteredInfo || {};
    const columns = [
      {
        title: "Number",
        dataIndex: "customerNumber",
        key: "customerNumber",
        width: 150,
        sorter: (a, b) => Math.round(a.customerNumber) - Math.round(b.customerNumber),
        ...this.getColumnSearchProps("customerNumber")
      },
      {
        title: "Name",
        dataIndex: "customerName",
        key: "customerName",
        width: 200,
        sorter: (a, b) => a.customerName.length - b.customerName.length,
        ...this.getColumnSearchProps("customerName")
      },
      {
        title: "Division Name",
        dataIndex: "divisionName",
        key: "divisionName",
        width: 175,
        sorter: (a, b) => a.divisionName.length - b.divisionName.length,
        ...this.getColumnSearchProps("divisionName")
      },
      {
        title: "Branch Name",
        dataIndex: "branchName",
        key: "branchName",
        width: 175,
        sorter: (a, b) => a.branchName.length - b.branchName.length,
        ...this.getColumnSearchProps("branchName")
      },
      {
        title: "NIC No",
        dataIndex: "nic",
        key: "nic",
        width: 150,
        sorter: (a, b) => a.nic.length - b.nic.length,
        ...this.getColumnSearchProps("nic")
      },
      {
        title: "Telephone",
        dataIndex: "phoneNumber",
        key: "phoneNumber",
        width: 150,
        sorter: (a, b) => a.phoneNumber.length - b.phoneNumber.length
      },
      {
        title: "Email",
        dataIndex: "email",
        key: "email",
        width: 150,
        sorter: (a, b) => a.email.length - b.email.length
      },
      {
        title: "Sex",
        dataIndex: "gender",
        key: "gender",
        width: 50,
        sorter: (a, b) => a.gender.length - b.gender.length
      },
      {
        title: "Status",
        dataIndex: "status",
        key: "status",
        filters: [
          { text: "Active", value: "Active" },
          { text: "Pending", value: "Pending" },
          { text: "Doman", value: "Doman" },
          { text: "Black Listed", value: "Black Listed" },
          { text: "Close", value: "Close" }
        ],
        filteredValue: filteredInfo.status || null,
        onFilter: (value, record) => record.status.includes(value),
        width: 150,
        sorter: (a, b) => a.status.length - b.status.length,
        ellipsis: true
      },
      {
        title: "Action",
        key: "action",
        render: (text, record) => (
          <span>
            <span className="gx-link" onClick={() => handleEditCustomer(text)}>
              Edit
            </span>
          </span>
        )
      }
    ];

    const { customersAll } = this.props;
    let sizeScreen = window.innerHeight;
    return (
      <Table
       pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15'] }}
        className="gx-table-responsive"
        columns={columns}
        dataSource={customersAll}
        scroll={{y: sizeScreen-500}}
        expandedRowRender={record => (
          <>
            {" "}
            <p>
              Address: <p style={{ color: "#038fdd" }}>{record.address}</p>
            </p>{" "}
          </>
        )}
        onChange={this.handleChange}
      />
    );
  }
}

export default DataTable;
