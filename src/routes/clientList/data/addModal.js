import React, { Component } from "react";
import {
  Button,
  Card,
  Form,
  Input,
  Select,
  Modal,
  DatePicker,
  message
} from "antd";
import { NAV_STYLE_DEFAULT_HORIZONTAL } from "../../../constants/ThemeSetting";
import moment from "moment";

const FormItem = Form.Item;
const Option = Select.Option;
class AddModal extends Component {
  handleFormLayoutChange = e => {
    this.setState({ formLayout: e.target.value });
  };

  state = {
    branchName: null
  };

  constructor(props) {
    super(props);
    this.state = {
      formLayout: "horizontal"
    };
  }

  handleCancel = () => {
    const { resetFields } = this.props.form;
    resetFields();
    this.setState({ branchName: null });
    this.props.modalHandler(false, "AD");
  };

  handleSubmit = e => {
    e.preventDefault();
    const { resetFields } = this.props.form;
    const {
      handleSubmitCustomerAdd,
      customersAll,
      editItem,
      handleSubmitCustomerUpdate
    } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let isCustomerNumberExists = customersAll.find(
          item => item.customerNumber === values.customerNumber
        );
        let date = values.dob.format("YYYY-MM-DD");
        values.dob = date;

        if (editItem) {
          values.customerId = editItem.id;
          handleSubmitCustomerUpdate(values);
          this.props.modalHandler(true, "SF");
          resetFields();
        } else {
          if (isCustomerNumberExists === undefined) {
            handleSubmitCustomerAdd(values);
            this.props.modalHandler(true, "SF");
            resetFields();
          } else {
            message.error("Customer number already exists!");
          }
        }
      }
    });
  };

  disabledDate = value => {
    const form = this.props.form;
    // Can not select days before today and today
    return value > Date.now();
  };

  handleChangeBranchName = value => {
    const { resetFields } = this.props.form;
    resetFields("divisionId");
    this.setState({
      ...this.state,
      branchName: value
    });
    const { getDivisionListMetaData } = this.props;
    getDivisionListMetaData(value);
  };

  render() {
    const { formLayout } = this.state;
    const { branches, divisionList, editItem } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    const { modalVisible } = this.props;
    const { getFieldDecorator } = this.props.form;
    const dateConfig = {
      initialValue: editItem ? moment(editItem.dob, dateFormat) : "",
      rules: [{ required: true, message: "Please select a date" }]
    };
    const dateFormat = "YYYY/MM/DD";

    return (
      <>
        <Modal
          visible={modalVisible}
          title={
            editItem
              ? "Update Client Basic Details"
              : "Add Client Basic Details"
          }
          onCancel={this.handleCancel}
          footer={[
            <Button
              key="submit"
              type="primary"
              onClick={e => this.handleSubmit(e)}
            >
              Next
            </Button>
          ]}
        >
          <Form layout={formLayout} onSubmit={this.handleSubmit}>
            <FormItem label="Branch" {...formItemLayout}>
              {getFieldDecorator("branchId", {
                initialValue: editItem ? editItem.branchMaster.id : "",
                rules: [{ required: true, message: "Please select a branch" }]
              })(
                <Select
                  // value={this.state.branchName}
                  onChange={value => this.handleChangeBranchName(value)}
                  placeholder="Please select a branch"
                >
                  {branches &&
                    branches.map(item => {
                      return <Option value={item.id}>{item.branchName}</Option>;
                    })}
                </Select>
              )}
            </FormItem>
            <FormItem label="Division" {...formItemLayout}>
              {getFieldDecorator("divisionId", {
                initialValue: editItem
                  ? this.state.branchName
                    ? ""
                    : editItem.divisionMaster.id
                  : "",
                rules: [{ required: true, message: "Please select an option!" }]
              })(
                <Select allowClear={true} placeholder="Please select an option">
                  {divisionList &&
                    divisionList.map(item => {
                      return (
                        <Option value={item.id}>{item.divisionName}</Option>
                      );
                    })}
                </Select>
              )}
            </FormItem>

            <FormItem label="Customer Number" {...formItemLayout}>
              {getFieldDecorator("customerNumber", {
                initialValue: editItem ? editItem.customerNumber : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input disabled={editItem ? true : false} />)}
            </FormItem>

            <FormItem label="Name with initials" {...formItemLayout}>
              {getFieldDecorator("customerName", {
                initialValue: editItem ? editItem.customerName : "",
                rules: [
                  { required: true, message: "Correct name should be added!" }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem label="Initials for" {...formItemLayout}>
              {getFieldDecorator("initialFor", {
                initialValue: editItem ? editItem.initialFor : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem label="NIC No" {...formItemLayout}>
              {getFieldDecorator("nic", {
                initialValue: editItem ? editItem.nic : "",
                rules: [
                  { required: true, message: "Field should not be empty!" }
                ]
              })(<Input />)}
            </FormItem>
            <FormItem label="Title" {...formItemLayout}>
              {getFieldDecorator("title", {
                initialValue: editItem ? editItem.title : "",
                rules: [{ required: true, message: "Field should not empty!" }]
              })(
                <Select placeholder="Please select an option">
                  <Option value="Mr">Mr.</Option>
                  <Option value="Mrs">Mrs.</Option>
                  <Option value="Miss">Miss.</Option>
                  <Option value="Ms">Ms.</Option>
                  <Option value="Dr">Dr.</Option>
                  <Option value="Professor">Professor.</Option>
                </Select>
              )}
            </FormItem>
            <FormItem label="Gender" {...formItemLayout}>
              {getFieldDecorator("gender", {
                initialValue: editItem ? editItem.gender : "",
                rules: [{ required: true, message: "Please select an option!" }]
              })(
                <Select placeholder="Please select an option">
                  <Option value="Male">Male</Option>
                  <Option value="Female">Female</Option>
                  <Option value="Other">Other</Option>
                </Select>
              )}
            </FormItem>
            <FormItem label="Date of Birth" {...formItemLayout}>
              {getFieldDecorator(
                "dob",
                dateConfig
              )(
                <DatePicker
                  format={dateFormat}
                  disabledDate={this.disabledDate}
                  className="gx-mb-3 gx-w-100"
                />
              )}
            </FormItem>
            <FormItem label="Telephone" {...formItemLayout}>
              {getFieldDecorator("phoneNumber", {
                initialValue: editItem ? editItem.phoneNumber : "",
                rules: [{ required: true, message: "Field should not empty!" }]
              })(<Input />)}
            </FormItem>
            <FormItem label="Address" {...formItemLayout}>
              {getFieldDecorator("address", {
                initialValue: editItem ? editItem.address : "",
                rules: [{ required: true, message: "Field should not empty!" }]
              })(<Input />)}
            </FormItem>
            <FormItem label="Email" {...formItemLayout}>
              {getFieldDecorator("email", {
                initialValue: editItem ? editItem.email : "",
                rules: [
                  {
                    type: "email",
                    message: "The input is not valid E-mail!"
                  },
                  { required: true, message: "Field should not empty!" }
                ]
              })(<Input type="email" />)}
            </FormItem>
          </Form>
        </Modal>
      </>
    );
  }
}

export default Form.create()(AddModal);
