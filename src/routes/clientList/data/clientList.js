import {
  Dropdown,
  Input,
  Select,
  Radio,
  Form,
  Card,
  Button,
  message
} from "antd";
import React from "react";
import DropdownList from "./Dropdown";
import DataTable from "./table";
import AddModal from "./addModal";
import SigatureForm from "./signatureForm";
import PropertyForm from "./property";
import AttachmentForm from "./attachment";
import { connect } from "react-redux";
import {
  getBranchesMetaData,
  getDivisionListMetaData,
  addCustomer,
  getAllCustomers,
  updateCustomer
} from "../../../appRedux/actions/Customer";

const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 3 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 18 },
    md: { span: 16 },
    lg: { span: 12 }
  }
};

class ClientList extends React.Component {
  state = {
    loading: false,
    modalVisible: false,
    sigatureModal: false,
    propertyModal: false,
    attachmentModal: false,
    editItem: null
  };

  componentDidMount() {
    const { getBranchesMetaData } = this.props;

    getBranchesMetaData();
  }

  selectOption = e => {
    this.setState({ formLayout: e.target.value });
  };

  constructor(props) {
    super(props);
  }

  showModal = (status, page) => {
    if (page === "AD") {
      this.setState({
        modalVisible: status,
        editItem: null
      });
    } else if (page === "SF") {
      this.setState({
        modalVisible: false,
        editItem: null
      });
      this.setState({
        sigatureModal: status,
        editItem: null
      });
    } else if (page === "PR") {
      this.setState({
        sigatureModal: false,
        editItem: null
      });
      this.setState({
        propertyModal: status,
        editItem: null
      });
    } else if (page === "AT") {
      this.setState({
        propertyModal: false,
        editItem: null
      });
      this.setState({
        attachmentModal: status,
        editItem: null
      });
    }
  };

  setFieldsValue = object => {};

  getDivisionListMetaData = branchId => {
    const { getDivisionListMetaData } = this.props;
    getDivisionListMetaData(branchId);
  };

  handleSubmitCustomerAdd = body => {
    const { addCustomer } = this.props;
    addCustomer(body);
  };

  handleEditCustomer = item => {
    const { getDivisionListMetaData } = this.props;

    this.setState({ ...this.state, editItem: item, modalVisible: true });
    getDivisionListMetaData(item.branchMaster.id);
  };

  handleSubmitCustomerUpdate = body => {
    const { updateCustomer } = this.props;
    updateCustomer(body);
  };

  render() {
    const {
      modalVisible,
      sigatureModal,
      propertyModal,
      attachmentModal
    } = this.state;
    const statusTypes = [
      { key: "pending", name: "Pending" },
      { key: "active", name: "Active" },
      { key: "doman", name: "Doman" },
      { key: "blackListed", name: "Black Listed" },
      { key: "close", name: "Close" }
    ];
    const { customer } = this.props;
    const NotificationMessage = () => {
      const { customer } = this.props;
      if (customer && customer.notification) {
        switch (customer.notification.type) {
          case "success":
            return message.success(customer.notification.message);
          case "error":
            return message.error(customer.notification.message);

          default:
            return null;
        }
      } else {
        return null;
      }
    };

    return (
      <>
        <NotificationMessage />
        <div>
          <Card title="Client List">
            <Button
              type="primary"
              className="gx-text-right"
              onClick={() => this.showModal(true, "AD")}
            >
              Add
            </Button>
            {/* <Form>
              <FormItem label="Add New" {...formItemLayout}>
                <Button
                  type="primary"
                  className="gx-text-right"
                  onClick={() => this.showModal(true, "AD")}
                >
                  Add
                </Button>
              </FormItem>
              {/* <FormItem label="Status" {...formItemLayout}>
                <DropdownList
                  options={statusTypes}
                  setFieldsValue={this.setFieldsValue}
                  initialValuez={"pending"}
                  fieldName={"pending"}
                  className="gx-mb-3"
                />
              </FormItem> */}
            {/* <FormItem label="More Filters" {...formItemLayout}>
                <Radio.Group
                  defaultValue="horizontal"
                  onChange={this.selectOption}
                >
                  <Radio.Button value="horizontal">Branch</Radio.Button>
                  <Radio.Button value="vertical">Division</Radio.Button>
                </Radio.Group>
              </FormItem>
              <FormItem
                {...formItemLayout}
                label="Search"
                // validateStatus="error"
                // help="Should be combination of numbers & alphabets"
              >
                <Input placeholder="Search item" id="error" />
              </FormItem> */}
            {/* </Form>  */}
            <DataTable
              customersAll={customer && customer.customersAll}
              handleEditCustomer={this.handleEditCustomer}
            />
            <AddModal
              modalVisible={modalVisible}
              modalHandler={(state, page) => this.showModal(state, page)}
              branches={customer && customer.branches}
              getDivisionListMetaData={this.getDivisionListMetaData}
              divisionList={customer && customer.divisionList}
              handleSubmitCustomerAdd={this.handleSubmitCustomerAdd}
              handleSubmitCustomerUpdate={this.handleSubmitCustomerUpdate}
              customersAll={customer && customer.customersAll}
              editItem={this.state.editItem && this.state.editItem}
            />
            <SigatureForm
              sigatureModal={sigatureModal}
              modalHandler={(state, page) => this.showModal(state, page)}
              customerInfo={customer && customer.customerInfo}
            />
            {customer && customer.customerInfo && (
              <PropertyForm
                propertyModal={propertyModal}
                modalHandler={(state, page) => this.showModal(state, page)}
                customerInfo={customer && customer.customerInfo}
              />
            )}
            {customer &&
              customer.customerInfo &&
              this.state.attachmentModal &&
              this.state.attachmentModal === true && (
                <AttachmentForm
                  attachmentModal={attachmentModal}
                  modalHandler={(state, page) => this.showModal(state, page)}
                  customerInfo={customer && customer.customerInfo}
                />
              )}
          </Card>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  customer: state.customer
});

const mapDispatchToProps = dispatch => {
  return {
    getBranchesMetaData: () => {
      dispatch(getBranchesMetaData());
    },
    getDivisionListMetaData: branchId => {
      dispatch(getDivisionListMetaData(branchId));
    },
    addCustomer: customerDetails => {
      dispatch(addCustomer(customerDetails));
    },
    updateCustomer: customerDetails => {
      dispatch(updateCustomer(customerDetails));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientList);
