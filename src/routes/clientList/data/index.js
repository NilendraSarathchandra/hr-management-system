import React from "react";
import { Col, Row } from "antd";

import ClientList from "./clientList";

const Client = () => {
  return (
    <Row>
      <Col span={24}>
        <ClientList />
      </Col>
    </Row>
  );
};

export default Client;