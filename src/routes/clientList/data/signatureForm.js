import { Button, Form, Modal, Icon, message, Upload } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { addCustomerProfileImages } from "../../../appRedux/actions/Customer";

class Sigature extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: []
  };

  handleSubmit = e => {
    e.preventDefault();
    let { image, signature } = this.state;
    const { addCustomerProfileImages, customerInfo } = this.props;

    if (image.file === undefined || signature.file === undefined) {
      message.error("Client Image and Signature both required");
    }
    if (image.file !== undefined && signature.file !== undefined) {
      // add images as form data request
      const form = new FormData();
      form.set("photo", image.file);
      form.set("signature", signature.file);
      form.set("customerNumber", customerInfo.customerNumber);

      addCustomerProfileImages(form);
      this.props.modalHandler(true, "PR");
      this.setState({
        image: { loading: false },
        signature: { loading: false }
      });
    }
  };

  getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  beforeUpload(file) {
    const isJPG = file.type === "image/jpeg";
    if (!isJPG) {
      message.error("You can only upload JPG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isJPG && isLt2M;
  }

  state = {
    image: { loading: false },
    signature: { loading: false }
  };

  handleChange = (info, name) => {
    if (info.file.status === "uploading") {
      this.setState({ ...this.state, [name]: { loading: true } });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          ...this.state,
          [name]: {
            imageUrl,
            loading: false,
            file: info.file.originFileObj
          }
        })
      );
    }
  };

  handleCancel = () => {
    this.setState({
      image: { loading: false },
      signature: { loading: false }
    });
    this.props.modalHandler(false, "SF");
  };

  onCustomRequest = file => {
    return new Promise((resolve, reject) => {
      const ajaxResponseWasFine = true;

      setTimeout(() => {
        if (ajaxResponseWasFine) {
          const reader = new FileReader();

          reader.addEventListener(
            "load",
            () => {
              resolve(reader.result);
            },
            false
          );

          if (file) {
            reader.readAsDataURL(file);
          }
        } else {
          reject("error");
        }
      }, 1000);
    });
  };

  render() {
    const {
      visible,
      loading,
      handleCancel,
      handleOk,
      sigatureModal
    } = this.props;
    const uploadButtonImage = (
      <div>
        <Icon type={this.state.image.loading ? "loading" : "plus"} />
        <div className="ant-upload-text">Image</div>
      </div>
    );

    const uploadButtonSignature = (
      <div>
        <Icon type={this.state.signature.loading ? "loading" : "plus"} />
        <div className="ant-upload-text">Signature</div>
      </div>
    );
    const imageUrl = this.state.image.imageUrl;
    const signatureUrl = this.state.signature.imageUrl;

    return (
      <>
        <Modal
          visible={sigatureModal}
          title="Upload Client Image and Signature"
          onOk={() => handleOk()}
          onCancel={this.handleCancel}
          footer={[
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={e => this.handleSubmit(e)}
            >
              Next
            </Button>
          ]}
        >
          <Upload
            name="image"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            beforeUpload={this.beforeUpload}
            onChange={info => this.handleChange(info, "image")}
            action={this.onCustomRequest}
          >
            {imageUrl ? <img src={imageUrl} alt="" /> : uploadButtonImage}
          </Upload>
          <Upload
            name="signature"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            beforeUpload={this.beforeUpload}
            onChange={info => this.handleChange(info, "signature")}
            action={this.onCustomRequest}
          >
            {signatureUrl ? (
              <img src={signatureUrl} alt="" />
            ) : (
              uploadButtonSignature
            )}
          </Upload>
        </Modal>
      </>
    );
  }
}

const SignatureForm = Form.create()(Sigature);

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => {
  return {
    addCustomerProfileImages: profileImagesRequest => {
      dispatch(addCustomerProfileImages(profileImagesRequest));
    }
  };
};

export default connect(null, mapDispatchToProps)(SignatureForm);
