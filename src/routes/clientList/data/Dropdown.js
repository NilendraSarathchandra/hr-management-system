import React from "react";
import {
    Select
} from "antd";

export default function DropdownList(props) {
    const { options, setFieldsValue, fieldName } = props;
    const Option = Select.Option;

    return (
        <Select key={Math.random} style={{ width: "100%" }} onChange={(value) => setFieldsValue({ value: value, name: fieldName })} >
            {options.map((item) => { return <Option key={Math.random} value={item.key}>{item.name}</Option> })}
        </Select>
    );
};
