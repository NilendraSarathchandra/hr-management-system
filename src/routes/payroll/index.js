import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";

import asyncComponent from "util/asyncComponent";


const Payroll = ({match}) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/addpayroll`}/>
    <Route path={`${match.url}/addpayroll`} component={asyncComponent(() => import('./AddPayroll'))}/>
  </Switch>
);

export default Payroll;