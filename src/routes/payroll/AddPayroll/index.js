import React from "react";
import {Col, Row} from "antd";
import PayrollItemTable from "./PayrollItemTable";

const ShowPayrollItems = () => {
  return (
    <Row>
      <Col span={24}>
        <PayrollItemTable/>
      </Col>
    </Row>
  );
};

export default ShowPayrollItems;
