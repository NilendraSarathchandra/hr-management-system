import { Card, Divider, Icon, Table, Button, message } from "antd";
import AddPayrollItem from "./AddPayrolItem";
import React, { Component } from "react";
import PropTypes from "prop-types";
import payrollSvc from "../../../../src/svc/payrollSvc";
import { connect } from "react-redux";
import SelectingMultipleAreas from "../../extensions/map/ammap/multipleAreas/Components/SelectingMultipleAreas";
import {
  getPayrollItemList,
  addPayrollItem,
  updatePayrollItem
} from "../../../appRedux/actions/Payroll";

//jst to show the structure : sample data
const Testdata = [
  {
    key: "1",
    category: "Basic Salary",
    itemName: "Basic salary",
    payType: 1,
    allowStatutory: "Yes",
    allowTax: "Yes",
    status: "Active"
  },
  {
    key: "2",
    category: "Incentive",
    itemName: "Travelling incentive",
    payType: 3,
    allowStatutory: "No",
    allowTax: "No",
    status: "Active"
  },
  {
    key: "3",
    category: "Statutary Deduction",
    itemName: "E.T.F.",
    payType: 5,
    allowStatutory: "Yes",
    allowTax: "Yes",
    status: "Active"
  }
];

class PayrollItemTable extends Component {
  static propTypes = {};

  state = {
    loading: false,
    visible: false,
    editPayrollItemDetails: null
  };

  columns = [
    {
      title: "Category",
      dataIndex: "hrPayrollItemCategory.categoryName",
      key: "category",
      width: 200,
      sorter: (a, b) => a.hrPayrollItemCategory.categoryName.length - b.hrPayrollItemCategory.categoryName.length
    },
    {
      title: "Item Name",
      dataIndex: "itemName",
      key: "itemName",
      width: 200,
      sorter: (a, b) => a.itemName.length - b.itemName.length
    },
    {
      title: "Pay Type",
      dataIndex: "payType",
      key: "payType",
      width: 200,
      sorter: (a, b) => a.payType - b.payType
    },
    {
      title: "Allow Statutory",
      dataIndex: "allowStatutary",
      key: "allowStatutary",
      width: 200,
      sorter: (a, b) => a.allowStatutary.length - b.allowStatutary.length
    },
    {
      title: "Allow Tax",
      dataIndex: "allowTax",
      key: "allowTax",
      width: 200,
      sorter: (a, b) => a.allowTax.length - b.allowTax.length
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      width: 200,
      sorter: (a, b) => a.status.length - b.status.length
    },
    {
      title: "Action",
      key: "action",
      render: (text, record, e) => (
        <span>
          {/* <span className="gx-link">Action 一 {record.title}</span> */}
          {/* <Divider type="vertical" /> */}
          <span
            className="gx-link"
            onClick={() => this.handleEditPayrollItem(text)}
          >
            Edit
          </span>
          {/* <Divider type="vertical" /> */}
          {/* <span className="gx-link ant-dropdown-link">
            More actions <Icon type="down" />
          </span> */}
        </span>
      )
    }
  ];

  showModal = () => {
    this.setState({
      visible: true,
      editPayrollItemDetails: null
    });
  };

  handleCancel = () => {
    this.setState({ visible: false, editPayrollItemDetails: null });
  };

  handleEditPayrollItem = record => {
    this.setState({
      visible: true,
      editPayrollItemDetails: record
    });
  };

  // submit payroll item data to Backend
  addPayrollItem = items => {
    const { addPayrollItem, updatePayrollItem } = this.props;
    const { editPayrollItemDetails } = this.state;

    const request = {
      categoryId: items.category,
      payrollItemName: items.itemName,
      payType: items.payType,
      allowTax: items.allowTax,
      allowStatutary: items.allowStatutory,
      payrollItemStatus: "1"
    };

    if (editPayrollItemDetails === null) {
      addPayrollItem(request);
    }
    if (editPayrollItemDetails !== null) {
      // request.categoryId = editPayrollItemDetails.hrPayrollItemCategory.id;
      request.payrollItemId = editPayrollItemDetails.id;
      updatePayrollItem(request);
    }

    this.setState({ visible: false, editPayrollItemDetails: null });
  };

  componentDidMount() {
    const { getPayrollItemList } = this.props;
    // get payroll item as a list from backend simulatenously call meta data 1. payroll item category
    getPayrollItemList();
  }

  render() {
    const { loading, visible, editPayrollItemDetails } = this.state;
    const { payroll } = this.props;

    const NotificationMessage = () => {
      const { payroll } = this.props;
      if (payroll && payroll.notification) {
        switch (payroll.notification.type) {
          case "success":
            return message.success(payroll.notification.message);
          case "error":
            return message.error(payroll.notification.message);

          default:
            return null;
        }
      } else {
        return null;
      }
    };
    let sizeScreen = window.innerHeight;
    return (
      <Card title="Payroll Items">
        <NotificationMessage />
        <AddPayrollItem
          addPayrollItem={this.addPayrollItem}
          loading={loading}
          visible={visible}
          showModal={this.showModal}
          handleCancel={this.handleCancel}
          editPayrollItemDetails={editPayrollItemDetails}
          payrollItemCategory={payroll && payroll.payrollItemCategory}
        />
        <Table
          className="gx-table-responsive"
          columns={this.columns}
          dataSource={payroll && payroll.payrollItemList}
          // scroll={{ y: sizeScreen - 500 }}
          pagination={{ defaultPageSize: 5, showSizeChanger: true, pageSizeOptions: ['5', '10', '15'] }}
        />
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  payroll: state.payroll
});

const mapDispatchToProps = dispatch => {
  return {
    getPayrollItemList: () => {
      dispatch(getPayrollItemList());
    },
    addPayrollItem: body => {
      dispatch(addPayrollItem(body));
    },
    updatePayrollItem: body => {
      dispatch(updatePayrollItem(body));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PayrollItemTable);
