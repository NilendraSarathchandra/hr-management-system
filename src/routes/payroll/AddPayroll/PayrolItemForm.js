import React, { Component } from "react";
import {
  AutoComplete,
  Button,
  Card,
  Cascader,
  Checkbox,
  Col,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Tooltip,
  Modal
} from "antd";
import Dropdown from "./Dropdown";

const FormItem = Form.Item;
const Option = Select.Option;

class PayrolItem extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: []
  };
  handleSubmit = e => {
    const { handleSubmit } = this.props;
    const { resetFields } = this.props.form;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        handleSubmit(values);
        resetFields();
      }
    });
  };

  setFieldsValue = obj => {
    this.props.form.setFieldsValue({ [obj.name]: obj.value });
  };

  handleFormCancel = () => {
    const { handleCancel } = this.props;
    const { resetFields } = this.props.form;
    resetFields();
    handleCancel();
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      visible,
      loading,
      handleCancel,
      handleOk,
      editPayrollItemDetails,
      payrollItemCategory
    } = this.props;
    const categorySource = [
      { key: "1", name: "Basic Salary" },
      { key: "2", name: "Incentive" },
      { key: "3", name: "Tax Deduction" }
    ];
    const payTypeSource = [
      { key: "1", name: "1" },
      { key: "2", name: "2" },
      { key: "3", name: "3" },
      { key: "4", name: "4" }
    ];
    const typesAllowed = [
      { key: "1", name: "Yes" },
      { key: "0", name: "No" }
    ];
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    let itemTitle =
      editPayrollItemDetails !== null
        ? "Edit Payroll Item"
        : "Add Payroll Item";
    return (
      <>
        <Modal
          visible={visible}
          title={itemTitle}
          onOk={() => handleOk()}
          onCancel={() => this.handleFormCancel()}
          footer={[
            <Button key="back" onClick={() => this.handleFormCancel()}>
              Close
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={e => this.handleSubmit(e)}
            >
              Save
            </Button>
          ]}
        >
          <Form onSubmit={this.handleSubmit}>
            <FormItem {...formItemLayout} label="Item Name">
              {getFieldDecorator("itemName", {
                initialValue:
                  editPayrollItemDetails !== null
                    ? editPayrollItemDetails.itemName
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter name!"
                  }
                ]
              })(<Input id="itemName" />)}
            </FormItem>
            <FormItem {...formItemLayout} label="Category">
              {getFieldDecorator("category", {
                initialValue:
                  editPayrollItemDetails !== null
                    ? editPayrollItemDetails.hrPayrollItemCategory.id
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter a category!"
                  }
                ]
              })(
                // cannot use if used reset method wont work
                // <Dropdown
                //   options={categorySource}
                //   setFieldsValue={this.setFieldsValue}
                //   fieldName={"category"}
                // />

                <Select
                  key={Math.random}
                  style={{ width: "100%" }}
                  onChange={value =>
                    this.setFieldsValue({ value: value, name: "category" })
                  }
                >
                  {payrollItemCategory &&
                    payrollItemCategory.map(item => {
                      if (item.status === 1) {
                        return (
                          <Option key={Math.random} value={item.id}>
                            {item.categoryName}
                          </Option>
                        );
                      }
                    })}
                </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Pay Type">
              {getFieldDecorator("payType", {
                initialValue:
                  editPayrollItemDetails !== null
                    ? editPayrollItemDetails.payType
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter a pay type!"
                  }
                ]
              })(
                // <Dropdown
                //   options={payTypeSource}
                //   setFieldsValue={this.setFieldsValue}
                //   fieldName={"payType"}
                // />
                <Select
                  key={Math.random}
                  style={{ width: "100%" }}
                  onChange={value =>
                    this.setFieldsValue({ value: value, name: "payType" })
                  }
                >
                  {payTypeSource.map(item => {
                    return (
                      <Option key={Math.random} value={item.key}>
                        {item.name}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Allow Statutory">
              {getFieldDecorator("allowStatutory", {
                initialValue:
                  editPayrollItemDetails !== null
                    ? editPayrollItemDetails.allowStatutary === "Yes"
                      ? "1"
                      : "0"
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter a statutory!"
                  }
                ]
              })(
                // <Dropdown
                //   options={typesAllowed}
                //   setFieldsValue={this.setFieldsValue}
                //   fieldName={"allowStatutory"}
                // />
                <Select
                  key={Math.random}
                  style={{ width: "100%" }}
                  onChange={value =>
                    this.setFieldsValue({
                      value: value,
                      name: "allowStatutory"
                    })
                  }
                >
                  {typesAllowed.map(item => {
                    return (
                      <Option key={Math.random} value={item.key}>
                        {item.name}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Allow Tax">
              {getFieldDecorator("allowTax", {
                initialValue:
                  editPayrollItemDetails !== null
                    ? editPayrollItemDetails.allowTax === "Yes"
                      ? "1"
                      : "0"
                    : "",
                rules: [
                  {
                    required: true,
                    message: "Please enter a Tax!"
                  }
                ]
              })(
                // <Dropdown
                //   options={typesAllowed}
                //   setFieldsValue={this.setFieldsValue}
                //   fieldName={"allowTax"}
                // />

                <Select
                  key={Math.random}
                  style={{ width: "100%" }}
                  onChange={value =>
                    this.setFieldsValue({ value: value, name: "allowTax" })
                  }
                >
                  {typesAllowed.map(item => {
                    return (
                      <Option key={Math.random} value={item.key}>
                        {item.name}
                      </Option>
                    );
                  })}
                </Select>
              )}
            </FormItem>
          </Form>
        </Modal>
      </>
    );
  }
}

const PayrolItemForm = Form.create()(PayrolItem);
export default PayrolItemForm;
