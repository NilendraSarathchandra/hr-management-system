import customerService from "../../svc/customerSvc";

// action get branches meta data
export const getBranchesMetaData = () => {
  return dispatch => {
    Promise.all([
      customerService.getBranchesMetaData(),
      customerService.getAllCustomers()
    ]).then(response => {
      if (response[0].data === undefined || response[1].data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        structureCustomerData(response[1].data.data).then(
          structuredResponse => {
            return dispatch({
              type: "BRANCHES_METADATA_RECEIVED",
              branches: response[0].data.data,
              customersAll: structuredResponse
            });
          }
        );
      }
    });
  };
};

// action get division list meta data param branchId
export const getDivisionListMetaData = branchId => {
  return dispatch => {
    customerService.getDivisionListMetaData(branchId).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        return dispatch({
          type: "DIVISION_LIST_METADATA_RECEIVED",
          divisionList: response.data.data
        });
      }
    });
  };
};

// action to add customer params are in body object
export const addCustomer = body => {
  return dispatch => {
    customerService.addCustomer(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        if (response.data.success === true) {
          structureCustomerData([response.data.data]).then(
            structuredResponse => {
              dispatch({
                type: "NOTIFICATION",
                notification: {
                  type: "success",
                  message: "Basic client details added"
                }
              });
              return dispatch({
                type: "CUSTOMER_ADD_SUCCESS",
                customerInfo: structuredResponse[0]
              });
            }
          );
        }
        if (response.data.success === false) {
          return dispatch({
            type: "NOTIFICATION",
            notification: {
              type: "error",
              message: "Error occured"
            }
          });
        }
      }
    });
  };
};

// action get property list meta data param customer type id
export const getPropertyListMetaData = customerTypeId => {
  return dispatch => {
    customerService.getPropertyListMetaData(customerTypeId).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        if (response.data.success === true) {
          return dispatch({
            type: "PROPERTY_LIST_METADATA_RECEIVED",
            propertyList: response.data.data
          });
        }
      }
    });
  };
};

// action to add customer image and signature, post request 3 params involved
export const addCustomerProfileImages = body => {
  return dispatch => {
    customerService.uploadCustomerProfileImages(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        if (response.data.success === true) {
          dispatch({
            type: "NOTIFICATION",
            notification: {
              type: "success",
              message: "Image and signature uploded successfully"
            }
          });
          return dispatch({
            type: "CUSTOMER_PROFILE_IMAGES_UPLOADED_SUCCESS"
          });
        }
        if (response.data.success === false) {
          return dispatch({
            type: "NOTIFICATION",
            notification: {
              type: "error",
              message: "Error occured"
            }
          });
        }
      }
    });
  };
};

// action to update customer property list params are in body object
export const updateCustomerPropertyList = body => {
  return dispatch => {
    customerService.updateCustomerPropertyList(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        if (response.data.success === true) {
          dispatch({
            type: "NOTIFICATION",
            notification: {
              type: "success",
              message: "Customer details updated successfully"
            }
          });
          return dispatch({
            type: "CUSTOMER_PROPERTY_LIST_ADD_SUCCESS",
            UpdatedCustomerPropertyList: response.data.data
          });
        }
        if (response.data.success === false) {
          return dispatch({
            type: "NOTIFICATION",
            notification: {
              type: "error",
              message: "Error occured"
            }
          });
        }
      }
    });
  };
};

// action get attachment list meta data param customer type id
export const getAttachmentListMetaData = customerTypeId => {
  return dispatch => {
    customerService.getAttachmentListMetaData(customerTypeId).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        return dispatch({
          type: "ATTACHMENT_LIST_METADATA_RECEIVED",
          attachmentList: response.data.data
        });
      }
    });
  };
};

// action to add customer attachments, post request 3 params involved
export const addCustomerAttachments = (
  attachmentList,
  filesList,
  customerId
) => {
  return dispatch => {
    uploadAttachments(attachmentList, filesList, customerId).then(response => {
      if (response === "error") {
        // TODO: handle error
        console.log("error", response);
      } else {
        dispatch({
          type: "NOTIFICATION",
          notification: {
            type: "success",
            message: "Client added successfully"
          }
        });
        return dispatch({
          type: "CUSTOMER_ATTACHMENT_UPLOADED_SUCCESS"
        });
      }
    });
  };
};

const uploadAttachments = (attachementList, fileList, customerId) => {
  return new Promise((resolve, reject) => {
    try {
      if (attachementList && attachementList.length > 0) {
        attachementList.map(item => {
          const form = new FormData();
          form.set("customerId", customerId);
          form.set("attachmentTypeId", item.id);
          form.set("file", fileList[item.id].file);

          customerService.uploadCustomerAttachment(form).then(response => {
            if (response.data === undefined) {
              // TODO: handle error
              console.log("error", response);
              reject("error");
            } else {
              console.log("success");
            }
          });
        });
      }
      resolve("success");
    } catch (error) {
      reject(error);
    }
  });
};

// action get all customers
export const getAllCustomers = () => {
  return dispatch => {
    customerService.getAllCustomers().then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        if (response.data.success === true) {
          structureCustomerData(response.data.data).then(structuredResponse => {
            return dispatch({
              type: "ALL_CUSTOMER_DATA_RECEIVED",
              customersAll: structuredResponse
            });
          });
        }
      }
    });
  };
};

// action to update customer params are in body object
export const updateCustomer = body => {
  return dispatch => {
    customerService.updateCustomer(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        if (response.data.success === true) {
          structureCustomerData([response.data.data]).then(structuredData => {
            dispatch({
              type: "NOTIFICATION",
              notification: {
                type: "success",
                message: "Basic client details updated"
              }
            });
            return dispatch({
              type: "CUSTOMER_UPDATE_SUCCESS",
              updatedItem: structuredData[0]
            });
          });
        }
        if (response.data.success === false) {
          return dispatch({
            type: "NOTIFICATION",
            notification: {
              type: "error",
              message: "Error occured"
            }
          });
        }
      }
    });
  };
};

const structureCustomerData = customerList => {
  return new Promise((resolve, reject) => {
    try {
      if (customerList && customerList.length > 0) {
        customerList.map(item => {
          let branchName = item.branchMaster.branchName;
          let divisionName = item.divisionMaster.divisionName;
          let status = getStatus(item.status);
          item.branchName = branchName;
          item.divisionName = divisionName;
          item.status = status;
        });
      }
      resolve(customerList);
    } catch (error) {
      reject(error);
    }
  });
};

const getStatus = statusTypeId => {
  switch (statusTypeId) {
    case 1:
      return "Active";
      break;

    default:
      break;
  }
};
