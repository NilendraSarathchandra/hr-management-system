import boardingSvc from "../../svc/boardingSvc";

export const getMetaData = () => {
    return dispatch => {
        Promise.all([
            boardingSvc.getPositionLevel(),
            boardingSvc.getBranches()
        ]).then(res => {
            if (res[0].data.data && res[1].data.data) {
                return dispatch({
                    type: "META_DATA",
                    positionList: res[0].data.data,
                    branchList: res[1].data.data
                });
            } else if (res[0].data.data && !res[0].data.data) {
                return dispatch({
                    type: "META_DATA",
                    positionList: res[0].data.data,
                    branchList: []
                });
            } else if (!res[0].data.data && res[0].data.data) {
                return dispatch({
                    type: "META_DATA",
                    positionList: [],
                    branchList: res[1].data.data
                });
            } else {
                return dispatch({
                    type: "META_DATA",
                    positionList: [],
                    branchList: []
                });
            }
        });
    };
};

export const getDevisionMetaData = (res) => {
    return dispatch => {
        Promise.all([
            boardingSvc.getDevision(res)
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "DEVISION_META_DATA",
                    devisionList: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "DEVISION_META_DATA",
                    devisionList: []
                });
            }
        });
    };
};

export const getEmployeeList = () => {
    return dispatch => {
        Promise.all([
            boardingSvc.getEmployees(),
            boardingSvc.getCustomer()
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "EMPLOYEES_LIST",
                    employeeList: res[0].data.data,
                    customerList: res[1].data.data
                });
            } else {
                return dispatch({
                    type: "EMPLOYEES_LIST",
                    employeeList: [],
                    customerList: []
                });
            }
        }, error => {
            return dispatch({
                type: "EMPLOYEES_LIST",
                error: error
            });
        });
    };
};

export const getEmployeeTypeList = () => {
    return dispatch => {
        Promise.all([
            boardingSvc.getEmployeeType()
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "EMPLOYEE_TYPE_LIST",
                    employeeTypeList: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "EMPLOYEE_TYPE_LIST",
                    employeeTypeList: []
                });
            }
        }, error => {
            return dispatch({
                type: "EMPLOYEE_TYPE_LIST",
                error: error
            });
        });
    };
};


export const updateEmployee = (req) => {
    return dispatch => {
        Promise.all([
            boardingSvc.updateEmployee(req)
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "UPDATE_EMPLOYEE",
                    updateEmp: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "UPDATE_EMPLOYEE",
                    updateEmp: {success: false}
                });
            }
        }, error => {
            return dispatch({
                type: "UPDATE_EMPLOYEE",
                updateEmp: error
            });
        });
    };
};

export const addEmployee = (req) => {
    return dispatch => {
        Promise.all([
            boardingSvc.addEmployee(req)
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "ADD_EMPLOYEE",
                    addEmp: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "ADD_EMPLOYEE",
                    addEmp: {success: false}
                });
            }
        }, error => {
            return dispatch({
                type: "ADD_EMPLOYEE",
                addEmp: error
            });
        });
    };
};

