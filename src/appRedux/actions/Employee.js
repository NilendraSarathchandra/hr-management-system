import employeeSvc from "../../svc/employeeSvc";

export const getAllEmployees = (month) => {
    return dispatch => {
        Promise.all([
            employeeSvc.getEmployees(month)
        ]).then(res => {
            if (res &&  res[0] && res[0].data &&  res[0].data.data) {
                return dispatch({
                    type: "GET_EMPLOYEES",
                    employees: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "GET_EMPLOYEES",
                    employees: []
                });
            }
        }, error => {
            return dispatch({
                type: "GET_EMPLOYEES",
                error: error
            });
        });
    };
}

export const addAllEmployees = (req) => {
    return dispatch => {
        Promise.all([
            employeeSvc.addEmployees(req)
        ]).then(res => {
            if (res &&  res[0] && res[0].data) {
                return dispatch({
                    type: "ADD_EMPLOYEES",
                    addData: res[0].data
                });
            } else {
                return dispatch({
                    type: "ADD_EMPLOYEES",
                    addData: false
                });
            }
        }, error => {
            return dispatch({
                type: "ADD_EMPLOYEES",
                error: false
            });
        });
    };
}

export const updateAllEmployees = (req) => {
    return dispatch => {
        Promise.all([
            employeeSvc.updateEmployees(req)
        ]).then(res => {
            if (res &&  res[0] && res[0].data) {
                console.log(res[0].data, 'iiii')
                return dispatch({
                    type: "UPDATE_EMPLOYEES",
                    updateData: res[0].data
                });
            } else {
                return dispatch({
                    type: "UPDATE_EMPLOYEES",
                    updateData: false
                });
            }
        }, error => {
            return dispatch({
                type: "UPDATE_EMPLOYEES",
                error: false
            });
        });
    };
}

export const reset = () => {
    return dispatch => {
        Promise.all().then(res => {
            return dispatch({
                type: "RESET_DATA",
                updateData: null,
                addData: null
            });
        }, error => {
            return dispatch({
                type: "RESET_DATA",
                error: false
            });
        });
    };
}

export const getAllmonths = () => {
    return dispatch => {
        Promise.all([
            employeeSvc.getAllmonths()
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "GET_MONTHS",
                    months: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "GET_MONTHS",
                    months: []
                });
            }
        }, error => {
            return dispatch({
                type: "GET_MONTHS",
                error: error
            });
        });
    };
}

export const getEmployeeList = () => {
    return dispatch => {
        Promise.all([
            employeeSvc.getAllEmployeePayrolls()
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "EMPLOYEES_LIST",
                    employeeList: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "EMPLOYEES_LIST",
                    employeeList: [],
                });
            }
        }, error => {
            return dispatch({
                type: "EMPLOYEES_LIST",
                error: error
            });
        });
    };
};

export const getPayrolItem = (req) => {
    return dispatch => {
        Promise.all([
            employeeSvc.getPayrolItem(req)
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "PAYROLL_ITEM",
                    payrollItem: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "PAYROLL_ITEM",
                    payrollItem: [],
                });
            }
        }, error => {
            return dispatch({
                type: "PAYROLL_ITEM",
                error: error
            });
        });
    };
};