import PayrollMasterService from "../../../src/svc/payrollMasterSvc";

// aciton to get all payroll items, get request no params involved
export const getPayrollMasterItemList = () => {
  return dispatch => {
    Promise.all([
      PayrollMasterService.getPayrollMasterItemList(),
      PayrollMasterService.getPayrollMasterPositionMaster()
    ]).then(allResponse => {
      if (
        allResponse[0].data === undefined ||
        allResponse[1].data === undefined
      ) {
        // TODO: handle error
        console.log("error", allResponse);
      } else {
        struturePayrollMasterListData(allResponse[0].data.data).then(
          structuredMasterList => {
            return dispatch({
              type: "PAYROLL_MASTER_ITEM_LIST",
              payrollMasterItemList: structuredMasterList,
              positionMaster: allResponse[1].data.data
            });
          }
        );
      }
    });
  };
};

// action to get all payroll items, post request params positionMasterId
export const getPayrollMasterByPositionMaster = id => {
  return dispatch => {
    Promise.all([
      PayrollMasterService.getPayrollMasterByPositionMaster({
        positionMasterId: id
      }),
      PayrollMasterService.getPayrollMasterItemNotMapWithPosition({
        positionMasterId: id
      })
    ]).then(allResponse => {
      if (allResponse[0].data === undefined) {
        // TODO: handle error
        console.log("error", allResponse);
      } else {
        struturePayrollMasterListData(allResponse[0].data.data).then(
          structuredMasterList => {
            if (allResponse[1].data.data !== undefined) {
              struturePayrollMasterNotMapData(allResponse[1].data.data).then(
                structuredNotMapList => {
                  return dispatch({
                    type: "PAYROLL_MASTER_ITEM_LIST_BY_POSITION_MASTER",
                    payrollMasterItemByPositionMaster: structuredMasterList,
                    payrollItemNotMapWithPosition: structuredNotMapList
                  });
                }
              );
            } else {
              return dispatch({
                type: "PAYROLL_MASTER_ITEM_LIST_BY_POSITION_MASTER",
                payrollMasterItemByPositionMaster: structuredMasterList
              });
            }
          }
        );
      }
    });
  };
};

export const addPayrollMasterList = body => {
  return dispatch => {
    Promise.all([
      PayrollMasterService.addPayrollMasterList(body),
      PayrollMasterService.getPayrollMasterItemNotMapWithPosition({
        positionMasterId: body.positionMasterId
      })
    ]).then(response => {
      if (response[0].data === undefined) {
        // TODO: handle error
        console.log("error", response);
        dispatch({
          type: "ERROR"
        });
        return dispatch({
          type: "CLEAR_ERROR"
        });
      } else {
        dispatch({
          type: "PAYROLL_MASTER_LIST_ADDED_NOTIFICATION"
        });
        struturePayrollMasterListData(response[0].data.data).then(
          structuredResponse => {
            if (response[1].data.data !== undefined) {
              struturePayrollMasterNotMapData(response[1].data.data).then(
                structuredNotMapList => {
                  return dispatch({
                    type: "PAYROLL_MASTER_LIST_ADDED",
                    newItem: structuredResponse,
                    payrollItemNotMapWithPosition: structuredNotMapList
                  });
                }
              );
            } else {
              return dispatch({
                type: "PAYROLL_MASTER_LIST_ADDED",
                newItem: structuredResponse
              });
            }
          }
        );
      }
    });
  };
};

export const updatePayrollMasteItem = body => {
  return dispatch => {
    PayrollMasterService.updatePayrollMasterListItem(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
        dispatch({
          type: "ERROR"
        });
        return dispatch({
          type: "CLEAR_ERROR"
        });
      } else {
        dispatch({
          type: "PAYROLL_MASTER_ITEM_UPDATED_NOTIFICATION"
        });
        struturePayrollMasterListData([response.data.data]).then(
          structuredResponse => {
            return dispatch({
              type: "PAYROLL_MASTER_ITEM_UPDATED",
              updatedItem: structuredResponse[0]
            });
          }
        );
      }
    });
  };
};

const struturePayrollMasterListData = data => {
  return new Promise((resolve, reject) => {
    let structuredResponse = [];
    try {
      if (data && data.length > 0) {
        data.map(item => {
          let allowStatutory =
            item.payrollItem.allowStatutary === 1 ? "Yes" : "No";
          let allowTax = item.payrollItem.allowTax === 1 ? "Yes" : "No";
          let status = item.payrollItem.status === 1 ? "Active" : "In-Active";
          return (structuredResponse = [
            ...structuredResponse,
            {
              ...item,
              payrollItem: {
                ...item.payrollItem,
                allowStatutary: allowStatutory,
                allowTax: allowTax,
                status: status
              }
            }
          ]);
        });
      }
      resolve(structuredResponse);
    } catch (error) {
      reject(error);
    }
  });
};

const struturePayrollMasterNotMapData = data => {
  return new Promise((resolve, reject) => {
    let structuredResponse = [];
    try {
      if (data && data.length > 0) {
        data.map(item => {
          let allowStatutory = item.allowStatutary === 1 ? "Yes" : "No";
          let allowTax = item.allowTax === 1 ? "Yes" : "No";
          let status = item.status === 1 ? "Active" : "In-Active";
          return (structuredResponse = [
            ...structuredResponse,
            {
              ...item,
              allowStatutary: allowStatutory,
              allowTax: allowTax,
              status: status
            }
          ]);
        });
      }
      resolve(structuredResponse);
    } catch (error) {
      reject(error);
    }
  });
};
