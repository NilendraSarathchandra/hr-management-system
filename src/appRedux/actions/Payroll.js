import payrollSvc from "../../../src/svc/payrollSvc";

// aciton to ger all payroll items, get request no params involved
export const getPayrollItemList = clickEvent => {
  return dispatch => {
    Promise.all([
      payrollSvc.getPayrollItemList(),
      payrollSvc.getPayrollItemCategory()
    ]).then(allResponse => {

      if (
        allResponse[0].data === undefined ||
        allResponse[1].data === undefined
      ) {
        // TODO: handle error
        console.log("error", allResponse);
      } else {
        struturePayrollListData(allResponse[0].data.data).then(
          structuredReponse => {
            return dispatch({
              type: "PAYROLL_ITEM_LIST",
              payrollItemList: structuredReponse,
              payrollItemCategory: allResponse[1].data.data
            });
          }
        );
      }
    });
  };
};

const struturePayrollListData = data => {
  return new Promise((resolve, reject) => {
    let structuredResponse = [];
    try {
      if (data && data.length > 0) {
        data.map(item => {
          let allowStatutory = item.allowStatutary === 1 ? "Yes" : "No";
          let allowTax = item.allowTax === 1 ? "Yes" : "No";
          let status = item.status === 1 ? "Active" : "In-Active";
          return (structuredResponse = [
            ...structuredResponse,
            {
              ...item,
              allowStatutary: allowStatutory,
              allowTax: allowTax,
              status: status
            }
          ]);
        });
      }
      resolve(structuredResponse);
    } catch (error) {
      reject(error);
    }
  });
};

export const addPayrollItem = body => {
  return dispatch => {
    payrollSvc.addPayrollItem(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
        dispatch({
          type: "ERROR"
        });
        return dispatch({
          type: "CLEAR_ERROR"
        });
      } else {
        dispatch({
          type: "PAYROLL_ITEM_ADDED_NOTIFICATION"
        });
        struturePayrollListData([response.data.data]).then(
          structuredResponse => {
            return dispatch({
              type: "PAYROLL_ITEM_ADDED",
              newItem: structuredResponse[0]
            });
          }
        );
      }
    });
  };
};

export const updatePayrollItem = body => {
  return dispatch => {
    payrollSvc.updatePayrollItem(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
        dispatch({
          type: "ERROR"
        });
        return dispatch({
          type: "CLEAR_ERROR"
        });
      } else {
        dispatch({
          type: "PAYROLL_ITEM_UPDATED_NOTIFICATION"
        });
        struturePayrollListData([response.data.data]).then(
          structuredResponse => {
            return dispatch({
              type: "PAYROLL_ITEM_UPDATED",
              updatedItem: structuredResponse[0]
            });
          }
        );
      }
    });
  };
};

// aciton to get all employee positions items, get request no params involved
// this will get all the meta data also at once
export const getEmployeePositionItemList = () => {
  return dispatch => {
    Promise.all([
      payrollSvc.getJobPositionLevel(),
      payrollSvc.getJobPositionItemList({}),
      payrollSvc.getJDTemplateValues()
    ]).then(allResponse => {
      if (
        allResponse[0].data === undefined ||
        allResponse[1].data === undefined || 
        allResponse[2].data === undefined
      ) {
        // TODO: handle error
        console.log("error", allResponse);
      } else {
        structureJobPositionListData(allResponse[1].data.data).then(
          structuredData => {
            return dispatch({
              type: "EMPLOYEE_POSITION_LIST",
              jobPositionItemList: structuredData,
              positionLevelList: allResponse[0].data.data,
              JDTemplateList: allResponse[2].data.data
            });
          }
        );
      }
    });
  };
};

export const addJobPosition = body => {
  return dispatch => {
    payrollSvc.addJobPosition(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
        dispatch({
          type: "ERROR"
        });
        return dispatch({
          type: "CLEAR_ERROR"
        });
      } else {
        dispatch({
          type: "PAYROLL_ITEM_ADDED_NOTIFICATION"
        });
        structureJobPositionListData([response.data.data]).then(
          structuredResponse => {
            return dispatch({
              type: "EMPLOYEE_POSITION_ITEM_ADDED",
              newItem: structuredResponse[0]
            });
          }
        );
      }
    });
  };
};

const structureJobPositionListData = data => {
  let structuredResponse = [];
  return new Promise((resolve, reject) => {
    try {
      if (data && data.length > 0) {
        data.map(item => {
          let status = item.status === 1 ? "Active" : "In-Active";
          return (structuredResponse = [
            ...structuredResponse,
            {
              ...item,
              status: status
            }
          ]);
        });
      }
      resolve(structuredResponse);
    } catch (error) {
      reject(error);
    }
  });
};

// put request, update existing job position
export const updateJobPosition = body => {
  return dispatch => {
    payrollSvc.updateJobPosition(body).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
        dispatch({
          type: "ERROR"
        });
        return dispatch({
          type: "CLEAR_ERROR"
        });
      } else {
        dispatch({
          type: "PAYROLL_ITEM_UPDATED_NOTIFICATION"
        });
        if (
          response.data.data === undefined &&
          response.data.success === true
        ) {
          return dispatch({
            type: "JOB_POSITION_ITEM_UPDATED",
            updatedItem: null
          });
        }

        if (response.data.success === false) {
          dispatch({
            type: "ERROR"
          });
          return dispatch({
            type: "CLEAR_ERROR"
          });
        }

        structureJobPositionListData([response.data.data]).then(
          structuredResponse => {
            return dispatch({
              type: "JOB_POSITION_ITEM_UPDATED",
              updatedItem: structuredResponse[0],
              request: body
            });
          }
        );
      }
    });
  };
};
