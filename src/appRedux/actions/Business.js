import businessSvc from "../../svc/businessSvc";

export const getAllBusinessList = () => {
  return dispatch => {
    businessSvc.getAllCustomers().then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        return dispatch({
          type: "BUSINESS_LIST_ALL_SUCCESS",
          businessList: response.data.data
        });
      }
    });
  };
};

export const AddNewBusiness = businessInfo => {
  return dispatch => {
    businessSvc.addNewBusiness(businessInfo).then(response => {
      if (response.data === undefined) {
        // TODO: handle error
        console.log("error", response);
      } else {
        return dispatch({
          type: "BUSINESS_ADD_SUCCESS",
          newRecord: response.data.data
        });
      }
    });
  };
};


export const UpdateBusinsess = businessInfo => {
    return dispatch => {
      businessSvc.updateBusiness(businessInfo).then(response => {
        if (response.data === undefined) {
          // TODO: handle error
          console.log("error", response);
        } else {
          return dispatch({
            type: "BUSINESS_UPDATE_SUCCESS",
            updateRecord: response.data.data
          });
        }
      });
    };
  };
  

  export const getAllUsers = (companyId) => {
    return dispatch => {
      businessSvc.getAllUsers({companyId: companyId}).then(response => {
        if (response.data === undefined) {
          // TODO: handle error
          console.log("error", response);
        } else {
          return dispatch({
            type: "BUSINESS_USERS_LIST_ALL_SUCCESS",
            businessUsers: response.data.data
          });
        }
      });
    };
  };
  
  export const updateUser = (info) => {
    return dispatch => {
      businessSvc.updateUsers(info).then(response => {
        if (response.data === undefined) {
          // TODO: handle error
          console.log("error", response);
        } else {
          return dispatch({
            type: "UPDATE_BUSINESS_USER_SUCCESS",
            updatedUser: info
          });
        }
      });
    };
  };

  export const addUser = (info) => {
    return dispatch => {
      businessSvc.addUsers(info).then(response => {
        console.log('response: ', response);
        if (response.data === undefined) {
          // TODO: handle error
          console.log("error", response);
        } else {
          businessSvc.getAllUsers({companyId: info.companyId}).then(response => {
            if (response.data === undefined) {
              // TODO: handle error
              console.log("error", response);
            } else {
              return dispatch({
                type: "ADD_BUSINESS_USER_SUCCESS",
                businessUsers: response.data.data
              });
            }
          });
        }
      });
    };
  };


  export const resetPassword = (info) => {
    return dispatch => {
      businessSvc.resetPassword(info).then(response => {

        if (response.data === undefined) {
          // TODO: handle error
          console.log("error", response);
        } else {
          if(response.data.error !== undefined) {
            dispatch({
              type: "ERROR_PASSOWRD_RESET",
              error: response.data.error.errorMessage
            });
            return dispatch({
              type: "RESET_NOTIFICATION",
            });
          }
          dispatch({
            type: "PASSOWRD_RESET_SUCCESS",
            success: response.data.success
          });
          return dispatch({
            type: "RESET_NOTIFICATION",
          });
          
        }
      });
    };
  };