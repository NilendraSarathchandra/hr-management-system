import salarySvc from "../../svc/salarySvc";

export const getAllEmployeePayroll = () => {
    return dispatch => {
        Promise.all([
            salarySvc.getAllEmployeePayrolls()
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "GET_EMPLOYEE_PAYROLL",
                    employeePayroll: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "GET_EMPLOYEE_PAYROLL",
                    employeePayroll: []
                });
            }
        });
    };
};

export const getEmployeeList = () => {
    return dispatch => {
        Promise.all([
            salarySvc.getEmployees()
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "EMPLOYEES_LIST",
                    employeeList: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "EMPLOYEES_LIST",
                    employeeList: [],
                });
            }
        }, error => {
            return dispatch({
                type: "EMPLOYEES_LIST",
                error: error
            });
        });
    };
};

export const getPositionMaster = (res) => {
    return dispatch => {
        Promise.all([
            salarySvc.getPayrolItem(res)
        ]).then(res => {
            if (res && res[0].data && res[0].data.data) {
                return dispatch({
                    type: "POSITION_MAST",
                    positionList: res[0].data.data
                });
            } else {
                return dispatch({
                    type: "POSITION_MAST",
                    positionList: [],
                });
            }
        }, error => {
            return dispatch({
                type: "POSITION_MAST",
                error: error
            });
        });
    };
};

export const addEmployeePayrolMaster = (res) => {
    return dispatch => {
        Promise.all([
            salarySvc.addEmployeePayrolMaster(res)
        ]).then(res => {
            if ( res[0] && res[0].data) {
                return dispatch({
                    type: "EMPLOYEES_ADD",
                    addSuccess: res[0].data
                });
            } else {
                return dispatch({
                    type: "EMPLOYEES_ADD",
                    addSuccess: false,
                });
            }
        }, error => {
            return dispatch({
                type: "EMPLOYEES_ADD",
                addSuccess: false
            });
        });
    };
};

export const reset = () => {
    return dispatch => {
        Promise.all().then(res => {
            return dispatch({
                type: "RESET_DATA",
                addSuccess: null,
                updateSuccess: null
            });
        }, error => {
            return dispatch({
                type: "RESET_DATA",
                addSuccess: false,
                updateSuccess: null
            });
        });
    };
};

export const updateEmployeePayrolMaster = (res) => {
    return dispatch => {
        Promise.all([
            salarySvc.updateEmployeePayrolMaster(res)
        ]).then(res => {
            if (res[0] && res[0].data) {
                return dispatch({
                    type: "EMPLOYEES_UPDATE",
                    updateSuccess: res[0].data
                });
            } else {
                return dispatch({
                    type: "EMPLOYEES_UPDATE",
                    updateSuccess: false,
                });
            }
        }, error => {
            return dispatch({
                type: "EMPLOYEES_UPDATE",
                updateSuccess: false
            });
        });
    };
};