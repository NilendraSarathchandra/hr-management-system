import update from "react-addons-update";

const initValues = {
  notification: null,
  notificationR: null
};

const Business = (state = initValues, action) => {
  switch (action.type) {
    case "BUSINESS_LIST_ALL_SUCCESS":
      return {
        ...state,
        businessList: action.businessList,
        notification: null
      };
    case "BUSINESS_USERS_LIST_ALL_SUCCESS":
      return {
        ...state,
        businessUsers: action.businessUsers,
        notification: null
      };
    case "ADD_BUSINESS_USER_SUCCESS":
      return {
        ...state,
        businessUsers: action.businessUsers,
        notification: null
      };
    case "UPDATE_BUSINESS_USER_SUCCESS":
      let userUpdatedItem = action.updatedUser;
      let itemListUser = state.businessUsers;
      let findIndexUser = itemListUser.findIndex(item => {
        return item.id === userUpdatedItem.userId;
      });
      let updatedItemsUser = update(itemListUser, {
        [findIndexUser]: { $set: userUpdatedItem }
      });
      return {
        ...state,
        businessUsers: updatedItemsUser,
        notification: null
      };
    case "BUSINESS_ADD_SUCCESS":
      return {
        ...state,
        businessList: [...state.businessList, action.newRecord],
        notification: null
      };
    case "BUSINESS_UPDATE_SUCCESS":
      let updatedItem = action.updateRecord;
      let itemList = state.businessList;
      let findIndex = itemList.findIndex(item => {
        return item.businessMasterId === updatedItem.businessMasterId;
      });
      let updatedItems = update(itemList, {
        [findIndex]: { $set: updatedItem }
      });
      return {
        ...state,
        businessList: updatedItems,
        notification: null
      };
    case "ERROR_PASSOWRD_RESET":
      return {
        ...state,
        notificationR: {
          type: "error",
          message: action.error
        }
      };
    case "RESET_NOTIFICATION":
      return {
        ...state,
        notificationR: null
      };
    case "PASSOWRD_RESET_SUCCESS":
      return {
        ...state,
        notificationR: {
          type: "success",
          message: "Password Reset Success"
        }
      };
    default:
      return state;
  }
};

export default Business;
