import update from "react-addons-update";

const initValues = {
  notification: null
};

const PayrollMaster = (state = initValues, action) => {
  switch (action.type) {
    case "PAYROLL_MASTER_ITEM_LIST":
      return {
        ...state,
        payrollMasterItemList: [],
        positionMaster: action.positionMaster,
        notification: null
      };
    case "PAYROLL_MASTER_ITEM_LIST_BY_POSITION_MASTER":
      return {
        ...state,
        payrollMasterItemList: action.payrollMasterItemByPositionMaster,
        payrollItemNotMapWithPosition: action.payrollItemNotMapWithPosition,
        notification: null
      };
    case "PAYROLL_MASTER_LIST_ADDED":
      let newItems = action.newItem;
      return {
        ...state,
        payrollMasterItemList:
          state.payrollMasterItemList === undefined
            ? newItems
            : [...state.payrollMasterItemList, ...newItems],
        notification: null,
        payrollItemNotMapWithPosition: action.payrollItemNotMapWithPosition
      };
    case "PAYROLL_MASTER_ITEM_UPDATED":
      let updatedItem = action.updatedItem;
      let itemList = state.payrollMasterItemList;
      let findIndex = itemList.findIndex(item => {
        return item.id === action.updatedItem.id;
      });
      let updatedItems = update(itemList, {
        [findIndex]: { $set: updatedItem }
      });
      return {
        ...state,
        payrollMasterItemList: updatedItems,
        notification: null
      };
      case "PAYROLL_MASTER_ITEM_UPDATED_NOTIFICATION":
        return {
          ...state,
          notification: {
            type: "success",
            message: "Item Updated Successfully"
          }
        };
      case "PAYROLL_MASTER_LIST_ADDED_NOTIFICATION":
        return {
          ...state,
          notification: {
            type: "success",
            message: "Item Added Successfully"
          }
        };
    default:
      return state;
  }
};

export default PayrollMaster;
