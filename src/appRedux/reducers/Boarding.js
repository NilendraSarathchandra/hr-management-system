
const initialBoarding = {
  positionList: [],
  branchList: [],
  devisionList: []
};

const Boarding = (state = initialBoarding, action) => {
  switch (action.type) {
    case "META_DATA":
      return {
        ...state,
        positionList: action.positionList,
        branchList: action.branchList
      };
      case "DEVISION_META_DATA":
      return {
        ...state,
        devisionList: action.devisionList
      };
      case "EMPLOYEES_LIST":
      return {
        ...state,
        employeeList: action.employeeList,
        customerList: action.customerList
      };
      case "EMPLOYEE_TYPE_LIST":
      return {
        ...state,
        employeeTypeList: action.employeeTypeList
      };
      case "ADD_EMPLOYEE":
      return {
        ...state,
        addEmp: action.addEmp
      };
    default:
      return state;
  }
};

export default Boarding;
