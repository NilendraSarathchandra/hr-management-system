import update from "react-addons-update";

const initValues = {
  notification: null
};

const Customer = (state = initValues, action) => {
  switch (action.type) {
    case "BRANCHES_METADATA_RECEIVED":
      return {
        ...state,
        branches: action.branches,
        customersAll: action.customersAll,
        notification: null
      };
    case "DIVISION_LIST_METADATA_RECEIVED":
      return {
        ...state,
        divisionList: action.divisionList,
        notification: null
      };
    case "CUSTOMER_ADD_SUCCESS":
      return {
        ...state,
        customerInfo: action.customerInfo,
        customersAll: [...state.customersAll, action.customerInfo],
        notification: null
      };
    case "CUSTOMER_UPDATE_SUCCESS":
      let updatedItem = action.updatedItem;
      let itemList = state.customersAll;
      let findIndex = itemList.findIndex(item => {
        return item.id === action.updatedItem.id;
      });
      let updatedItems = update(itemList, {
        [findIndex]: { $set: updatedItem }
      });
      return {
        ...state,
        customerInfo: action.updatedItem,
        customersAll: updatedItems,
        notification: null
      };
    case "PROPERTY_LIST_METADATA_RECEIVED":
      return {
        ...state,
        propertyList: action.propertyList,
        notification: null
      };
    case "CUSTOMER_PROPERTY_LIST_ADD_SUCCESS":
      return {
        ...state,
        UpdatedCustomerPropertyList: action.UpdatedCustomerPropertyList,
        notification: null
      };
    case "ATTACHMENT_LIST_METADATA_RECEIVED":
      return {
        ...state,
        attachmentList: action.attachmentList,
        notification: null
      };
    case "CUSTOMER_ATTACHMENT_UPLOADED_SUCCESS":
      return {
        ...state,
        notification: null
      };
    case "CUSTOMER_PROFILE_IMAGES_UPLOADED_SUCCESS":
      return {
        ...state,
        notification: null
      };
    case "ALL_CUSTOMER_DATA_RECEIVED":
      return {
        ...state,
        notification: null,
        customersAll: action.customersAll
      };
    case "NOTIFICATION":
      return {
        ...state,
        notification: {
          type: action.notification.type,
          message: action.notification.message
        }
      };
    default:
      return state;
  }
};

export default Customer;
