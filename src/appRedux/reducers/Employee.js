
const initialEmployees = {
  employees: [],
  months: [],
  employeeList: []
};

const Employees = (state = initialEmployees, action) => {
  // debugger
  switch (action.type) {
    case "GET_EMPLOYEES":
      return {
        ...state,
        employees: action.employees
      };
    case "GET_MONTHS":
      return {
        ...state,
        months: action.months
      };
    case "ADD_EMPLOYEES":
      return {
        ...state,
        addData: action.addData
      };
    case "UPDATE_EMPLOYEES":
      return {
        ...state,
        updateData: action.updateData
      };
    case "EMPLOYEES_LIST":
      return {
        ...state,
        employeeList: action.employeeList
      };
    case "PAYROLL_ITEM":
      return {
        ...state,
        payrollItem: action.payrollItem
      };
      case "RESET_DATA":
      return {
        ...state,
        updateData: action.updateData,
        addData: action.addData
      };
    default:
      return state;
  }
}; 

export default Employees;
