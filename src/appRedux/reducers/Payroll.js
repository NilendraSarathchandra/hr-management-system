import update from "react-addons-update";

const initialPayroll = {
  notification: null
};

const Payroll = (state = initialPayroll, action) => {
  switch (action.type) {
    case "PAYROLL_ITEM_LIST":
      return {
        ...state,
        payrollItemList: action.payrollItemList,
        payrollItemCategory: action.payrollItemCategory,
        notification: null
      };
    case "PAYROLL_ITEM_ADDED":
      let newItem = action.newItem;
      return {
        ...state,
        payrollItemList: [...state.payrollItemList, newItem],
        notification: null
      };
    case "PAYROLL_ITEM_UPDATED_NOTIFICATION":
      return {
        ...state,
        notification: {
          type: "success",
          message: "Item Updated Successfully"
        }
      };
    case "PAYROLL_ITEM_ADDED_NOTIFICATION":
      return {
        ...state,
        notification: {
          type: "success",
          message: "Item Added Successfully"
        }
      };
    case "ERROR":
      return {
        ...state,
        notification: {
          type: "error",
          message: "Some error occured"
        }
      };
    case "CLEAR_ERROR":
      return {
        ...state,
        notification: null
      };
    case "PAYROLL_ITEM_UPDATED":
      let updatedItem = action.updatedItem;
      let itemList = state.payrollItemList;
      let findIndex = itemList.findIndex(item => {
        return item.id === action.updatedItem.id;
      });
      let updatedItems = update(itemList, {
        [findIndex]: { $set: updatedItem }
      });
      return {
        ...state,
        payrollItemList: updatedItems,
        notification: null
      };
    case "EMPLOYEE_POSITION_LIST":
      return {
        ...state,
        positionList: action.jobPositionItemList,
        positionLevelList: action.positionLevelList,
        JDTemplateList: action.JDTemplateList
      };
    case "EMPLOYEE_POSITION_ITEM_ADDED":
      let newEmpPosition = action.newItem;
      return {
        ...state,
        positionList:
          state.positionList === undefined
            ? [newEmpPosition]
            : [...state.positionList, newEmpPosition],
        notification: null
      };
    case "JOB_POSITION_ITEM_UPDATED":
      if (action.updatedItem === null) {
        return {
          ...state,
          notification: null
        };
      }
      let updatedItemJP = action.updatedItem;
      let positionList = state.positionList;
      let positionIndex = positionList.findIndex(item => {
        return item.id === updatedItemJP.id;
      });
      let updatedItemEmpPosition = update(positionList, {
        [positionIndex]: { $set: updatedItemJP }
      });
      return {
        ...state,
        positionList: updatedItemEmpPosition,
        notification: null
      };
    default:
      return state;
  }
};

export default Payroll;
