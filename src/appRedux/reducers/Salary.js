
const initialBoarding = {
  employeePayroll: [],
  positionList: []
};

const Boarding = (state = initialBoarding, action) => {
  // debugger
  switch (action.type) {
    case "GET_EMPLOYEE_PAYROLL":
      return {
        ...state,
        employeePayroll: action.employeePayroll
      };
    case "EMPLOYEES_LIST":
      return {
        ...state,
        employeeList: action.employeeList
      };
    case "POSITION_MAST":
      return {
        ...state,
        positionList: action.positionList
      };
    case "EMPLOYEES_ADD":
      return {
        ...state,
        addSuccess: action.addSuccess
      };
    case "EMPLOYEES_UPDATE":
      return {
        ...state,
        updateSuccess: action.updateSuccess
      };
    case "RESET_DATA":
      return {
        ...state,
        addSuccess: action.addSuccess,
        updateSuccess: action.updateSuccess
      };
    default:
      return state;
  }
};

export default Boarding;
