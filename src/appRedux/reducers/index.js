import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import Settings from "./Settings";
import Auth from "./Auth";
import Notes from "./Notes";
import Contact from "./Contact";
import Common from "./Common";
import Payroll from "./Payroll";
import Boarding from "./Boarding";
import Salary from "./Salary";
import PayrollMaster from "./PayrollMaster";
import Customer from './Customer';
import Employee from './Employee';
import Business from './Business';

const reducers = combineReducers({
  routing: routerReducer,
  settings: Settings,
  auth: Auth,
  notes: Notes,
  contact: Contact,
  common: Common,
  payroll: Payroll,
  payrollMaster: PayrollMaster,
  customer: Customer,
  boarding: Boarding,
  salary: Salary,
  employee: Employee,
  business: Business,
});

export default reducers;
